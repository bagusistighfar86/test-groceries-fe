import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, HStack, Text, useToast, VStack,
  Flex, Button, TableContainer, Table, Thead,
  Tr, Th, Tbody, Td, Spacer,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCalendar, faCashRegister, faCircle, faCoins, faTag, faUser, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useNavigate, useParams,
} from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { INITIAL_STATE, KasMasukReducer } from 'reducer/Keuangan/KasMasukReducer';
import ConvertISOtoDate from 'utils/ConvertISOtoDate';

function DetailKasMasuk() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(KasMasukReducer, INITIAL_STATE);

  const fetchGetDetailKasMasuk = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: `cashes/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetDetailKasMasuk();
  }, []);

  const isPosting = state.detailKasMasuk.is_posting;

  const kategoriTransaksi = {
    piutang:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="secondary.100">
    <Text color="secondary.600" fontWeight="semibold">Piutang</Text>
  </Box>,
    pembelianRetur:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="blue.100">
    <Text color="blue.600" fontWeight="semibold">Pembelian Retur</Text>
  </Box>,
    penjualan:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="pink.100">
    <Text color="pink.600" fontWeight="semibold">Penjualan</Text>
  </Box>,
  };

  const status = {
    pending:
  <HStack color="basic.400">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Pending</Text>
  </HStack>,
    posting:
  <HStack color="success.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Posting</Text>
  </HStack>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />

            <VStack>
              <Text fontWeight="bold" display={isPosting ? 'block' : 'none'}>{ConvertISOtoDate(state?.detailKasMasuk?.updated_at)}</Text>
              {isPosting ? status.posting : status.pending}
            </VStack>
          </HStack>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Penjualan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Pelanggan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailKasMasuk?.sale?.customer?.fullname}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Tujuan Pengiriman</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.detailKasMasuk?.sale?.customer?.address}
                        {', '}
                        {state?.detailKasMasuk?.sale?.customer?.subdistrict}
                        {', '}
                        {state?.detailKasMasuk?.sale?.customer?.city}
                        {', '}
                        {state?.detailKasMasuk?.sale?.customer?.province}
                        {', '}
                        {state?.detailKasMasuk?.sale?.customer?.country}
                        {', '}
                        {state?.detailKasMasuk?.sale?.customer?.postcode}
                      </Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Beli</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailKasMasuk?.sale?.date_of_sale}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="center">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Pembayaran</Text>
                    </Box>
                    <Box
                      w={{
                        base: '50%',
                        lg: '60%',
                      }}
                    >
                      <Box w="fit-content" px={3} borderRadius={5} bg={state?.detailKasMasuk?.sale?.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                        <Text color="basic.100" fontWeight="semibold">{state?.detailKasMasuk?.sale?.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                      </Box>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kendaraan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailKasMasuk?.sale?.transportation_type?.name}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="center">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Kirim</Text>
                    </Box>
                    <Box
                      w={{
                        base: '50%',
                        lg: '60%',
                      }}
                    >
                      <Text>{state?.detailKasMasuk?.sale?.delivery_date}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Catatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailKasMasuk?.sale?.description}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kategori Transaksi</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      {state?.detailKasMasuk?.reference_type.toLowerCase() === 'pembayaran piutang' && kategoriTransaksi.piutang}
                      {state?.detailKasMasuk?.reference_type.toLowerCase() === 'pembelian retur' && kategoriTransaksi.pembelianRetur}
                      {state?.detailKasMasuk?.reference_type.toLowerCase() === 'penjualan' && kategoriTransaksi.penjualan}
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Pembayaran</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCalendar} />
                          <Text ms={2}>Tanggal</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCoins} />
                          <Text ms={2}>Nominal</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faUser} />
                          <Text ms={2}>Dikelola</Text>
                        </HStack>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailKasMasuk?.sale?.payments.length === 0 ? (
                      <Tr>
                        <Td colspan="99" textAlign="center">
                          <Text fontWeight="semibold" color="basic.700">Tidak ada data</Text>
                        </Td>
                      </Tr>
                    )
                      : state?.detailKasMasuk?.sale?.payments.map((item) => (
                        <Tr key={item.id}>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{item.date}</Text>
                          </Td>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total_pay)}</Text>
                          </Td>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{item.user_name ? item.user_name : '-'}</Text>
                          </Td>
                        </Tr>
                      ))}
                  </Tbody>
                </Table>
              </TableContainer>
            </VStack>

          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Penjualan</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailKasMasuk?.sale?.sale_details.map((item) => (
                      <Tr key={item.id}>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.products?.sell_price)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.qty}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products?.warehouse?.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.subtotal)}</Text>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state?.detailKasMasuk?.total)}</Text>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>

    </DashboardLayout>
  );
}

export default DetailKasMasuk;
