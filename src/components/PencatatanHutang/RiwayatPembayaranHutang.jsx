import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, HStack, Text, useToast, VStack,
  Button, TableContainer, Table, Thead,
  Tr, Th, Tbody, Td, Spacer,
} from '@chakra-ui/react';
import {
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useNavigate, useParams,
} from 'react-router-dom';
import { PencatatanHutangReducer, INITIAL_STATE } from 'reducer/PencatatanHutangReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';

function RiwayatPembayaranHutang() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(PencatatanHutangReducer, INITIAL_STATE);

  const fetchGetDataPembayaran = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: `debt/payment/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPembayaran',
            data: dataBaru,
            isLoading: false,
          },
        });
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const downloadFile = (link) => {
    const popout = window.open(link);
    window.setTimeout(() => {
      popout.close();
    }, 1000);
  };

  useEffect(() => {
    fetchGetDataPembayaran();
  }, []);

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />
            <Text fontWeight="bold">{state.dataPembayaran?.reference_id}</Text>
          </HStack>
        </VStack>

        <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
          <Table
            size={{
              base: 'sm',
              lg: 'md',
            }}
            fontSize={{
              base: 'sm',
              lg: 'md',
            }}
            variant="unstyled"
          >
            <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
              <Tr>
                <Th>
                  <Text>TANGGAL</Text>
                </Th>
                <Th>
                  <Text ms={2}>NOMINAL BAYAR</Text>
                </Th>
                <Th>
                  <Text ms={2}>DIKELOLA</Text>
                </Th>
                <Th>
                  <Text ms={2}>BUKTI BAYAR</Text>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {state.dataPembayaran.map((item) => (
                <Tr key={item.id}>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item?.date}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.total_pay)}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.user_name ? item.user_name : '-'}</Text>
                  </Td>
                  <Td>
                    <Button as={ReachLink} colorScheme="primary" px={10} onClick={() => downloadFile(`https://api-groceries.madjou.com/api/debt/export/${item.proof_picture}`)} target="_blank">Unduh</Button>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
    </DashboardLayout>
  );
}

export default RiwayatPembayaranHutang;
