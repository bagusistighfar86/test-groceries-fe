import {
  Button, FormControl, FormLabel, HStack, Input, InputGroup, InputLeftAddon,
  Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader,
  ModalOverlay, Text, useDisclosure, useToast, VStack,
} from '@chakra-ui/react';
import axios from 'axios';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import getDateNow from 'utils/getDateNow';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import usePencatatanHutangFormValidator from 'utils/usePencatatanHutangFormValidator';

function BayarAngsuranButton({ state, dispatch }) {
  const navigate = useNavigate();
  const auth = { accessToken: getCookie('accessToken') };

  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { errors, validateForm, onBlurField } = usePencatatanHutangFormValidator(state.formBayarAngsuran);

  // Ubah stok akhir
  const onUpdateField = async (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formBayarAngsuran,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onUpdateFieldFile = async (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formBayarAngsuran,
      [field]: e.target.files[0],
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchBayarAngsuran = async () => {
    axios({
      method: 'post',
      url: 'payment/debt',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        reference_id: state.formBayarAngsuran.kodePembelian,
        rest_of_bill: state.formBayarAngsuran.sisaTagihan,
        total_pay: state.formBayarAngsuran.nominalBayar,
        picture: state.formBayarAngsuran.buktiPembayaran,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil membayar angsuran',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        onClose();
        const timeId = setTimeout(() => {
          navigate(0);
        }, 1000);

        return () => {
          clearTimeout(timeId);
        };
      }
      return null;
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal membayar angsuran',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitFormBayarAngsuran = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formBayarAngsuran, errors, forceTouchErrors: true });

    if (!isValid) return;

    fetchBayarAngsuran();
  };

  return (
    <>
      <Button
        onClick={onOpen}
        type="button"
        bg="basic.700"
        variant="solid"
        w="full"
        fontSize="sm"
        px={5}
        py={2}
        borderRadius={5}
        color="basic.100"
        fontWeight="bold"
        _hover={{
          textDecoration: 'none',
          cursor: 'pointer',
          bg: 'basic.600',
        }}
      >
        Bayar Angsuran
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Pembayaran hutang</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_ADD_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl>
                    <FormLabel fontWeight="bold">Kode Pembelian</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodePembelian"
                      w="100%"
                      value={state.formBayarAngsuran.kodePembelian ? state.formBayarAngsuran.kodePembelian : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl>
                    <FormLabel fontWeight="bold">Tanggal Bayar</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="tanggalBayar"
                      w="100%"
                      value={getDateNow()}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl>
                    <FormLabel fontWeight="bold">Sisa Tagihan</FormLabel>
                    <InputGroup>
                      <InputLeftAddon>Rp</InputLeftAddon>
                      <Input
                        isDisabled
                        type="text"
                        name="stokAwal"
                        w="100%"
                        value={state.formBayarAngsuran.sisaTagihan ? state.formBayarAngsuran.sisaTagihan : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </InputGroup>
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nominal Bayar</FormLabel>
                    <InputGroup>
                      <InputLeftAddon>Rp</InputLeftAddon>
                      <Input
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        type="text"
                        name="nominalBayar"
                        w="100%"
                        value={state.formBayarAngsuran.nominalBayar ? state.formBayarAngsuran.nominalBayar : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </InputGroup>
                    {errors.nominalBayar.dirty && errors.nominalBayar.error ? (
                      <Text color="red" my={2}>{errors.nominalBayar.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Bukti Pembayaran</FormLabel>
                    <InputGroup>
                      <Input
                        onBlur={onBlurField}
                        onChange={onUpdateFieldFile}
                        type="file"
                        name="buktiPembayaran"
                        w="100%"
                        accept="image/.*"
                        value={undefined}
                        variant="flushed"
                        border="none"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </InputGroup>
                    {errors.buktiPembayaran.dirty && errors.buktiPembayaran.error ? (
                      <Text color="red" my={2}>{errors.buktiPembayaran.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              isDisabled={state.formBayarAngsuran.nominalBayar === ''}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormBayarAngsuran}
            >
              Simpan

            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default BayarAngsuranButton;
