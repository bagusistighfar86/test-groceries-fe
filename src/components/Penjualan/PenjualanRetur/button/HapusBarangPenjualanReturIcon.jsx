import React from 'react';
import {
  Button, chakra, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function HapusBarangPenjualanReturIcon({
  id, data, dataSaleDetails, dispatch,
}) {
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleDeletePenjualan = () => {
    const dataBaru = data.filter((item) => item.id !== id);
    const dataBaruSaleDetails = dataSaleDetails.filter((item) => item.product_id !== id);
    dispatch({
      type: 'UPDATE_DATA',
      payload: {
        data: dataBaru,
        data_sale_details: dataBaruSaleDetails,
        selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
      },
    });
    onClose();
    toast({
      title: 'Data barang penjualan retur berhasil dihapus',
      position: 'top',
      status: 'success',
      isClosable: true,
    });
  };

  return (
    <>
      <IconChakra
        icon={faTrash}
        onClick={onOpen}
        color="error.600"
        _hover={{ color: 'error.400', cursor: 'pointer' }}
      />

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="lg">Hapus Barang Penjualan Retur</ModalHeader>
          <ModalBody fontSize="md">
            <Text color="basic.500">Apakah Anda yakin? Anda tidak dapat membatalkan tindakan ini setelahnya.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
            <Button colorScheme="error" color="basic.100" borderRadius={10} px={5} onClick={handleDeletePenjualan}>Hapus</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default HapusBarangPenjualanReturIcon;
