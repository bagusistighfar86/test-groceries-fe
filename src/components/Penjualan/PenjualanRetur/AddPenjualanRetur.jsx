import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text,
  useToast, VStack, Flex, Button, TableContainer, Table, Thead, Tr, Th, Center, Checkbox, Td, Tbody,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCashRegister, faCircleInfo, faTag, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { PenjualanReturReducer, INITIAL_STATE } from 'reducer/Penjualan/PenjualanReturReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import Select from 'react-select';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import usePenjualanReturFormValidator from 'utils/usePenjualanReturFormValidator';
import AddBarangPenjualanReturButton from './button/AddBarangPenjualanReturButton';
import EditBarangPenjualanReturIcon from './button/EditBarangPenjualanReturIcon';
import HapusBarangPenjualanReturIcon from './button/HapusBarangPenjualanReturIcon';
import HapusSelectedBarangPenjualanReturButton from './button/HapusSelectedBarangPenjualanReturButton';

function AddPenjualanRetur() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(PenjualanReturReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = usePenjualanReturFormValidator(state.form);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });

    const selectedData = {
      value: e.value, label: e.label,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedData,
      },
    });
    if (name === 'pelangganID') {
      const formAlamatTujuan = {
        ...state.form,
        pelangganID: e.value,
        tujuanPengiriman: e.alamatTujuan,
      };
      dispatch({
        type: 'SET_FORM',
        payload: formAlamatTujuan,
      });
    }
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const setData = (dataBaru) => {
    const form = {
      ...state.form,
      pelangganID: dataBaru.customer_id,
      noPesanan: dataBaru?.id,
      total: 0,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });

    const selectedPelanggan = {
      value: dataBaru.customer_id, label: dataBaru?.customer.fullname, alamatTujuan: dataBaru?.customer.address,
    };

    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedPelanggan',
        data: selectedPelanggan,
      },
    });

    const selectedTipeKendaraan = {
      value: dataBaru.transportation_type_id, label: dataBaru?.transportation_type.name,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedTipeKendaraan',
        data: selectedTipeKendaraan,
      },
    });

    dispatch({ type: 'SET_IS_LOADING', payload: false });
  };

  const fetchGetDetailPenjualanSelesai = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `sales/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });

        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataBarang',
            data: dataBaru?.sale_details?.map((item) => ({
              id: item?.products.id,
              value: item?.products.id,
              label: item?.products.name,
              kodeBarang: item?.products.id,
              gudang: item?.products?.warehouse.name,
              hargaJual: item?.products.sell_price,
              kuantitasMax: item.qty,
              subTotal: item.subtotal,
            })),
          },
        });

        setData(dataBaru);
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  useEffect(() => {
    if (state.sale_details.length !== 0) {
      let totalCount = 0;
      state.sale_details.forEach((item) => { totalCount += item.subtotal; });
      const nextFormState = {
        ...state.form,
        total: totalCount,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    } else {
      const nextFormState = {
        ...state.form,
        total: 0,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    }
  }, [state.sale_details]);

  useEffect(() => {
    fetchGetDetailPenjualanSelesai();
  }, []);

  const fetchAddRetur = () => {
    axios({
      method: 'post',
      url: 'return-sales',
      data: {
        sale_id: state.detailPenjualan.id,
        customer_id: state.form.pelangganID,
        date: state.form.tanggalRetur,
        payment_method: state.detailPenjualan.payment_method,
        transportation_type_id: state.form.tipeKendaraan,
        delivery_date: state.form.tanggalPengiriman,
        total: state.form.total,
        return_type: state.form.jenisPengembalian,
        note: state.form.catatan,
        return_sales_details: state.sale_details,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        dispatch({ type: 'RESET_STATE' });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const jenisPengembalian = [
    { value: 'barang', label: 'Barang' },
    { value: 'uang', label: 'Uang' },
  ];

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataPenjualan.every(checkSelect);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPenjualan.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const onSubmitFormAddPenjualan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchAddRetur();
    navigate('/penjualan');
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Retur</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Pelanggan</FormLabel>
                      <SelectChakra
                        isDisabled
                        id="pelangganID"
                        isSearchable
                        value={state.selectedPelanggan}
                        options={state.dataPelanggan}
                        styles={customStyles}
                        w="100%"
                        placeholder="Select pelanggan"
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Nomor Pesanan</FormLabel>
                      <Input
                        isDisabled
                        type="text"
                        name="noPesanan"
                        w="100%"
                        value={state.form.noPesanan ? state.form.noPesanan : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Retur</FormLabel>
                      <Input
                        isDisabled
                        type="date"
                        name="tanggalRetur"
                        w="100%"
                        onChange={onUpdateField}
                        value={state.form.tanggalRetur ? state.form.tanggalRetur : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jenis Pengembalian</FormLabel>
                      <SelectChakra
                        name="jenisPengembalian"
                        placeholder="Pilih jenis pengembalian"
                        onChange={(e) => onSelectUpdateField(e, 'jenisPengembalian', 'selectedJenisPengembalian')}
                        onBlur={(e) => onBlurField(e, 'jenisPengembalian')}
                        value={state.selectedJenisPengembalian}
                        options={jenisPengembalian}
                        styles={customStyles}
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jenisPengembalian.dirty && errors.jenisPengembalian.error ? (
                        <Text color="red" my={2}>{errors.jenisPengembalian.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Catatan</FormLabel>
                      <Input
                        type="text"
                        name="catatan"
                        value={state.form.catatan ? state.form.catatan : ''}
                        onChange={onUpdateField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Penjualan</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <HStack
                w="100%"
                justifyContent="space-between"
              >
                <AddBarangPenjualanReturButton
                  state={state}
                  dispatch={dispatch}
                />
                <HapusSelectedBarangPenjualanReturButton
                  noSelected={noSelected}
                  data={state.dataPenjualan}
                  dataSaleDetails={state.sale_details}
                  dispatch={dispatch}
                  selectedData={state.selectedDataPenjualan}
                />
              </HStack>

              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <Center py={3}>
                          <Box w="10px" h="10px" bg="white" />
                        </Center>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                      <Th textAlign="center">
                        <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCircleInfo} />
                          <Text ms={2}>AKSI</Text>
                        </Center>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.dataPenjualan.map((item, index) => (
                      <Tr key={item.id}>
                        <Td>
                          <Center>
                            <Checkbox
                              colorScheme="primary"
                              isChecked={state?.selectedDataPenjualan[index].isSelect}
                              onChange={(e) => handleSelected(e, item.id)}
                            />
                          </Center>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.namaBarang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.hargaJual)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.kuantitasRetur}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.gudang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subTotal)}</Text>
                        </Td>
                        <Td textAlign="center">
                          <HStack justifyContent="center" spacing={5}>
                            <EditBarangPenjualanReturIcon
                              data={item}
                              state={state}
                              dispatch={dispatch}
                            />
                            <HapusBarangPenjualanReturIcon
                              id={item.id}
                              data={state.dataPenjualan}
                              dataSaleDetails={state.sale_details}
                              dispatch={dispatch}
                            />
                          </HStack>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state.form.total)}</Text>
              </HStack>
              <HStack spacing={5} alignSelf="end">
                <Button
                  as={ReachLink}
                  onClick={() => navigate(-1)}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Batal
                </Button>
                <Button
                  isDisabled={state.sale_details.length === 0}
                  type="button"
                  onClick={onSubmitFormAddPenjualan}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddPenjualanRetur;
