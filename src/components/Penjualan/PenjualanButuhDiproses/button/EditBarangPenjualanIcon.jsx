import {
  Button, chakra, FormControl, FormLabel, HStack, Input, InputGroup,
  InputRightAddon, Modal, ModalBody, ModalCloseButton,
  ModalContent, ModalFooter, ModalHeader, ModalOverlay, NumberInput, NumberInputField, Text,
  useDisclosure, useToast, VStack,
} from '@chakra-ui/react';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import Select from 'react-select';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import useBarangPenjualanFormValidator from 'utils/useBarangPenjualanFormValidator';

function EditBarangPenjualanIcon({ data, state, dispatch }) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const SelectChakra = chakra(Select);
  const IconChakra = chakra(FontAwesomeIcon);

  const { errors, validateForm, onBlurField } = useBarangPenjualanFormValidator(state.formEditBarang);

  const handleOpen = () => {
    const nextFormState = {
      barangID: data.id,
      kuantitas: data.kuantitas,
      subTotal: data.kuantitas * data.hargaJual,
    };
    dispatch({
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });

    onOpen();
  };

  useEffect(() => {
    if (isOpen) {
      if (state.formEditBarang.kuantitas < 1) {
        const nextFormState = {
          ...state.formEditBarang,
          kuantitas: 1,
          subTotal: 1 * data.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_EDIT_BARANG',
          payload: nextFormState,
        });
      } else if (state.formEditBarang.kuantitas <= (data.stok - data.limitStok)) {
        const nextFormState = {
          ...state.formEditBarang,
          subTotal: state.formEditBarang.kuantitas * data.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_EDIT_BARANG',
          payload: nextFormState,
        });
      } else {
        const nextFormState = {
          ...state.formEditBarang,
          kuantitas: data.stok - data.limitStok,
          subTotal: (data.stok - data.limitStok) * data.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_EDIT_BARANG',
          payload: nextFormState,
        });
      }
    }
  }, [state.formEditBarang.kuantitas]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formEditBarang,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditBarangPenjualan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formEditBarang, errors, forceTouchErrors: true });
    const dataBaru = {
      id: data.id,
      kodeBarang: data.kodeBarang,
      namaBarang: data.namaBarang,
      hargaJual: data.hargaJual,
      kuantitas: state.formEditBarang.kuantitas,
      gudang: data.gudang,
      stok: data.stok,
      limitStok: data.limitStok,
      subTotal: state.formEditBarang.subTotal,
    };
    if (!isValid) return;

    const itemPosition = state.dataPenjualan.map((item) => item.id).indexOf(dataBaru.id);
    dispatch({
      type: 'SET_QUANTITY',
      payload: {
        itemPos: itemPosition,
        data: dataBaru,
      },
    });
    toast({
      title: 'Data berhasil diubah',
      position: 'top',
      status: 'success',
      isClosable: true,
    });

    onClose();
    dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' });
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  return (
    <>
      <IconChakra
        icon={faPenToSquare}
        onClick={handleOpen}
        color="basic.700"
        _hover={{ color: 'basic.500', cursor: 'pointer' }}
      />

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Edit Barang Penjualan</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kode Barang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodeBarang"
                      w="100%"
                      value={data.id ? data.id : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                    <SelectChakra
                      isDisabled
                      id="barangID"
                      value={{ value: data.id, label: data.namaBarang }}
                      styles={customStyles}
                      w="100%"
                      placeholder="Pilih data barang"
                      border="1px solid"
                      borderColor="primary.300"
                      borderRadius={5}
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                    {errors.barangID.dirty && errors.barangID.error ? (
                      <Text color="red" my={2}>{errors.barangID.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Gudang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="gudang"
                      w="100%"
                      value={data.gudang ? data.gudang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Harga Jual</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="hargaJual"
                      w="100%"
                      value={data.hargaJual ? ConvertMoneyIDR(data.hargaJual) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok</FormLabel>
                    <Input
                      isDisabled
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      name="stok"
                      w="100%"
                      defaultValue={data.stok}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Limit Stok</FormLabel>
                    <Input
                      isDisabled
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      name="limitStok"
                      w="100%"
                      value={data.limitStok}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kuantitas</FormLabel>
                    <InputGroup>
                      <NumberInput
                        isDisabled={data.limitStok === 0 || data.stok === 0}
                        min={1}
                        max={state.selectedBarang.stok - state.selectedBarang.limitStok - state.selectedBarang.kuantitas}
                        w="100%"
                        defaultValue={state.formEditBarang.kuantitas ? state.formEditBarang.kuantitas : ''}
                      >
                        <NumberInputField
                          name="kuantitas"
                          placeholder="Masukkan jumlah barang"
                          w="100%"
                          onChange={onUpdateField}
                          onBlur={onBlurField}
                          value={state.formEditBarang.kuantitas ? state.formEditBarang.kuantitas : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </NumberInput>
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                    {errors.kuantitas.dirty && errors.kuantitas.error ? (
                      <Text color="red" my={2}>{errors.kuantitas.message}</Text>
                    ) : <Text color="basic.500" my={2}>Kuantitas harus kurang dari stok dan total stok tidak boleh kurang dari limit stok</Text>}
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Sub Total</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="subTotal"
                      w="100%"
                      value={state.formEditBarang.subTotal ? ConvertMoneyIDR(state.formEditBarang.subTotal) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              disabled={state.formEditBarang.subTotal === 0 || !state.formEditBarang.subTotal}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormEditBarangPenjualan}
            >
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default EditBarangPenjualanIcon;
