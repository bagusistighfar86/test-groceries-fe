import {
  Button, chakra, FormControl, FormLabel, HStack, Input, InputGroup,
  InputRightAddon, Link, Modal, ModalBody, ModalCloseButton, ModalContent,
  ModalFooter, ModalHeader, ModalOverlay, NumberInput, NumberInputField,
  Text, useDisclosure, VStack,
} from '@chakra-ui/react';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import Select from 'react-select';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import useBarangPenjualanFormValidator from 'utils/useBarangPenjualanFormValidator';
import { Link as ReachLink, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setPrevPath } from 'redux/reducer/navigationReducer';

function AddBarangPenjualanButton({ state, dispatch }) {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { errors, validateForm, onBlurField } = useBarangPenjualanFormValidator(state.formAddBarang);

  const dataSudahDitambahkan = (id) => state.selectedDataPenjualan.some((item) => id === item.id);
  // ubah subTotal
  useEffect(() => {
    if (isOpen) {
      if (state.formAddBarang.kuantitas < 1) {
        const nextFormState = {
          ...state.formAddBarang,
          kuantitas: 1,
          subTotal: 1 * state.selectedBarang.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_ADD_BARANG',
          payload: nextFormState,
        });
      } else if (state.formAddBarang.kuantitas <= (state.selectedBarang.stok - state.selectedBarang.limitStok)) {
        const nextFormState = {
          ...state.formAddBarang,
          subTotal: state.formAddBarang.kuantitas * state.selectedBarang.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_ADD_BARANG',
          payload: nextFormState,
        });
      } else {
        const nextFormState = {
          ...state.formAddBarang,
          kuantitas: state.selectedBarang.stok - state.selectedBarang.limitStok,
          subTotal: (state.selectedBarang.stok - state.selectedBarang.limitStok) * state.selectedBarang.hargaJual,
        };
        dispatch({
          type: 'SET_FORM_ADD_BARANG',
          payload: nextFormState,
        });
      }
    }
  }, [state.formAddBarang.kuantitas]);

  // Ubah kuantitas
  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formAddBarang,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM_ADD_BARANG',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  // Ubah kodeBarang
  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.formAddBarang,
      [field]: e.value,
    };
    const selectedBarang = {
      id: e.id,
      value: e.value,
      label: e.label,
      kodeBarang: e.kodeBarang,
      gudang: e.gudang,
      hargaJual: e.hargaJual,
      stok: e.stok,
      limitStok: e.limitStok,
    };

    dispatch({
      type: 'SET_FORM_ADD_BARANG',
      payload: nextFormState,
    });
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedBarang,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  const onSubmitFormAddBarangPenjualan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formAddBarang, errors, forceTouchErrors: true });
    const dataBaru = {
      id: state.selectedBarang.id,
      kodeBarang: state.selectedBarang.kodeBarang,
      namaBarang: state.selectedBarang.label,
      hargaJual: state.selectedBarang.hargaJual,
      kuantitas: state.formAddBarang.kuantitas,
      gudang: state.selectedBarang.gudang,
      stok: state.selectedBarang.stok,
      limitStok: state.selectedBarang.limitStok,
      subTotal: state.formAddBarang.subTotal,
    };
    if (!isValid) return;
    if (!dataSudahDitambahkan(dataBaru.id)) {
      dispatch({
        type: 'SET_DATA',
        payload: {
          name: 'dataPenjualan',
          data: [
            ...state.dataPenjualan,
            dataBaru,
          ],
        },
      });
      dispatch({
        type: 'SET_DATA',
        payload: {
          name: 'sale_details',
          data: [
            ...state.sale_details,
            {
              product_id: dataBaru.id,
              qty: dataBaru.kuantitas,
              subtotal: dataBaru.subTotal,
            },
          ],
        },
      });
      dispatch({
        type: 'SET_SELECTED_SELECT',
        payload: {
          dataName: 'selectedDataPenjualan',
          data: [
            ...state.selectedDataPenjualan,
            { id: dataBaru.id, isSelect: false },
          ],
        },
      });
    } else {
      const itemPosition = state.dataPenjualan.map((item) => item.id).indexOf(dataBaru.id);
      dispatch({
        type: 'ADD_QUANTITY',
        payload: {
          itemPos: itemPosition,
          data: dataBaru,
        },
      });
    }
    onClose();
    dispatch({ type: 'RESET_STATE_ADD_BARANG_FORM' });
  };

  return (
    <>
      <Button
        onClick={onOpen}
        bg="basic.700"
        color="basic.100"
        fontWeight="normal"
        px={6}
        _hover={{
          bg: 'basic.900',
        }}
      >
        + Barang Penjualan
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Tambah Barang Penjualan</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_ADD_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kode Barang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodeBarang"
                      w="100%"
                      value={state.selectedBarang.kodeBarang ? state.selectedBarang.kodeBarang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                    <InputGroup w="100%">
                      <SelectChakra
                        id="barangID"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'barangID', 'selectedBarang')}
                        onBlur={(e) => onBlurField(e, 'barangID')}
                        value={state.selectedBarang}
                        options={state.dataBarang}
                        isOptionDisabled={(option) => dataSudahDitambahkan(option.id) || option.stok === 0}
                        styles={customStyles}
                        w="100%"
                        placeholder="Pilih data barang"
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightAddon p={0}>
                        <Link as={ReachLink} to="/master-barang/tambah-barang" onClick={() => dispatchRedux(setPrevPath(location.pathname))} px={3}>
                          <IconChakra icon={faPlus} />
                        </Link>
                      </InputRightAddon>
                    </InputGroup>
                    {errors.barangID.dirty && errors.barangID.error ? (
                      <Text color="red" my={2}>{errors.barangID.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Gudang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="gudang"
                      w="100%"
                      value={state.selectedBarang.gudang ? state.selectedBarang.gudang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Harga Jual</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="hargaJual"
                      w="100%"
                      value={state.selectedBarang.hargaJual ? ConvertMoneyIDR(state.selectedBarang.hargaJual) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok</FormLabel>
                    <Input
                      isDisabled
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      name="stok"
                      w="100%"
                      defaultValue={state.selectedBarang.stok}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Limit Stok</FormLabel>
                    <Input
                      isDisabled
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      name="limitStok"
                      w="100%"
                      value={state.selectedBarang.limitStok}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kuantitas</FormLabel>
                    <InputGroup>
                      <NumberInput
                        isDisabled={state.selectedBarang.stok === 0 || state.selectedBarang.stok === 0}
                        min={1}
                        max={state.selectedBarang.stok - state.selectedBarang.limitStok}
                        w="100%"
                      >
                        <NumberInputField
                          name="kuantitas"
                          placeholder="Masukkan jumlah barang"
                          w="100%"
                          onChange={onUpdateField}
                          onBlur={onBlurField}
                          value={state.formAddBarang.kuantitas ? state.formAddBarang.kuantitas : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </NumberInput>
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                    {errors.kuantitas.dirty && errors.kuantitas.error ? (
                      <Text color="red" my={2}>{errors.kuantitas.message}</Text>
                    ) : <Text color="basic.500" my={2}>Kuantitas harus kurang dari stok dan total stok tidak boleh kurang dari limit stok</Text>}
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Sub Total</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="subTotal"
                      w="100%"
                      value={state.formAddBarang.kuantitas ? ConvertMoneyIDR(state.formAddBarang.subTotal) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              disabled={state.formAddBarang.subTotal === 0
                || !state.formAddBarang.subTotal || state.formAddBarang.kuantitas === 0}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormAddBarangPenjualan}
            >
              Simpan

            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default AddBarangPenjualanButton;
