import React, { useEffect, useReducer, useState } from 'react';
import {
  Link, chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr, Flex, useToast, Box, Checkbox, Center, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
// import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faLock, faGear, faBoxesStacked, faCalendar, faSort, faPenToSquare, faUser, faCoins, faCircleInfo,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, PenjualanDiprosesReducer } from 'reducer/Penjualan/PenjualanDiprosesReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import ReactSelect from 'react-select';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import HapusDataPenjualanIcon from './button/HapusDataPenjualanIcon';
import HapusSelectedPenjualanButton from './button/HapusSelectedPenjualanButton';

function PenjualanButuhDiproses() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(ReactSelect);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(PenjualanDiprosesReducer, INITIAL_STATE);
  const [changeOption, setChangeOption] = useState({
    selected: { value: '', label: '' },
    index: '',
    itemID: '',
  });

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataPenjualan.every(checkSelect);

  const fetchGetAllPenjualan = () => {
    axios({
      method: 'get',
      url: `sales?limit=${state.showRows}&type=1,2,3,4`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item, index) => ({ id: item.id, isSelect: false, idx: index })),
            isLoading: false,
          },
        });

        const selectedStatus = dataBaru.map(
          (item) => ({ value: item.status_id, label: item?.status.name }),
        );

        const dataStatus = dataBaru.map((item) => {
          if (item?.status_id === 1) {
            return [
              { value: 1, label: 'Butuh Diproses', disabled: true },
              { value: 2, label: 'Diproses', disabled: false },
              { value: 3, label: 'Siap Dikirim', disabled: true },
              { value: 4, label: 'Dikirim', disabled: true },
              { value: 5, label: 'Selesai', disabled: true },
              { value: 7, label: 'Batal', disabled: false },
            ];
          }
          if (item?.status_id === 4) {
            return [
              { value: 1, label: 'Butuh Diproses', disabled: true },
              { value: 2, label: 'Diproses', disabled: true },
              { value: 3, label: 'Siap Dikirim', disabled: true },
              { value: 4, label: 'Dikirim', disabled: true },
              { value: 5, label: 'Selesai', disabled: false },
              { value: 7, label: 'Batal', disabled: true },
            ];
          }
          return null;
        });

        dispatch({
          type: 'SET_STATUS',
          payload: {
            selected: selectedStatus,
            data: dataStatus,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  useEffect(() => {
    fetchGetAllPenjualan();
  }, []);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPenjualan.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  useEffect(() => {
    fetchGetAllPenjualan();
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const fetchChangeStatus = async () => {
    await axios({
      method: 'patch',
      url: `sales/status-order/${changeOption?.itemID}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        status_id: changeOption?.selected?.value,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const status = changeOption?.selected?.value;

        if (status !== 1 && status !== 2 && status !== 3 && status !== 4) {
          const dataBaru = state.dataPenjualan.filter((item) => item.id !== changeOption?.itemID);
          const selectedStatusBaru = state.selectedStatus;
          selectedStatusBaru.splice(changeOption?.index, 1);
          const dataStatusBaru = state.dataStatus;
          dataStatusBaru.splice(changeOption?.index, 1);

          dispatch({
            type: 'REMOVE_DATA',
            payload: {
              data: dataBaru,
              selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
              selectedStatus: selectedStatusBaru,
              dataStatus: dataStatusBaru,
            },
          });
        } else {
          const selectedStatus = changeOption?.selected;

          dispatch({
            type: 'UPDATE_STATUS',
            payload: {
              itemPos: changeOption?.index,
              value: changeOption?.selected?.value,
              data: selectedStatus,
            },
          });

          const dataBaru = state.dataPenjualan.map((item) => {
            if (item.id === changeOption?.itemID) return { ...item, status_id: changeOption?.selected?.value };
            return item;
          });
          dispatch({
            type: 'UPDATE_DATA',
            payload: {
              data: dataBaru,
              selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            },
          });
        }

        onClose();

        toast({
          title: 'Berhasil memperbarui status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal memperbarui status',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSelectUpdateField = async (e, index, itemID) => {
    await setChangeOption({
      selected: { value: e.value, label: e.label },
      index,
      itemID,
    });
    onOpen();
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />

          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <HStack spacing={noSelected ? 0 : 7} mt={{ base: 5, xl: 0 }}>
            <HapusSelectedPenjualanButton
              noSelected={noSelected}
              accessToken={auth.accessToken}
              data={state.dataPenjualan}
              state={state}
              dispatch={dispatch}
              selectedData={state.selectedDataPenjualan}
            />

            <Link
              as={ReachLink}
              to="/penjualan/tambah-penjualan"
              bg="primary.500"
              variant="solid"
              fontSize="sm"
              px={5}
              py={2}
              borderRadius={5}
              color="basic.100"
              fontWeight="bold"
              _hover={{
                textDecoration: 'none',
                cursor: 'pointer',
                bg: 'primary.600',
              }}
            >
              Tambah Penjualan
            </Link>
          </HStack>

        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'xxl',
            lg: 'sm',
            xl: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <Center py={3}>
                  <Box w="10px" h="10px" bg="white" />
                </Center>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text display={{ base: 'none', lg: 'block' }}>ID PENJUALAN</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text display={{ base: 'none', lg: 'block' }}>PELANGGAN</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Flex ms={2} flexDir="column">
                    <Text>TANGGAL </Text>
                    <Text>JUAL</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCoins} />
                  <Flex ms={2} flexDir="column">
                    <Text>TOTAL</Text>
                    <Text>HARGA</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Flex ms={2} flexDir="column">
                    <Text>JENIS</Text>
                    <Text>PEMBAYARAN</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th w="auto">
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>STATUS</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataPenjualan.map((item, index) => (
              <Tr key={item.id}>
                <Td py={5}>
                  <Center>
                    <Checkbox
                      disabled={item.status_id !== 1}
                      colorScheme="primary"
                      isChecked={state.selectedDataPenjualan[index].isSelect}
                      onChange={(e) => handleSelected(e, item.id, index)}
                    />
                  </Center>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/penjualan/detail-penjualan/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.customer.fullname}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.date_of_sale}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total)}</Text>
                </Td>
                <Td>
                  <Box w="fit-content" px={3} py={2} borderRadius={5} bg={item.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                    <Text color="basic.100" fontWeight="semibold">{item.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                  </Box>
                </Td>
                <Td w="auto">
                  <SelectChakra
                    isDisabled={state?.selectedStatus[index]?.value !== 1 && state?.selectedStatus[index]?.value !== 4}
                    id="status"
                    onChange={(e) => onSelectUpdateField(e, index, item.id)}
                    value={state?.selectedStatus[index]}
                    options={state?.dataStatus[index]}
                    isOptionDisabled={(option) => option.disabled}
                    styles={customStyles}
                    border="1px solid"
                    borderColor="primary.300"
                    borderRadius={5}
                    color="basic.500"
                    _focus={{
                      color: 'basic.800',
                    }}
                    menuPortalTarget={document.body}
                  />
                </Td>
                <Td>
                  <HStack justifyContent="center" spacing={5}>
                    {item.status_id === 1
                      ? (
                        <Link
                          as={ReachLink}
                          to={`/penjualan/penjualan-butuh-diproses/edit-penjualan/${item.id}`}
                          color="basic.700"
                          _hover={{ color: 'basic.500', cursor: 'pointer' }}
                        >
                          <IconChakra icon={faPenToSquare} />
                        </Link>
                      )
                      : (
                        <IconChakra color="basic.400" icon={faPenToSquare} />
                      )}
                    <HapusDataPenjualanIcon accessToken={auth.accessToken} id={item.id} data={state.dataPenjualan} state={state} dispatch={dispatch} status={item.status_id} />
                  </HStack>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="9">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <IconChakra icon={faChevronLeft} fontSize="sm" _hover={{ color: 'basic.500', cursor: 'pointer' }} />
                  <Text>1 of 1</Text>
                  <IconChakra icon={faChevronRight} fontSize="sm" _hover={{ color: 'basic.500', cursor: 'pointer' }} />
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>

      <ConfirmChangeStatus isOpen={isOpen} onClose={onClose} changeOption={changeOption} fetchChangeStatus={fetchChangeStatus} />
    </>
  );
}

export function ConfirmChangeStatus({
  isOpen, onClose, changeOption, fetchChangeStatus,
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader fontSize="lg">
          Penjualan
          {' '}
          {changeOption?.selected.label}
        </ModalHeader>
        <ModalBody fontSize="md">
          <Text color="basic.500">
            Apakah Anda yakin penjualan
            {' '}
            {changeOption?.selected.label.toLowerCase()}
            ? Anda tidak dapat membatalkan tindakan ini setelahnya.
          </Text>
        </ModalBody>
        <ModalFooter>
          <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
          <Button colorScheme="primary" color="basic.100" borderRadius={10} px={5} onClick={() => fetchChangeStatus()}>Ya</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default PenjualanButuhDiproses;
