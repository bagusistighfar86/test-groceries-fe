import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text,
  useToast, VStack, Flex, Button, Link, InputGroup, InputRightAddon, TableContainer, Table, Thead, Tr, Th, Center, Checkbox, Td, Tbody,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCashRegister, faCircleInfo, faPlus, faTag, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useLocation, useNavigate, useParams,
} from 'react-router-dom';
import usePenjualanFormValidator from 'utils/usePenjualanFormValidator';
import { PenjualanDiprosesReducer, INITIAL_STATE } from 'reducer/Penjualan/PenjualanDiprosesReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import Select from 'react-select';
import LoadingPage from 'views/LoadingPage';
import { useDispatch } from 'react-redux';
import { setPrevPath } from 'redux/reducer/navigationReducer';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import HapusSelectedBarangPenjualanButton from './button/HapusSelectedBarangPenjualanButton';
import AddBarangPenjualanButton from './button/AddBarangPenjualanButton';
import EditBarangPenjualanIcon from './button/EditBarangPenjualanIcon';
import HapusBarangPenjualanIcon from './button/HapusBarangPenjualanIcon';

function EditPenjualan() {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(PenjualanDiprosesReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = usePenjualanFormValidator(state.form);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });

    const selectedData = {
      value: e.value, label: e.label,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedData,
      },
    });
    if (name === 'pelangganID') {
      const formAlamatTujuan = {
        ...state.form,
        pelangganID: e.value,
        tujuanPengiriman: e.alamatTujuan,
      };
      dispatch({
        type: 'SET_FORM',
        payload: formAlamatTujuan,
      });
    }
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const setData = (dataBaru) => {
    const form = {
      pelangganID: dataBaru.customer_id,
      tujuanPengiriman: dataBaru?.customer.address,
      tanggalJual: dataBaru.date_of_sale,
      jenisPembayaran: dataBaru.payment_method,
      tipeKendaraan: dataBaru.transportation_type_id,
      tanggalPengiriman: dataBaru.delivery_date,
      catatan: dataBaru.description,
      total: dataBaru.total,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });

    dispatch({
      type: 'SET_DATA',
      payload: {
        name: 'dataPenjualan',
        data: dataBaru?.sale_details.map((item) => (
          {
            id: item.product_id,
            namaBarang: item?.products.name,
            hargaJual: item?.products.sell_price,
            kuantitas: item.qty,
            gudang: item?.products?.warehouse.name,
            subTotal: item.subtotal,
            stok: item?.products?.stock,
            limitStok: item?.products?.limit_stock,
          }
        )),
      },
    });

    const selectedPelanggan = {
      value: dataBaru.customer_id, label: dataBaru?.customer.fullname, alamatTujuan: dataBaru?.customer.address,
    };

    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedPelanggan',
        data: selectedPelanggan,
      },
    });

    const selectedTipeKendaraan = {
      value: dataBaru.transportation_type_id, label: dataBaru?.transportation_type.name,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedTipeKendaraan',
        data: selectedTipeKendaraan,
      },
    });

    const selectedJenisPembayaran = {
      value: dataBaru.payment_method, label: dataBaru.payment_method,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedJenisPembayaran',
        data: selectedJenisPembayaran,
      },
    });

    dispatch({
      type: 'SET_DATA',
      payload: {
        name: 'sale_details',
        data: dataBaru?.sale_details.map((item) => (
          {
            product_id: item.product_id,
            qty: item.qty.toString(),
            subtotal: item.subtotal,
          }
        )),
      },
    });

    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'selectedDataPenjualan',
        data: dataBaru?.sale_details.map((item) => ({ id: item.product_id, isSelect: false })),
      },
    });

    dispatch({ type: 'SET_IS_LOADING', payload: false });
  };

  const fetchGetDetailPenjualan = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `sales/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        setData(dataBaru);
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetAllPelanggan = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: 'customers',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPelanggan',
            data: dataBaru.map((item) => ({ value: item.id, label: item.fullname, alamatTujuan: item.address })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetAllTipeKendaraan = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'transportation_types',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataTipeKendaraan',
            data: dataBaru.map((item) => ({ value: item.id, label: item.name })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetAllBarang = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'products',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataBarang',
            data: dataBaru.map((item) => ({
              id: item.id,
              value: item.id,
              label: item.name,
              kodeBarang: item.id,
              gudang: item?.warehouse.name,
              hargaJual: item.sell_price,
              stok: item.stock,
              limitStok: item.limit_stock,
            })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (state.sale_details.length !== 0) {
      let totalCount = 0;
      state.sale_details.forEach((item) => { totalCount += item.subtotal; });
      const nextFormState = {
        ...state.form,
        total: totalCount,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    } else {
      const nextFormState = {
        ...state.form,
        total: 0,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    }
  }, [state.sale_details]);

  useEffect(() => {
    fetchGetAllPelanggan();
    fetchGetAllTipeKendaraan();
    fetchGetAllBarang();
    fetchGetDetailPenjualan();
  }, []);

  const fetchEditPenjualan = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'put',
      url: `sales/${id}`,
      data: {
        customer_id: state.form.pelangganID,
        date_of_sale: state.form.tanggalJual,
        payment_method: state.form.jenisPembayaran,
        transportation_type_id: state.form.tipeKendaraan,
        delivery_date: state.form.tanggalPengiriman,
        total: state.form.total,
        description: state.form.catatan,
        sale_details: state.sale_details,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        navigate('/penjualan');
        toast({
          title: 'Data berhasil diubah',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gagal diubah',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const jenisPembayaran = [
    { value: 'tunai', label: 'Tunai' },
    { value: 'kredit', label: 'Kredit' },
  ];

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataPenjualan.every(checkSelect);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPenjualan.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const onSubmitFormEditPenjualan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEditPenjualan();
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Penjualan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Pelanggan</FormLabel>
                      <InputGroup w="100%">
                        <SelectChakra
                          id="pelangganID"
                          isSearchable
                          onChange={(e) => onSelectUpdateField(e, 'pelangganID', 'selectedPelanggan')}
                          onBlur={(e) => onBlurField(e, 'pelangganID')}
                          value={state.selectedPelanggan}
                          options={state.dataPelanggan}
                          styles={customStyles}
                          w="100%"
                          placeholder="Select pelanggan"
                          border="1px solid"
                          borderColor="primary.300"
                          borderRadius={5}
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                        <InputRightAddon p={0}>
                          <Link as={ReachLink} to="/pelanggan/tambah-pelanggan" onClick={() => dispatchRedux(setPrevPath(location.pathname))} px={3}>
                            <IconChakra icon={faPlus} />
                          </Link>
                        </InputRightAddon>
                      </InputGroup>
                      {errors.pelangganID.dirty && errors.pelangganID.error ? (
                        <Text color="red" my={2}>{errors.pelangganID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tujuan Pengiriman</FormLabel>
                      <Input
                        isDisabled
                        type="text"
                        name="tujuanPengiriman"
                        w="100%"
                        onChange={onUpdateField}
                        value={state.form.tujuanPengiriman ? state.form.tujuanPengiriman : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Jual</FormLabel>
                      <Input
                        isDisabled
                        type="date"
                        name="tanggalJual"
                        w="100%"
                        onChange={onUpdateField}
                        value={state.form.tanggalJual ? state.form.tanggalJual : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jenis Pembayaran</FormLabel>
                      <SelectChakra
                        name="jenisPembayaran"
                        placeholder="Pilih jenis pembayaran"
                        onChange={(e) => onSelectUpdateField(e, 'jenisPembayaran', 'selectedJenisPembayaran')}
                        onBlur={(e) => onBlurField(e, 'jenisPembayaran')}
                        value={state.selectedJenisPembayaran}
                        options={jenisPembayaran}
                        styles={customStyles}
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jenisPembayaran.dirty && errors.jenisPembayaran.error ? (
                        <Text color="red" my={2}>{errors.jenisPembayaran.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Tipe Kendaraan</FormLabel>
                      <SelectChakra
                        id="tipeKendaraan"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'tipeKendaraan', 'selectedTipeKendaraan')}
                        onBlur={(e) => onBlurField(e, 'tipeKendaraan')}
                        value={state.selectedTipeKendaraan}
                        options={state.dataTipeKendaraan}
                        styles={customStyles}
                        w="100%"
                        placeholder="Select tipe kendaraan"
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.tipeKendaraan.dirty && errors.tipeKendaraan.error ? (
                        <Text color="red" my={2}>{errors.tipeKendaraan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Pengiriman</FormLabel>
                      <Input
                        type="date"
                        name="tanggalPengiriman"
                        w="100%"
                        onChange={onUpdateField}
                        value={state.form.tanggalPengiriman ? state.form.tanggalPengiriman : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.tanggalPengiriman.dirty && errors.tanggalPengiriman.error ? (
                        <Text color="red" my={2}>{errors.tanggalPengiriman.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Catatan</FormLabel>
                      <Input
                        type="text"
                        name="catatan"
                        value={state.form.catatan ? state.form.catatan : ''}
                        onChange={onUpdateField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Penjualan</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <HStack
                w="100%"
                justifyContent="space-between"
              >
                <AddBarangPenjualanButton
                  state={state}
                  dispatch={dispatch}
                />
                <HapusSelectedBarangPenjualanButton
                  noSelected={noSelected}
                  data={state.dataPenjualan}
                  dataSaleDetails={state.sale_details}
                  dispatch={dispatch}
                  selectedData={state.selectedDataPenjualan}
                />
              </HStack>

              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <Center py={3}>
                          <Box w="10px" h="10px" bg="white" />
                        </Center>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                      <Th textAlign="center">
                        <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCircleInfo} />
                          <Text ms={2}>AKSI</Text>
                        </Center>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.dataPenjualan.map((item, index) => (
                      <Tr key={item.id}>
                        <Td>
                          <Center>
                            <Checkbox
                              colorScheme="primary"
                              isChecked={state?.selectedDataPenjualan[index]?.isSelect}
                              onChange={(e) => handleSelected(e, item.id)}
                            />
                          </Center>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.namaBarang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.hargaJual)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.kuantitas}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.gudang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subTotal)}</Text>
                        </Td>
                        <Td textAlign="center">
                          <HStack justifyContent="center" spacing={5}>
                            <EditBarangPenjualanIcon
                              data={item}
                              state={state}
                              dispatch={dispatch}
                            />
                            <HapusBarangPenjualanIcon
                              id={item.id}
                              data={state.dataPenjualan}
                              dataSaleDetails={state.sale_details}
                              dispatch={dispatch}
                            />
                          </HStack>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state.form.total)}</Text>
              </HStack>
              <HStack spacing={5} alignSelf="end">
                <Button
                  as={ReachLink}
                  onClick={() => navigate(-1)}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Batal
                </Button>
                <Button
                  isDisabled={state.sale_details.length === 0}
                  type="button"
                  onClick={onSubmitFormEditPenjualan}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditPenjualan;
