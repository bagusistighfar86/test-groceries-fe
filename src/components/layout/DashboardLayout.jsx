import React, { useEffect } from 'react';
import {
  Flex, useDisclosure,
} from '@chakra-ui/react';
import Sidebar from 'components/Sidebar';
import Navbar from 'components/Navbar';
import { getCookie } from 'utils/SetCookies';
import { useNavigate } from 'react-router-dom';

function DashboardLayout({ children }) {
  const navigate = useNavigate();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isOpenTablet,
    onOpen: onOpenTablet,
    onClose: onCloseTablet,
  } = useDisclosure();

  const auth = { accessToken: getCookie('accessToken') };

  useEffect(() => {
    if (!auth.accessToken) navigate('/login');
  }, []);

  if (auth.accessToken) {
    return (
      <Flex
        h="100vh"
        w="100%"
        spacing={0}
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
      >
        <Sidebar isOpen={isOpen} onOpen={onOpen} isOpenTablet={isOpenTablet} onCloseTablet={onCloseTablet} />
        <Navbar child={children} isOpen={isOpen} onOpen={onOpen} onClose={onClose} isOpenTablet={isOpenTablet} onOpenTablet={onOpenTablet} onCloseTablet={onCloseTablet} />
      </Flex>
    );
  }
}

export default DashboardLayout;
