import React from 'react';
import {
  Center, Container, Flex, Image, Spacer, Text, VStack,
} from '@chakra-ui/react';
import LogoSembako from 'assets/logo-sembako.png';

function LoginForgotPasswordLayout({ children, vector }) {
  return (
    <Container
      maxW="100%"
      h="100vh"
      overflow="hidden"
      px={7}
      py={{
        base: 16,
        lg: 7,
      }}
    >
      <Flex spacing={0} h="100%">
        <VStack
          flexBasis={{
            base: '100%',
            lg: '50%',
          }}
          alignItems="start"
        >
          <Image
            height="7%"
            alignSelf={{
              base: 'center',
              lg: 'start',
            }}
            src={LogoSembako}
          />
          <Spacer />

          {children}

          <Spacer />
          <Text color="basic.400" textAlign="center" alignSelf="center">© 2022 Sembako Express. All Rights Reserved</Text>
        </VStack>
        <Center
          bg="primary.300"
          flexBasis="50%"
          borderRadius={40}
          display={{
            base: 'none',
            lg: 'flex',
          }}
        >
          <Image src={vector} />
        </Center>
      </Flex>
    </Container>
  );
}

export default LoginForgotPasswordLayout;
