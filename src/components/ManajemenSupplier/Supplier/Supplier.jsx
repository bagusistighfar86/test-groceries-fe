import React, { useEffect, useReducer } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Select, Spacer, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr, Flex, Center, useToast, Button,
} from '@chakra-ui/react';
import Switch from 'react-switch';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useLocation, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faClock, faLock, faMapPin, faPenToSquare, faSort, faStore, faUser, faGear,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, SupplierReducer } from 'reducer/SupplierReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { useDispatch } from 'react-redux';
import { setPrevPath } from 'redux/reducer/navigationReducer';
import LoadingPage from 'views/LoadingPage';
import HapusDataSupplierIcon from './button/HapusDataSupplierIcon';
import HapusSelectedSupplierButton from './button/HapusSelectedSupplierButton';

function Supplier() {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const [state, dispatch] = useReducer(SupplierReducer, INITIAL_STATE);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataSupplier.every(checkSelect);

  const fetchGetAllSupplier = () => {
    axios({
      method: 'post',
      url: 'suppliers/filter',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        filter: [
          {
            key: 'is_approved',
            value: true,
          },
        ],
        limit: `${state.showRows}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      // copas ke seluruh
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `suppliers/filter?&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        filter: [
          {
            key: 'is_approved',
            value: true,
          },
        ],
        limit: `${state.showRows}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllSupplier();
    }
  }, [state.showRows]);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataSupplier.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const handleChangeStatus = (id, status) => {
    axios({
      method: 'PATCH',
      url: `suppliers/active/${id}`,
      headers: {
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const itemPosition = state.dataSupplier.map((item) => item.id).indexOf(id);
        dispatch({
          type: 'SET_STATUS',
          payload: {
            itemPos: itemPosition,
            data: !status,
          },
        });
        toast({
          title: 'Berhasil mengganti status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengganti status. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <HStack spacing={7} w="100%">
          <FormControl w={{ base: '40%', xl: '30%' }}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari berdasarkan nama"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>
          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <HapusSelectedSupplierButton
            noSelected={noSelected}
            accessToken={auth.accessToken}
            data={state.dataSupplier}
            dispatch={dispatch}
            selectedData={state.selectedDataSupplier}
            fetchGetAllSupplier={fetchGetAllSupplier}
          />
          <Link
            as={ReachLink}
            to="/supplier/tambah-supplier"
            onClick={() => dispatchRedux(setPrevPath(location.pathname))}
            bg="primary.500"
            variant="solid"
            fontSize="sm"
            px={5}
            py={2}
            borderRadius={5}
            color="basic.100"
            fontWeight="bold"
            _hover={{
              textDecoration: 'none',
              cursor: 'pointer',
              bg: 'primary.600',
            }}
          >
            Tambah Supplier
          </Link>
        </HStack>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <Center py={3}>
                  <Box w="10px" h="10px" bg="white" />
                </Center>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text ms={2}>ID SUPPLIER </Text>
                </HStack>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text ms={2}>NAMA</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faStore} />
                  <Text ms={2}>Perusahaan</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faMapPin} />
                  <Text display={{ base: 'none', lg: 'block' }}>Batas Kredit</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Batas</Text>
                    <Text>Kredit</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faClock} />
                  <Text display={{ base: 'none', lg: 'block' }}>Jatuh Tempo</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Jatuh</Text>
                    <Text>Tempo</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>Status</Text>
                </HStack>
              </Th>
              <Th textAlign="center">
                <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </Center>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataSupplier.map((item, index) => (
              <Tr key={item.id}>
                <Td>
                  <Center>
                    <Checkbox
                      colorScheme="primary"
                      isChecked={state.selectedDataSupplier[index].isSelect}
                      onChange={(e) => handleSelected(e, item.id)}
                    />
                  </Center>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/supplier/detail-supplier/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.fullname}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.company_name}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.max_credit)}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">
                    {item.set_due_date}
                    {' '}
                    hari
                  </Text>
                </Td>
                <Td>
                  <Switch
                    onChange={() => handleChangeStatus(item.id, item.is_active)}
                    checked={item.is_active}
                    uncheckedIcon={(
                      <Center h="100%" w={120} position="relative" right={16}>
                        <Text color="basic.100">Tidak Aktif</Text>
                      </Center>
                      )}
                    checkedIcon={(
                      <Center h="100%" w={120}>
                        <Text color="basic.100">Aktif</Text>
                      </Center>
                      )}
                    handleDiameter={20}
                    offColor="#CDCDCD"
                    onColor="#29B912"
                    height={30}
                    width={120}
                    className="react-switch"
                  />
                </Td>
                <Td textAlign="center">
                  <HStack justifyContent="center" spacing={5}>
                    <Link
                      as={ReachLink}
                      to={`/supplier/edit-supplier/${item.id}`}
                      color="basic.700"
                      _hover={{ color: 'basic.500', cursor: 'pointer' }}
                    >
                      <IconChakra icon={faPenToSquare} />
                    </Link>
                    <HapusDataSupplierIcon accessToken={auth.accessToken} id={item.id} data={state.dataSupplier} dispatch={dispatch} />
                  </HStack>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default Supplier;
