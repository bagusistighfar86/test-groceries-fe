import React, { useEffect, useReducer } from 'react';
import {
  Box, Flex, HStack, Text, VStack, chakra, Spacer, Button, useToast,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, SupplierReducer } from 'reducer/SupplierReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';

function DetailPersetujuanSupplier() {
  const toast = useToast();
  const navigate = useNavigate();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(SupplierReducer, INITIAL_STATE);

  const handleChangeStatus = (value) => {
    axios({
      method: 'PATCH',
      url: `suppliers/approve/${id}`,
      headers: {
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        is_approved: value,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil mengubah status persetujuan',
          position: 'top',
          status: 'success',
          isClosable: true,
        }); navigate(-1);
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengganti status. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetSupplierByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `suppliers/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetSupplierByID();
  }, []);

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />
            <HStack spacing={5} mt={12} display={state?.detailSupplier?.is_waiting_approval ? 'block' : 'none'}>
              <Button type="button" variant="outline" colorScheme="primary" px={10} onClick={() => handleChangeStatus(false)}>Tolak</Button>
              <Button type="button" variant="solid" colorScheme="primary" px={10} onClick={() => handleChangeStatus(true)}>Setujui</Button>
            </HStack>
          </HStack>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Supplier</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Nama Lengkap</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.fullname}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Nomor Telepon</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.phone}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Kelamin</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.gender}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">NIK / NPWP</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.nik_or_npwp}</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Perusahaan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Nama Perusahaan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.company_name}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Batas Kredit</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{ConvertMoneyIDR(state?.detailSupplier?.max_credit)}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jatuh Tempo</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.detailSupplier?.set_due_date}
                        {' '}
                        hari
                      </Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Catatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.description}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Negara</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.country}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Provinsi</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.province}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kota</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.city}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kecamatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.subdistrict}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kode Pos</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.postcode}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Alamat Lengkap</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.address}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tempat Gudang</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailSupplier?.warehouse?.name}</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default DetailPersetujuanSupplier;
