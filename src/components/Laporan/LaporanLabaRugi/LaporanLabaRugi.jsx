import React, { useEffect, useReducer, useState } from 'react';
import {
  chakra, Tbody, Td, Text,
  Table, TableContainer, Th, Thead, Tr, Flex, useToast, Button,
} from '@chakra-ui/react';
import { INITIAL_STATE, LaporanLabaRugiReducer } from 'reducer/Laporan/LaporanLabaRugiReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import { useNavigate } from 'react-router-dom';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function LaporanLabaRugi() {
  const navigate = useNavigate();
  const toast = useToast();
  const RangeDate = chakra(RangeDatepicker);
  const IconChakra = chakra(FontAwesomeIcon);

  const [state, dispatch] = useReducer(LaporanLabaRugiReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllLabaRugi = () => {
    axios({
      method: 'get',
      url: 'profitloss',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    fetchGetAllLabaRugi();
  }, []);

  const downloadFile = (link) => {
    const popout = window.open(link);
    window.setTimeout(() => {
      popout.close();
    }, 1000);
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />
        </Flex>

        <Button
          bg="primary.500"
          variant="solid"
          fontSize="sm"
          px={5}
          py={2}
          borderRadius={5}
          color="basic.100"
          fontWeight="bold"
          onClick={() => downloadFile('https://api-groceries.madjou.com/api/profitloss/export-pdf')}
          _hover={{
            textDecoration: 'none',
            cursor: 'pointer',
            bg: 'primary.600',
          }}
        >
          <IconChakra icon={faFilePdf} className="fa-xl" me={3} />
          <Text>Export PDF</Text>
        </Button>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th w="100%">
                <Text fontWeight="bold">Tipe</Text>
              </Th>
              <Th>
                <Text fontWeight="bold">Saldo</Text>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            <Tr>
              <Td w="100%" bg="basic.200" colSpan={2}>
                <Text fontWeight="bold">Pendapatan</Text>
              </Td>
            </Tr>
            <Tr>
              <Td>
                <Text fontWeight="medium" color="basic.700">Pendapatan</Text>
              </Td>
              <Td>
                <Text fontWeight="medium" color="basic.700">{state.dataLaporan.penjualan ? ConvertMoneyIDR(state?.dataLaporan?.penjualan) : 0}</Text>
              </Td>
            </Tr>
            <Tr>
              <Td>
                <Text fontWeight="bold" color="basic.700">Total Pendapatan</Text>
              </Td>
              <Td>
                <Text fontWeight="bold" color="basic.700">{state.dataLaporan.penjualan ? ConvertMoneyIDR(state?.dataLaporan?.penjualan) : 0}</Text>
              </Td>
            </Tr>

            <Tr>
              <Td w="100%" bg="basic.200" colSpan={2}>
                <Text fontWeight="bold">Beban Pokok</Text>
              </Td>
            </Tr>
            <Tr>
              <Td>
                <Text fontWeight="medium" color="basic.700">Pembelian</Text>
              </Td>
              <Td>
                <Text fontWeight="medium" color="basic.700">{state.dataLaporan.pembelian ? ConvertMoneyIDR(state?.dataLaporan?.pembelian) : 0}</Text>
              </Td>
            </Tr>
            <Tr>
              <Td>
                <Text fontWeight="bold" color="basic.700">Total Pembelian</Text>
              </Td>
              <Td>
                <Text fontWeight="bold" color="basic.700">{state.dataLaporan.pembelian ? ConvertMoneyIDR(state?.dataLaporan?.pembelian) : 0}</Text>
              </Td>
            </Tr>

            <Tr bg="basic.200">
              <Td>
                <Text fontWeight="bold" color="basic.700">Laba Bersih</Text>
              </Td>
              <Td>
                <Text fontWeight="bold" color="basic.700">{state.dataLaporan.penjualan ? ConvertMoneyIDR(state.dataLaporan.penjualan - state.dataLaporan.pembelian) : 0}</Text>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default LaporanLabaRugi;
