import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, HStack, Text, useToast, VStack, Flex, Button, TableContainer, Table, Thead,
  Tr, Th, Tbody, Td, Spacer,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCalendar, faCashRegister, faCoins, faTag, faUser, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useNavigate, useParams,
} from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { LaporanPenjualanReducer, INITIAL_STATE } from 'reducer/Laporan/LaporanPenjualanReducer';
import ConvertISOtoDate from 'utils/ConvertISOtoDate';

function DetailLaporanPenjualanSelesai() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(LaporanPenjualanReducer, INITIAL_STATE);

  const fetchGetDetailLaporanPenjualanSelesai = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: `report/sale/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetDetailLaporanPenjualanSelesai();
  }, []);

  // eslint-disable-next-line no-unused-vars
  const status = {
    penjualanSelesai:
  <Text fontWeight="semibold" color="success.400">Penjualan Selesai</Text>,
    penjualanRetur:
  <Text fontWeight="semibold" color="blue.400">Penjualan Retur</Text>,
    penjualanBatal:
  <Text fontWeight="semibold" color="error.400">Penjualan Batal</Text>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />
            <VStack>
              <Text fontWeight="bold">{ConvertISOtoDate(state.detailLaporan.date_of_sale)}</Text>
              {state.detailLaporan.status_id === 5 && status.penjualanSelesai}
              {state.detailLaporan.status_id === 6 && status.penjualanRetur}
              {state.detailLaporan.status_id === 7 && status.penjualanBatal}
            </VStack>
          </HStack>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Penjualan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Pelanggan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailLaporan?.customer.fullname}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Tujuan Pengiriman</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.detailLaporan?.customer.address}
                        {', '}
                        {state?.detailLaporan?.customer.subdistrict}
                        {', '}
                        {state?.detailLaporan?.customer.city}
                        {', '}
                        {state?.detailLaporan?.customer.province}
                        {', '}
                        {state?.detailLaporan?.customer.country}
                        {', '}
                        {state?.detailLaporan?.customer.postcode}
                      </Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Jual</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailLaporan?.date_of_sale}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="center">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Pembayaran</Text>
                    </Box>
                    <Box
                      w={{
                        base: '50%',
                        lg: '60%',
                      }}
                    >
                      <Box w="fit-content" px={3} borderRadius={5} bg={state?.detailLaporan.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                        <Text color="basic.100" fontWeight="semibold">{state?.detailLaporan.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                      </Box>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Kendaraan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailLaporan?.transportation_type?.name}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Kirim</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailLaporan?.delivery_date}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Catatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailLaporan?.description}</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Pembayaran</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCalendar} />
                          <Text ms={2}>Tanggal</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCoins} />
                          <Text ms={2}>Nominal</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faUser} />
                          <Text ms={2}>Dikelola</Text>
                        </HStack>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailLaporan?.payments.length === 0 ? (
                      <Tr>
                        <Td colspan="99" textAlign="center">
                          <Text fontWeight="semibold" color="basic.700">Tidak ada data</Text>
                        </Td>
                      </Tr>
                    )
                      : state?.detailLaporan?.payments.map((item) => (
                        <Tr key={item.id}>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{item.date}</Text>
                          </Td>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total_pay)}</Text>
                          </Td>
                          <Td>
                            <Text fontWeight="semibold" color="basic.700">{item.user_name ? item.user_name : '-'}</Text>
                          </Td>
                        </Tr>
                      ))}
                  </Tbody>
                </Table>
              </TableContainer>
            </VStack>

          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Penjualan</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailLaporan?.sale_details.map((item) => (
                      <Tr key={item.id}>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.products.sell_price)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.qty}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products.warehouse.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subtotal)}</Text>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state?.detailLaporan?.total)}</Text>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default DetailLaporanPenjualanSelesai;
