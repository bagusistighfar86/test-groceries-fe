import React, { useEffect, useReducer, useState } from 'react';
import {
  chakra, HStack, Tbody, Td, Text, Select, Spacer, Table,
  TableContainer, Th, Thead, Tr, Flex, useToast, Link, Box, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faList, faCalendar, faSort, faCoins, faUser, faGear, faCircleInfo, faCashRegister, faFileExcel,
} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { INITIAL_STATE, LaporanPembelianReducer } from 'reducer/Laporan/LaporanPembelianReducer';

function LaporanPembelian() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(LaporanPembelianReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllLaporanPembelian = () => {
    axios({
      method: 'get',
      url: `purchases?limit=${state.showRows}&type=10,11,12`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });

        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `purchases?limit=${state.showRows}&type=10,11,12&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllLaporanPembelian();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const status = {
    pembelianSelesai:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="primary.100">
    <Text color="primary.600" fontWeight="semibold">Selesai</Text>
  </Box>,
    pembelianRetur:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="blue.100">
    <Text color="blue.600" fontWeight="semibold">Retur</Text>
  </Box>,
    pembelianBatal:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="red.100">
    <Text color="red.600" fontWeight="semibold">Batal</Text>
  </Box>,
  };

  const getLinkDetailByStatus = (statusID, itemID) => {
    if (statusID === 10) return `/laporan/laporan-pembelian/detail-laporan-selesai/${itemID}`;
    if (statusID === 11) return `/laporan/laporan-pembelian/detail-laporan-retur/${itemID}`;
    if (statusID === 12) return `/pembelian/pembelian-batal/detail-pembelian/${itemID}`;
    return `/laporan/laporan-pembelian/detail-laporan-selesai/${itemID}`;
  };

  const downloadFile = (link) => {
    const popout = window.open(link);
    window.setTimeout(() => {
      popout.close();
    }, 1000);
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />

          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <Button
            bg="primary.500"
            variant="solid"
            fontSize="sm"
            px={5}
            py={2}
            borderRadius={5}
            color="basic.100"
            fontWeight="bold"
            onClick={() => downloadFile('https://api-groceries.madjou.com/api/purchase/export')}
            _hover={{
              textDecoration: 'none',
              cursor: 'pointer',
              bg: 'primary.600',
            }}
          >
            <IconChakra icon={faFileExcel} className="fa-xl" me={3} />
            <Text>Export Excel</Text>
          </Button>
        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text ms={2}>SUPPLIER</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text display={{ base: 'none', lg: 'block' }}>TANGGAL PROSES</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>TANGGAL</Text>
                    <Text>PROSES</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faList} />
                  <Text display={{ base: 'none', lg: 'block' }}>NOMOR REFERENSI</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>NOMOR </Text>
                    <Text>REFERENSI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>STATUS</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCoins} />
                  <Text display={{ base: 'none', lg: 'block' }}>TOTAL HARGA</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>TOTAL</Text>
                    <Text>HARGA</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCashRegister} />
                  <Text display={{ base: 'none', lg: 'block' }}>JENIS PEMBAYARAN</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>JENIS</Text>
                    <Text>PEMBAYARAN</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataLaporan.map((item) => (
              <Tr key={item.id}>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.supplier.fullname}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.date_of_purchase}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.id}</Text>
                </Td>
                <Td>
                  {item.status_id === 10 && status.pembelianSelesai}
                  {item.status_id === 11 && status.pembelianRetur}
                  {item.status_id === 12 && status.pembelianBatal}
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total)}</Text>
                </Td>
                <Td textAlign="center">
                  <Box w="fit-content" px={3} py={2} borderRadius={5} bg={item.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                    <Text color="basic.100" fontWeight="semibold">{item.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                  </Box>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={getLinkDetailByStatus(item.status_id, item.id)}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    Detail
                  </Link>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default LaporanPembelian;
