import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Select, Text, useToast, VStack, Flex, Button, InputGroup, InputRightElement,
} from '@chakra-ui/react';
import { faArrowLeft, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Avvvatars from 'avvvatars-react';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import React, { useReducer } from 'react';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { INITIAL_STATE, KaryawanReducer } from 'reducer/KaryawanReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import useKaryawanFormValidator from 'utils/useKaryawanFormValidator';

function AddKaryawan() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const jenisKelamin = [
    { id: '1', gender: 'Laki - Laki' },
    { id: '2', gender: 'Perempuan' },
  ];

  const roles = [
    { id: 1, role: 'Super Admin' },
    { id: 2, role: 'Admin Gudang' },
    { id: 3, role: 'Admin Sales' },
  ];

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(KaryawanReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useKaryawanFormValidator(state.form);

  const handleShowPassword = () => dispatch({ type: 'SET_SHOW_PASSWORD' });

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',

      payload: {
        name: 'form',
        data: nextFormState,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchAddKaryawan = async () => {
    await axios({
      method: 'post',
      url: 'employees',
      data: {
        name: state.form.nama,
        phone: state.form.telepon,
        gender: state.form.jenisKelamin,
        date_of_birth: state.form.tanggalLahir,
        nik_or_npwp: state.form.nik,
        address: state.form.alamat,
        email: state.form.email,
        jabatan: state.form.jabatan,
        role_id: state.form.role,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitFormAddKaryawan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchAddKaryawan();
    navigate('/manajemen-karyawan');
  };

  const onSubmitAddFormKaryawanAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddKaryawan();
    navigate(0);
  };

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>
          <HStack w="100%" borderBottom="1px solid" borderColor="basic.200" py={10}>
            <Box flexBasis="20%">
              <Text fontWeight="bold" fontSize="xl">Foto Profil</Text>
            </Box>
            <Box w="80%">
              <Avvvatars value="Username" size={100} />
            </Box>
          </HStack>
          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Diri</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Nama Lengkap</FormLabel>
                      <Input
                        type="text"
                        name="nama"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.nama ? state.form.nama : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.nama.dirty && errors.nama.error ? (
                      <Text color="red" my={2}>{errors.nama.message}</Text>
                    ) : null}
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="telepon"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.telepon ? state.form.telepon : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.telepon.dirty && errors.telepon.error ? (
                      <Text color="red" my={2}>{errors.telepon.message}</Text>
                    ) : null}
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Jenis Kelamin</FormLabel>
                      <Select
                        name="jenisKelamin"
                        placeholder="-"
                        value={state.form.jenisKelamin ? state.form.jenisKelamin : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {jenisKelamin.map((item) => (
                          <option key={item.id} value={item.gender}>{item.gender}</option>
                        ))}
                      </Select>
                      {errors.jenisKelamin.dirty && errors.jenisKelamin.error ? (
                        <Text color="red" my={2}>{errors.jenisKelamin.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Lahir</FormLabel>
                      <Input
                        name="tanggalLahir"
                        type="date"
                        value={state.form.tanggalLahir ? state.form.tanggalLahir : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.tanggalLahir.dirty && errors.tanggalLahir.error ? (
                        <Text color="red" my={2}>{errors.tanggalLahir.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">NIK / NPWP</FormLabel>
                      <Input
                        name="nik"
                        type="text"
                        w="100%"
                        value={state.form.nik ? state.form.nik : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.nik.dirty && errors.nik.error ? (
                        <Text color="red" my={2}>{errors.nik.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Alamat</FormLabel>
                      <Input
                        name="alamat"
                        type="text"
                        w="100%"
                        value={state.form.alamat ? state.form.alamat : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.alamat.dirty && errors.alamat.error ? (
                        <Text color="red" my={2}>{errors.alamat.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Email</FormLabel>
                      <Input
                        name="email"
                        type="email"
                        w="100%"
                        value={state.form.email ? state.form.email : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.email.dirty && errors.email.error ? (
                        <Text color="red" my={2}>{errors.email.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Kata Sandi</FormLabel>
                      <InputGroup>
                        <Input
                          type={state.showPassword ? 'text' : 'password'}
                          name="password"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          value={state.form.password ? state.form.password : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                        <InputRightElement width="4.5rem">
                          <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowPassword}>
                            {state.showPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                          </Button>
                        </InputRightElement>
                      </InputGroup>
                      {errors.password.dirty && errors.password.error ? (
                        <Text color="red" my={2}>{errors.password.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Jabatan</FormLabel>
                      <Input
                        name="jabatan"
                        type="text"
                        w="100%"
                        value={state.form.jabatan ? state.form.jabatan : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jabatan.dirty && errors.jabatan.error ? (
                        <Text color="red" my={2}>{errors.jabatan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Role</FormLabel>
                      <Select
                        name="role"
                        placeholder="-"
                        value={state.form.role ? state.form.role : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {roles.map((item) => (
                          <option key={item.id} value={item.id}>{item.role}</option>
                        ))}
                      </Select>
                      {errors.role.dirty && errors.role.error ? (
                        <Text color="red" my={2}>{errors.role.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Button
                  type="button"
                  onClick={onSubmitAddFormKaryawanAgain}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Simpan dan Buat Lagi
                </Button>
                <Button
                  type="button"
                  onClick={onSubmitFormAddKaryawan}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddKaryawan;
