import React, { useEffect, useReducer } from 'react';
import {
  Box, Flex, HStack, Text, VStack, Link, chakra, Spacer, Button, useToast, Tag,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import Avvvatars from 'avvvatars-react';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { INITIAL_STATE, KaryawanReducer } from 'reducer/KaryawanReducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import axios from 'axios';
import LoadingPage from 'views/LoadingPage';
import HapusDataKaryawanButton from './button/HapusDataKaryawanButton';

function DetailKaryawan() {
  const toast = useToast();
  const navigate = useNavigate();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(KaryawanReducer, INITIAL_STATE);

  const fetchGetKaryawanByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `employees/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetKaryawanByID();
  }, []);

  useEffect(() => {
    const form = {
      nama: state.detailKaryawan?.name,
      telepon: state.detailKaryawan?.phone,
      jenisKelamin: state.detailKaryawan?.gender,
      tanggalLahir: state.detailKaryawan?.date_of_birth,
      nik: state.detailKaryawan?.nik_or_npwp,
      alamat: state.detailKaryawan?.address,
      email: state.detailKaryawan?.email,
      jabatan: state.detailKaryawan?.jabatan,
      role: state.detailKaryawan?.user?.role_id,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });
  }, [state.detailKaryawan]);

  const RolesTag = {
    admin: <Tag bg="purple.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>SUPER ADMIN</Tag>,
    gudang: <Tag bg="blue.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>ADMIN GUDANG</Tag>,
    sales: <Tag bg="pink.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>ADMIN SALES</Tag>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />
            <HStack spacing={5} mt={12}>
              <HapusDataKaryawanButton accessToken={auth.accessToken} id={id} />
              <Link
                as={ReachLink}
                to={`/manajemen-karyawan/edit-karyawan/${id}`}
                bg="primary.500"
                variant="solid"
                fontSize="sm"
                px={5}
                py={2}
                borderRadius={5}
                color="basic.100"
                fontWeight="bold"
                _hover={{
                  textDecoration: 'none',
                  cursor: 'pointer',
                  bg: 'primary.600',
                }}
              >
                Edit Data
              </Link>
            </HStack>
          </HStack>
          <HStack w="100%" borderBottom="1px solid" borderColor="basic.200" py={10}>
            <Box flexBasis="20%">
              <Text fontWeight="bold" fontSize="xl">Foto Profil</Text>
            </Box>
            <Box w="80%">
              <Avvvatars value="Username" size={100} />
            </Box>
          </HStack>
          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Diri</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Nama Lengkap</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.nama}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Nomor Telepon</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.telepon}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Kelamin</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.jenisKelamin}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Tanggal Lahir</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.tanggalLahir}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">NIK / NPWP</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.nik}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Alamat</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.alamat}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Email</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.email}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Password</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Link
                        as={ReachLink}
                        to="/change-password"
                        color="secondary.500"
                        fontWeight="bold"
                        textDecoration="underline"
                        _hover={{ cursor: 'pointer' }}
                      >
                        Ubah Password
                      </Link>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jabatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.form?.jabatan}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Role</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.form?.role === 1 && RolesTag.admin}
                        {state?.form?.role === 2 && RolesTag.gudang}
                        {state?.form?.role === 3 && RolesTag.sales}
                      </Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default DetailKaryawan;
