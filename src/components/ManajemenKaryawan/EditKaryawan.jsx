import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Select, Text, useToast, VStack, Flex, Button,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Avvvatars from 'avvvatars-react';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import React, { useEffect, useReducer } from 'react';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { INITIAL_STATE, KaryawanReducer } from 'reducer/KaryawanReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import useKaryawanFormValidator from 'utils/useKaryawanFormValidator';

function EditKaryawan() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const jenisKelamin = [
    { id: '1', gender: 'Laki - Laki' },
    { id: '2', gender: 'Perempuan' },
  ];

  const roles = [
    { id: 1, role: 'Super Admin' },
    { id: 2, role: 'Admin Gudang' },
    { id: 3, role: 'Admin Sales' },
  ];

  const [state, dispatch] = useReducer(KaryawanReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useKaryawanFormValidator(state.formEditKaryawan);

  const fetchGetKaryawanByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `employees/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetKaryawanByID();
  }, []);

  useEffect(() => {
    const formEditKaryawan = {
      ...state.formEditKaryawan,
      nama: state.detailKaryawan?.name,
      telepon: state.detailKaryawan?.phone,
      jenisKelamin: state.detailKaryawan?.gender,
      tanggalLahir: state.detailKaryawan?.date_of_birth,
      nik: state.detailKaryawan?.nik_or_npwp,
      alamat: state.detailKaryawan?.address,
      email: state.detailKaryawan?.user?.email,
      jabatan: state.detailKaryawan?.jabatan,
      role: state.detailKaryawan?.user?.role_id,
      isActive: state.detailKaryawan?.is_active,
    };

    dispatch({
      type: 'SET_FORM',
      payload: {
        name: 'formEditKaryawan',
        data: formEditKaryawan,
      },
    });
  }, [state.detailKaryawan]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formEditKaryawan,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: {
        name: 'formEditKaryawan',
        data: nextFormState,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          formEditKaryawan: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchEditBarang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'put',
      url: `employees/${id}`,
      data: {
        name: state.formEditKaryawan.nama,
        phone: state.formEditKaryawan.telepon,
        gender: state.formEditKaryawan.jenisKelamin,
        date_of_birth: state.formEditKaryawan.tanggalLahir,
        nik_or_npwp: state.formEditKaryawan.nik,
        address: state.formEditKaryawan.alamat,
        email: state.formEditKaryawan.email,
        jabatan: state.formEditKaryawan.jabatan,
        role_id: state.formEditKaryawan.role,
        is_active: state.formEditKaryawan.isActive,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        navigate('/manajemen-karyawan');
        toast({
          title: 'Data berhasil diubah',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gagal diubah',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitFormEditKaryawan = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formEditKaryawan, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEditBarang();
  };

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>
          <HStack w="100%" borderBottom="1px solid" borderColor="basic.200" py={10}>
            <Box flexBasis="20%">
              <Text fontWeight="bold" fontSize="xl">Foto Profil</Text>
            </Box>
            <Box w="80%">
              <Avvvatars value="Username" size={100} />
            </Box>
          </HStack>
          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Diri</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Nama Lengkap</FormLabel>
                      <Input
                        type="text"
                        name="nama"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.formEditKaryawan.nama ? state.formEditKaryawan.nama : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.nama.dirty && errors.nama.error ? (
                      <Text color="red" my={2}>{errors.nama.message}</Text>
                    ) : null}
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="telepon"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.formEditKaryawan.telepon ? state.formEditKaryawan.telepon : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.telepon.dirty && errors.telepon.error ? (
                      <Text color="red" my={2}>{errors.telepon.message}</Text>
                    ) : null}
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Jenis Kelamin</FormLabel>
                      <Select
                        name="jenisKelamin"
                        placeholder="-"
                        value={state.formEditKaryawan.jenisKelamin ? state.formEditKaryawan.jenisKelamin : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {jenisKelamin.map((item) => (
                          <option key={item.id} value={item.gender}>{item.gender}</option>
                        ))}
                      </Select>
                      {errors.jenisKelamin.dirty && errors.jenisKelamin.error ? (
                        <Text color="red" my={2}>{errors.jenisKelamin.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Lahir</FormLabel>
                      <Input
                        name="tanggalLahir"
                        type="date"
                        value={state.formEditKaryawan.tanggalLahir ? state.formEditKaryawan.tanggalLahir : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.tanggalLahir.dirty && errors.tanggalLahir.error ? (
                        <Text color="red" my={2}>{errors.tanggalLahir.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">NIK / NPWP</FormLabel>
                      <Input
                        name="nik"
                        type="text"
                        w="100%"
                        value={state.formEditKaryawan.nik ? state.formEditKaryawan.nik : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.nik.dirty && errors.nik.error ? (
                        <Text color="red" my={2}>{errors.nik.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Alamat</FormLabel>
                      <Input
                        name="alamat"
                        type="text"
                        w="100%"
                        value={state.formEditKaryawan.alamat ? state.formEditKaryawan.alamat : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.alamat.dirty && errors.alamat.error ? (
                        <Text color="red" my={2}>{errors.alamat.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Email</FormLabel>
                      <Input
                        isDisabled
                        name="email"
                        type="email"
                        w="100%"
                        value={state.formEditKaryawan.email ? state.formEditKaryawan.email : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.email.dirty && errors.email.error ? (
                        <Text color="red" my={2}>{errors.email.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Jabatan</FormLabel>
                      <Input
                        name="jabatan"
                        type="text"
                        w="100%"
                        value={state.formEditKaryawan.jabatan ? state.formEditKaryawan.jabatan : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jabatan.dirty && errors.jabatan.error ? (
                        <Text color="red" my={2}>{errors.jabatan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Role</FormLabel>
                      <Select
                        name="role"
                        placeholder="-"
                        value={state.formEditKaryawan.role ? state.formEditKaryawan.role : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {roles.map((item) => (
                          <option key={item.id} value={item.id}>{item.role}</option>
                        ))}
                      </Select>
                      {errors.role.dirty && errors.role.error ? (
                        <Text color="red" my={2}>{errors.role.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Button
                  as={ReachLink}
                  onClick={() => navigate(-1)}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Batal
                </Button>
                <Button
                  type="button"
                  onClick={onSubmitFormEditKaryawan}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditKaryawan;
