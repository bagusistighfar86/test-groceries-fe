import React, { useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Select, Text, useToast, VStack, Flex, Button, InputLeftAddon, InputGroup,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { INITIAL_STATE, PelangganReducer } from 'reducer/PelangganReducer';
import usePelangganFormValidator from 'utils/usePelangganFormValidator';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';

function AddPelanggan() {
  const prevPath = getCookie('prevPath');
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const auth = { accessToken: getCookie('accessToken') };

  const jenisKelamin = [
    { id: '1', gender: 'Laki - Laki' },
    { id: '2', gender: 'Perempuan' },
  ];

  const [state, dispatch] = useReducer(PelangganReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = usePelangganFormValidator(state.form);

  const fetchAddPelanggan = async () => {
    await axios({
      method: 'post',
      url: 'customers',
      data: {
        fullname: state.form.namaLengkap,
        phone: state.form.telepon,
        gender: state.form.jenisKelamin,
        nik_or_npwp: state.form.nik,
        store_name: state.form.namaToko,
        max_credit: state.form.batasKredit,
        set_due_date: state.form.jatuhTempo,
        description: state.form.catatan,
        country: state.form.negara,
        province: state.form.provinsi,
        city: state.form.kota,
        subdistrict: state.form.kecamatan,
        postcode: state.form.kodePos,
        address: state.form.alamatToko,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormAddPelanggan = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddPelanggan();
    if (prevPath === '/penjualan/tambah-penjualan') navigate(prevPath);
    else if (prevPath === '/pelanggan') navigate('/pelanggan');
  };

  const onSubmitAddFormPelangganAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddPelanggan();
    navigate(0);
  };
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Pelanggan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Lengkap</FormLabel>
                      <Input
                        type="text"
                        name="namaLengkap"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaLengkap ? state.form.namaLengkap : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaLengkap.dirty && errors.namaLengkap.error ? (
                        <Text color="red" my={2}>{errors.namaLengkap.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="telepon"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.telepon ? state.form.telepon : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.telepon.dirty && errors.telepon.error ? (
                        <Text color="red" my={2}>{errors.telepon.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jenis Kelamin</FormLabel>
                      <Select
                        name="jenisKelamin"
                        placeholder="-"
                        value={state.form.jenisKelamin ? state.form.jenisKelamin : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {jenisKelamin.map((item) => (
                          <option key={item.id} value={item.gender}>{item.gender}</option>
                        ))}
                      </Select>
                      {errors.jenisKelamin.dirty && errors.jenisKelamin.error ? (
                        <Text color="red" my={2}>{errors.jenisKelamin.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">NIK / NPWP</FormLabel>
                      <Input
                        name="nik"
                        type="text"
                        w="100%"
                        value={state.form.nik ? state.form.nik : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.nik.dirty && errors.nik.error ? (
                        <Text color="red" my={2}>{errors.nik.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Toko</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Toko</FormLabel>
                      <Input
                        type="text"
                        name="namaToko"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaToko ? state.form.namaToko : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaToko.dirty && errors.namaToko.error ? (
                        <Text color="red" my={2}>{errors.namaToko.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Batas Kredit</FormLabel>
                      <InputGroup>
                        <InputLeftAddon>Rp</InputLeftAddon>
                        <Input
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          name="batasKredit"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          value={state.form.batasKredit ? state.form.batasKredit : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </InputGroup>
                    </FormControl>
                    {errors.batasKredit.dirty && errors.batasKredit.error ? (
                      <Text color="red" my={2}>{errors.batasKredit.message}</Text>
                    ) : null}
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jatuh Tempo (hari)</FormLabel>
                      <Input
                        type="number"
                        onWheel={(e) => e.target.blur()}
                        name="jatuhTempo"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.jatuhTempo ? state.form.jatuhTempo : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jatuhTempo.dirty && errors.jatuhTempo.error ? (
                        <Text color="red" my={2}>{errors.jatuhTempo.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Catatan</FormLabel>
                      <Input
                        type="text"
                        name="catatan"
                        value={state.form.catatan ? state.form.catatan : ''}
                        onChange={onUpdateField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Negara</FormLabel>
                      <Input
                        name="negara"
                        type="text"
                        w="100%"
                        value={state.form.negara ? state.form.negara : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.negara.dirty && errors.negara.error ? (
                        <Text color="red" my={2}>{errors.negara.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Provinsi</FormLabel>
                      <Input
                        type="text"
                        name="provinsi"
                        w="100%"
                        value={state.form.provinsi ? state.form.provinsi : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.provinsi.dirty && errors.provinsi.error ? (
                        <Text color="red" my={2}>{errors.provinsi.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Kota</FormLabel>
                      <Input
                        type="text"
                        name="kota"
                        w="100%"
                        value={state.form.kota ? state.form.kota : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.kota.dirty && errors.kota.error ? (
                        <Text color="red" my={2}>{errors.kota.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Kecamatan</FormLabel>
                      <Input
                        type="text"
                        name="kecamatan"
                        w="100%"
                        value={state.form.kecamatan ? state.form.kecamatan : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.kecamatan.dirty && errors.kecamatan.error ? (
                        <Text color="red" my={2}>{errors.kecamatan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Kode Pos</FormLabel>
                      <Input
                        type="text"
                        name="kodePos"
                        w="100%"
                        value={state.form.kodePos ? state.form.kodePos : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.kodePos.dirty && errors.kodePos.error ? (
                      <Text color="red" my={2}>{errors.kodePos.message}</Text>
                    ) : null}
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Alamat Toko</FormLabel>
                      <Input
                        type="text"
                        name="alamatToko"
                        w="100%"
                        value={state.form.alamatToko ? state.form.alamatToko : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.alamatToko.dirty && errors.alamatToko.error ? (
                        <Text color="red" my={2}>{errors.alamatToko.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Button
                  type="button"
                  onClick={onSubmitAddFormPelangganAgain}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Simpan dan Buat Lagi
                </Button>
                <Button
                  type="button"
                  onClick={onSubmitFormAddPelanggan}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddPelanggan;
