import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Select, Text, useToast, VStack, Flex, Button, InputGroup, InputLeftAddon, InputRightAddon, Tooltip,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { INITIAL_STATE, BarangReducer } from 'reducer/BarangReducer';
import useBarangFormValidator from 'utils/useBarangFormValidator';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';

function EditBarang() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(BarangReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useBarangFormValidator(state.form);

  const fetchGetKategoriProduk = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: 'product_categories',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataKategoriProduk',
            data: res.data.data,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetSupplier = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: 'suppliers',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataSupplier',
            data: res.data.data,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetGudang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: 'warehouses',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataGudang',
            data: res.data.data,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetMerek = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: 'brands',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataMerek',
            data: res.data.data,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetBarangByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `products/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetKategoriProduk();
    fetchGetSupplier();
    fetchGetGudang();
    fetchGetMerek();
    fetchGetBarangByID();
  }, []);

  const fetchEditBarang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'put',
      url: `products/${id}`,
      data: {
        name: state.form.namaBarang,
        sell_price: state.form.hargaJual,
        purchase_price: state.form.hargaBeli,
        stock: state.form.stokBarang,
        limit_stock: state.form.limitStok,
        product_category_id: state.form.kategoriProdukID,
        supplier_id: state.form.supplierID,
        warehouse_id: state.form.gudangID,
        brand_id: state.form.merekID,
        barcode: state.form.barcodeBarang,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        navigate('/master-barang');
        toast({
          title: 'Data berhasil diubah',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gagal diubah',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    const form = {
      namaBarang: state.detailBarang?.name,
      hargaJual: state.detailBarang?.sell_price,
      hargaBeli: state.detailBarang?.purchase_price,
      stokBarang: state.detailBarang?.stock,
      limitStok: state.detailBarang?.limit_stock,
      kategoriProdukID: state.detailBarang?.product_category_id,
      supplierID: state.detailBarang?.supplier_id,
      gudangID: state.detailBarang?.warehouse_id,
      merekID: state.detailBarang?.brand_id,
      barcodeBarang: state.detailBarang?.barcode,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });
  }, [state.detailBarang]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditBarang = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEditBarang();
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Barang</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                      <Input
                        type="text"
                        name="namaBarang"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaBarang ? state.form.namaBarang : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaBarang.dirty && errors.namaBarang.error ? (
                        <Text color="red" my={2}>{errors.namaBarang.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Kategori</FormLabel>
                      <Select
                        name="kategoriProdukID"
                        placeholder="-"
                        value={state.form.kategoriProdukID ? state.form.kategoriProdukID : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {state.dataKategoriProduk.map((item) => (
                          <option key={item.id} value={item.id}>{item.name}</option>
                        ))}
                      </Select>
                      {errors.kategoriProdukID.dirty && errors.kategoriProdukID.error ? (
                        <Text color="red" my={2}>{errors.kategoriProdukID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Harga Jual /pcs</FormLabel>
                      <InputGroup>
                        <InputLeftAddon>Rp</InputLeftAddon>
                        <Input
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          name="hargaJual"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          value={state.form.hargaJual ? state.form.hargaJual : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </InputGroup>
                      {errors.hargaJual.dirty && errors.hargaJual.error ? (
                        <Text color="red" my={2}>{errors.hargaJual.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jumlah Stok</FormLabel>
                      <InputGroup>
                        <Input
                          isDisabled
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          name="stokBarang"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          defaultValue={state.form.stokBarang}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                        <InputRightAddon>pcs</InputRightAddon>
                      </InputGroup>
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Barcode barang</FormLabel>
                      <Tooltip
                        label="Kosongkan isian apabila barang belum memiliki barcode. Barcode akan secara otomatis dibuat oleh sistem setelah barang tersimpan"
                        closeDelay={500}
                        hasArrow
                        arrowSize={15}
                        placement="bottom-start"
                        arrowPadding={200}
                      >
                        <Input
                          name="barcodeBarang"
                          type="text"
                          w="100%"
                          value={state.form.barcodeBarang ? state.form.barcodeBarang : ''}
                          onChange={onUpdateField}
                          onBlur={onBlurField}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </Tooltip>
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Limit Stok</FormLabel>
                      <InputGroup>
                        <Input
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          name="limitStok"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          value={state.form.limitStok ? state.form.limitStok : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                        <InputRightAddon>pcs</InputRightAddon>
                      </InputGroup>
                      {errors.limitStok.dirty && errors.limitStok.error ? (
                        <Text color="red" my={2}>{errors.limitStok.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Merek Barang</FormLabel>
                      <Input
                        type="text"
                        name="merekID"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.merekID ? state.form.merekID : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.merekID.dirty && errors.merekID.error ? (
                        <Text color="red" my={2}>{errors.merekID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Harga Beli /pcs</FormLabel>
                      <InputGroup>
                        <InputLeftAddon>Rp</InputLeftAddon>
                        <Input
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          name="hargaBeli"
                          w="100%"
                          onBlur={onBlurField}
                          onChange={onUpdateField}
                          value={state.form.hargaBeli ? state.form.hargaBeli : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </InputGroup>
                      {errors.hargaBeli.dirty && errors.hargaBeli.error ? (
                        <Text color="red" my={2}>{errors.hargaBeli.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Supplier Barang</FormLabel>
                      <Select
                        name="supplierID"
                        placeholder="-"
                        value={state.form.supplierID ? state.form.supplierID : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {state.dataSupplier.map((item) => (
                          <option key={item.id} value={item.id}>{item.fullname}</option>
                        ))}
                      </Select>
                      {errors.supplierID.dirty && errors.supplierID.error ? (
                        <Text color="red" my={2}>{errors.supplierID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Tempat Gudang</FormLabel>
                      <Select
                        name="gudangID"
                        placeholder="-"
                        value={state.form.gudangID ? state.form.gudangID : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {state.dataGudang.map((item) => (
                          <option key={item.id} value={item.id}>{item.name}</option>
                        ))}
                      </Select>
                      {errors.gudangID.dirty && errors.gudangID.error ? (
                        <Text color="red" my={2}>{errors.gudangID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>

                <HStack spacing={5} mt={12}>
                  <Button
                    as={ReachLink}
                    onClick={() => navigate(-1)}
                    border="1px solid"
                    borderColor="primary.500"
                    variant="outline"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.700"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                    }}
                  >
                    Batal
                  </Button>
                  <Button
                    type="button"
                    onClick={onSubmitFormEditBarang}
                    bg="primary.500"
                    variant="solid"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.100"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                      bg: 'primary.600',
                    }}
                  >
                    Simpan
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditBarang;
