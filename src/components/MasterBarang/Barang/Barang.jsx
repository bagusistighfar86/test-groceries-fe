import React, { useEffect, useReducer } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Select, Spacer, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr, Flex, Center, useToast, Button,
} from '@chakra-ui/react';
import Switch from 'react-switch';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useLocation, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faLock, faPenToSquare, faSort, faUser, faGear, faBoxOpen, faList, faTag, faBoxesStacked,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, BarangReducer } from 'reducer/BarangReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { setPrevPath } from 'redux/reducer/navigationReducer';
import { useDispatch } from 'react-redux';
import HapusDataBarangIcon from './button/HapusDataBarangIcon';
import HapusSelectedBarangButton from './button/HapusSelectedBarangButton';

function Barang() {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const [state, dispatch] = useReducer(BarangReducer, INITIAL_STATE);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataBarang.every(checkSelect);

  const fetchGetAllBarang = () => {
    axios({
      method: 'get',
      url: `products?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `products?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllBarang();
    }
  }, [state.showRows]);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataBarang.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const handleChangeStatus = (id, status) => {
    axios({
      method: 'PATCH',
      url: `products/active/${id}`,
      headers: {
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const itemPosition = state.dataBarang.map((item) => item.id).indexOf(id);
        dispatch({
          type: 'SET_STATUS',
          payload: {
            itemPos: itemPosition,
            data: !status,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengganti status. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <HStack spacing={7} w="100%">
          <FormControl w={{ base: '40%', xl: '30%' }}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari berdasarkan nama"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>
          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <HapusSelectedBarangButton
            noSelected={noSelected}
            accessToken={auth.accessToken}
            data={state.dataBarang}
            dispatch={dispatch}
            selectedData={state.selectedDataBarang}
            fetchGetAllBarang={fetchGetAllBarang}
          />
          <Link
            as={ReachLink}
            to="/master-barang/tambah-barang"
            onClick={() => dispatchRedux(setPrevPath(location.pathname))}
            bg="primary.500"
            variant="solid"
            fontSize="sm"
            px={5}
            py={2}
            borderRadius={5}
            color="basic.100"
            fontWeight="bold"
            _hover={{
              textDecoration: 'none',
              cursor: 'pointer',
              bg: 'primary.600',
            }}
          >
            Tambah Barang
          </Link>
        </HStack>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <Center py={3}>
                  <Box w="10px" h="10px" bg="white" />
                </Center>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text ms={2}>ID BARANG </Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxOpen} />
                  <Text display={{ base: 'none', lg: 'block' }}>Nama Barang</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Nama</Text>
                    <Text>Barang</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faList} />
                  <Text display={{ base: 'none', lg: 'block' }}>Kategori Barang</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Kategori</Text>
                    <Text>Barang</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faTag} />
                  <Text display={{ base: 'none', lg: 'block' }}>Harga Jual</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Harga</Text>
                    <Text>Jual</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text ms={2}>Supplier</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>Status</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>Stok Barang</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>Stok</Text>
                    <Text>Barang</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th textAlign="center">
                <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </Center>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataBarang.map((item, index) => (
              <Tr key={item.id}>
                <Td>
                  <Center>
                    <Checkbox
                      colorScheme="primary"
                      isChecked={state.selectedDataBarang[index].isSelect}
                      onChange={(e) => handleSelected(e, item.id)}
                    />
                  </Center>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/master-barang/detail-barang/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.name}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.product_category?.name}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.sell_price}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.supplier?.fullname}</Text>
                </Td>
                <Td>
                  <Switch
                    onChange={() => handleChangeStatus(item.id, item.is_active)}
                    checked={item.is_active}
                    uncheckedIcon={(
                      <Center h="100%" w={120} position="relative" right={16}>
                        <Text color="basic.100">Tidak Aktif</Text>
                      </Center>
                    )}
                    checkedIcon={(
                      <Center h="100%" w={120}>
                        <Text color="basic.100">Aktif</Text>
                      </Center>
                    )}
                    handleDiameter={20}
                    offColor="#CDCDCD"
                    onColor="#29B912"
                    height={30}
                    width={120}
                    className="react-switch"
                  />
                </Td>
                <Td>
                  <Text textAlign="center" fontWeight="semibold" color="basic.700">{item.stock}</Text>
                </Td>
                <Td textAlign="center">
                  <HStack justifyContent="center" spacing={5}>
                    <Link
                      as={ReachLink}
                      to={`/master-barang/edit-barang/${item.id}`}
                      color="basic.700"
                      _hover={{ color: 'basic.500', cursor: 'pointer' }}
                    >
                      <IconChakra icon={faPenToSquare} />
                    </Link>
                    <HapusDataBarangIcon accessToken={auth.accessToken} id={item.id} data={state.dataBarang} dispatch={dispatch} />
                  </HStack>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default Barang;
