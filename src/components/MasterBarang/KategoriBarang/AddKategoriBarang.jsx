import React, { useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text, useToast, VStack, Flex, Button,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { INITIAL_STATE, KategoriBarangReducer } from 'reducer/KategoriBarangReducer';
import useKategoriBarangFormValidator from 'utils/useKategoriBarangFormValidator';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';

function AddKategoriBarang() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(KategoriBarangReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useKategoriBarangFormValidator(state.form);

  const fetchAddKategoriBarang = async () => {
    await axios({
      method: 'post',
      url: 'product_categories',
      data: {
        name: state.form.namaKategoriBarang,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormAddKategoriBarang = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchAddKategoriBarang();
    navigate('/master-barang');
  };

  const onSubmitAddFormKategoriBarangAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddKategoriBarang();
    navigate(0);
  };
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Kategori Barang</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Kategori</FormLabel>
                      <Input
                        type="text"
                        name="namaKategoriBarang"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaKategoriBarang ? state.form.namaKategoriBarang : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaKategoriBarang.dirty && errors.namaKategoriBarang.error ? (
                        <Text color="red" my={2}>{errors.namaKategoriBarang.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack spacing={5} mt={12}>
                  <Button
                    type="button"
                    onClick={onSubmitAddFormKategoriBarangAgain}
                    border="1px solid"
                    borderColor="primary.500"
                    variant="outline"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.700"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                    }}
                  >
                    Simpan dan Buat Lagi
                  </Button>
                  <Button
                    type="button"
                    onClick={onSubmitFormAddKategoriBarang}
                    bg="primary.500"
                    variant="solid"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.100"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                      bg: 'primary.600',
                    }}
                  >
                    Simpan
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddKategoriBarang;
