/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Button, Center, Heading, Modal, ModalBody, ModalContent, ModalOverlay, Box, Text, ModalFooter, VStack, chakra,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { Link as ReachLink, useNavigate } from 'react-router-dom';

function SubmitChangePasswordButton({ isLoading, isOpen }) {
  const IconChakra = chakra(FontAwesomeIcon);
  const navigate = useNavigate();

  return (
    <>
      <Button
        type="button"
        py={7}
        borderRadius="full"
        colorScheme={isLoading ? 'basic' : 'primary'}
        variant="solid"
        w="100%"
        fontSize="lg"
        boxShadow="rgba(0, 0, 0, 0.15) 0px 12px 21px"
      >
        Submit
      </Button>

      <Modal
        isOpen={isOpen}
        isCentered
        size="md"
      >
        <ModalOverlay />
        <ModalContent borderRadius={20} p={5}>
          <ModalBody>
            <Center flexDir="column">
              <Box bg="primary.200" borderRadius="full" p={5} mb={5}>
                <IconChakra icon={faCheck} size="2x" />
              </Box>
              <Heading fontSize="2xl" fontWeight="extrabold" mb={5}>Update Berhasil</Heading>
              <Text
                textAlign="center"
                fontSize={{
                  base: 'md',
                  lg: 'lg',
                }}
              >
                Kata sandi Anda telah berhasil diatur ulang
              </Text>
            </Center>
          </ModalBody>
          <ModalFooter>
            <VStack
              spacing={5}
              color="secondary.500"
              fontWeight="bold"
              w="100%"
              fontSize={{
                base: 'md',
                lg: 'lg',
              }}
            >
              <Button as={ReachLink} colorScheme="primary" borderRadius="full" px={24} onClick={() => navigate('/login')}>Login</Button>
            </VStack>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default SubmitChangePasswordButton;
