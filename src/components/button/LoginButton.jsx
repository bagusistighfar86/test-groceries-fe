/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Box,
  Button, Center, chakra, Heading, Modal, ModalBody, ModalContent, ModalOverlay, Spinner,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

function LoginButton({ isLoading, isOpen, fetchLogin }) {
  const IconChakra = chakra(FontAwesomeIcon);
  return (
    <>
      <Button
        type="submit"
        onClick={fetchLogin}
        py={7}
        borderRadius="full"
        colorScheme={isLoading ? 'basic' : 'primary'}
        variant="solid"
        w="100%"
        fontSize="lg"
        boxShadow="rgba(0, 0, 0, 0.15) 0px 12px 21px"
      >
        {isLoading ? <Spinner /> : 'Masuk'}
      </Button>

      <Modal
        isOpen={isOpen}
        isCentered
        size="xs"
      >
        <ModalOverlay />
        <ModalContent borderRadius={20}>
          <ModalBody py={10}>
            <Center flexDir="column">
              <Box bg="primary.200" borderRadius="full" p={5} mb={5}>
                <IconChakra icon={faCheck} size="2x" />
              </Box>
              <Heading fontSize="2xl" fontWeight="extrabold">Sukses Masuk</Heading>
            </Center>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default LoginButton;
