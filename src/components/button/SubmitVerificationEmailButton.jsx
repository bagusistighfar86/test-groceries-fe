/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Button, Center, Heading, Modal, ModalBody, ModalContent, ModalOverlay, Image, Box, Text, ModalFooter, VStack, Link,
} from '@chakra-ui/react';
import EmailVector from 'assets/email-vector.png';

function SubmitVerificationEmailButton({ isOpen }) {
  return (
    <>
      <Button
        type="submit"
        py={7}
        borderRadius="full"
        colorScheme="primary"
        variant="solid"
        w="100%"
        fontSize="lg"
        boxShadow="rgba(0, 0, 0, 0.15) 0px 12px 21px"
      >
        Minta tautan setel ulang kata sandi
      </Button>

      <Modal
        isOpen={isOpen}
        isCentered
        size={{
          base: 'lg',
          lg: 'md',
        }}
      >
        <ModalOverlay />
        <ModalContent borderRadius={20} p={5}>
          <ModalBody>
            <Center flexDir="column">
              <Box h="100px" mb={10}>
                <Image src={EmailVector} h="100%" />
              </Box>
              <Heading size="lg" fontWeight="extrabold" mb={5}>Lupa Kata Sandi</Heading>
              <Text
                textAlign="center"
                fontSize={{
                  base: 'md',
                  lg: 'lg',
                }}
                mb={5}
              >
                Anda akan menerima email dengan tautan untuk mengatur ulang kata sandi Anda. Silakan periksa kotak masuk Anda.

              </Text>
            </Center>
          </ModalBody>
          <ModalFooter>
            <VStack
              spacing={5}
              color="secondary.500"
              fontWeight="bold"
              w="100%"
              fontSize={{
                base: 'md',
                lg: 'lg',
              }}
            >
              <Link to="/">Kirim ulang link email</Link>
              <Link to="/">Ubah email</Link>
            </VStack>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default SubmitVerificationEmailButton;
