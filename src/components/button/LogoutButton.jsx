import React from 'react';
import {
  Button, Link, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import { useNavigate } from 'react-router-dom';

function LogoutButton() {
  const navigate = useNavigate();
  const toast = useToast;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const auth = { accessToken: getCookie('accessToken') };

  const fetchLogout = () => {
    axios({
      method: 'post',
      url: 'logout',
      headers: {
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then(async (res) => {
      if (res.status >= 200 && res.status < 300) {
        await deleteAllCookies();
        window.location.reload();
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Logout gagal.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  return (
    <>
      <Link to="/" w="100%" py={1} onClick={onOpen} _hover={{ textDecor: 'none' }}>Logout</Link>

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader textAlign="center" fontSize="xl">Log Out</ModalHeader>
          <ModalBody textAlign="center" fontSize="lg">
            <Text color="basic.500">Apakah anda yakin ingin keluar?</Text>
          </ModalBody>
          <ModalFooter>
            <Button border="2px solid" borderColor="basic.200" borderRadius={10} bg="none" onClick={onClose} me={2} px={5}>Close</Button>
            <Button colorScheme="error" borderRadius={10} px={5} onClick={fetchLogout}>Keluar</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default LogoutButton;
