/* eslint-disable no-unused-expressions */
import React from 'react';
import {
  Box,
  Center,
  chakra,
  Drawer,
  DrawerContent,
  DrawerOverlay,
  Flex,
  HStack,
  Image, Link, Popover, PopoverContent, PopoverTrigger, Text, VStack,
} from '@chakra-ui/react';
import {
  faBook,
  faBoxOpen, faChartSimple, faChevronRight, faClipboardCheck, faHouse, faMoneyBill, faTruckFast, faUserGroup, faUserPlus, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import LogoSembakoPutih from 'assets/logo-sembako-putih.png';
import SmallLogoSembakoPutih from 'assets/small-logo-sembako-putih.png';
import { AnimatePresence, motion } from 'framer-motion';
import { Link as ReachLink, NavLink as RouterLink } from 'react-router-dom';

function Sidebar({
  isOpen, onOpen, isOpenTablet, onCloseTablet,
}) {
  const IconChakra = chakra(FontAwesomeIcon);

  const SideItem = [
    {
      label: 'Home', icon: faHouse, to: '/', isNested: false,
    },
    {
      label: 'Manajemen Karyawan', icon: faUserGroup, to: '/manajemen-karyawan', isNested: false,
    },
    {
      label: 'Transaksi',
      icon: faClipboardCheck,
      to: '/pembelian',
      isNested: true,
      nested: [
        { label: 'Pembelian', to: '/pembelian' },
        { label: 'Penjualan', to: '/penjualan' },
        { label: 'Pencatatan Piutang', to: '/pencatatan-piutang' },
        { label: 'Pencatatan Hutang', to: '/pencatatan-hutang' },
      ],
    },
    {
      label: 'Manajemen Gudang',
      icon: faWarehouse,
      to: '/manajemen-gudang',
      isNested: true,
      nested: [
        { label: 'Manajemen Gudang', to: '/manajemen-gudang' },
        { label: 'Rincian Stok', to: '/rincian-stok' },
      ],
    },
    {
      label: 'Pelanggan dan Supplier',
      icon: faUserPlus,
      to: '/pelanggan',
      isNested: true,
      nested: [
        { label: 'Pelanggan', to: '/pelanggan' },
        { label: 'Supplier', to: '/supplier' },
      ],
    },
    {
      label: 'Master Barang', icon: faBoxOpen, to: '/master-barang', isNested: false,
    },
    {
      label: 'Keuangan', icon: faMoneyBill, to: '/keuangan', isNested: false,
    },
    {
      label: 'Laporan', icon: faChartSimple, to: '/laporan', isNested: false,
    },
    {
      label: 'Distribusi',
      icon: faTruckFast,
      to: '/manajemen-driver',
      isNested: true,
      nested: [
        { label: 'Manajemen Driver', to: '/manajemen-driver' },
        { label: 'Manajemen Transportasi', to: '/manajemen-transportasi' },
        { label: 'Tracking Distribusi', to: '/tracking-distribusi' },
      ],
    },
    {
      label: 'Jurnal', icon: faBook, to: '/jurnal', isNested: false,
    },
  ];

  const showAnimation = {
    hidden: {
      opacity: 0,
      display: 'none',
      x: '100px',
      y: '-20px',
      transition: {
        duration: 1,
        type: 'spring',
      },
    },
    show: {
      opacity: 1,
      display: 'block',
      x: '0px',
      y: '-20px',
      transition: {
        duration: 1,
        type: 'spring',
      },
    },
  };
  return (
    <>
      <TabletSidebar
        SideItem={SideItem}
        isOpenTablet={isOpenTablet}
        onCloseTablet={onCloseTablet}
        IconChakra={IconChakra}
      />
      <Box
        display={{
          base: 'none',
          lg: 'block',
        }}
        h="100vh"
        bg="primary.500"
      >
        <AnimatePresence initial={false}>
          <motion.div
            animate={{
              width: isOpen ? '300px' : '105px',
              transition: {
                duration: 0.1,
                type: 'spring',
                damping: 10,
              },
            }}
          >
            <Center py={5} display="flex" justifyContent="center" borderBottomWidth="1px" borderColor="whiteAlpha.400">
              <Image src={isOpen ? LogoSembakoPutih : SmallLogoSembakoPutih} h="50px" />
            </Center>
            <VStack mt={5} px={5}>
              <AnimatePresence initial={false}>
                {SideItem.map((item) => (
                  <Box key={item.label} w="100%">
                    {item.isNested
                      ? (
                        <Popover trigger={isOpen ? 'hover' : ''} placement="right-start" offset={[0, -10]}>
                          <PopoverTrigger>
                            <Flex
                              overflow="hidden"
                              w="100%"
                              borderBottom="1px solid"
                              justifyContent="center"
                              alignItems="center"
                              borderColor="blackAlpha.200"
                              borderRadius={8}
                              color="basic.100"
                              onClick={onOpen}
                              _activeLink={{
                                bg: 'basic.100',
                                color: 'basic.900',
                                boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                              }}
                              _hover={{
                                cursor: 'pointer',
                                bg: 'basic.100',
                                color: 'basic.900',
                                boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                              }}
                            >
                              <Center w="100%" h="100%" justifyContent="start" px={6} py={3} position="relative">
                                <Center w="30px">
                                  <IconChakra icon={item.icon} className="fa-sm" />
                                </Center>
                                {isOpen
                                  && (
                                  <motion.div
                                    variants={showAnimation}
                                    initial="hidden"
                                    animate="show"
                                  >
                                    <HStack position="absolute" top={2} w="200px" display={isOpen ? 'flex' : 'none'} fontWeight="500">
                                      <Text>
                                        {item.label}
                                      </Text>
                                      <IconChakra icon={faChevronRight} className="fa-sm" />
                                    </HStack>
                                  </motion.div>
                                  )}
                              </Center>
                              {isOpen
                                && (
                                <PopoverContent borderRadius="0 8px 8px 8px" bg="basic.100" borderColor="white" overflow="hidden" w="100%">
                                  {item.isNested
                                    && item.nested.map((nestLink) => (
                                      <Link
                                        key={nestLink.label}
                                        as={ReachLink}
                                        to={nestLink.to}
                                        overflow="hidden"
                                        w="100%"
                                        display="flex"
                                        justifyContent="center"
                                        alignItems="center"
                                        color="basic.900"
                                        _hover={{
                                          cursor: 'pointer',
                                          bg: 'primary.200',
                                        }}
                                      >
                                        <Center w="100%" h="100%" justifyContent="start" px={10} py={3} position="relative">
                                          <Text>{nestLink.label}</Text>
                                        </Center>
                                      </Link>
                                    ))}
                                </PopoverContent>
                                )}
                            </Flex>
                          </PopoverTrigger>
                        </Popover>
                      ) : (
                        <Link
                          as={RouterLink}
                          to={item.to}
                          end
                          overflow="hidden"
                          w="100%"
                          borderBottom="1px solid"
                          display="flex"
                          justifyContent="center"
                          alignItems="center"
                          borderColor="blackAlpha.200"
                          borderRadius={8}
                          color="basic.100"
                          _activeLink={{
                            bg: 'basic.100',
                            color: 'basic.900',
                            boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                          }}
                          _hover={{
                            cursor: 'pointer',
                            bg: 'basic.100',
                            color: 'basic.900',
                            boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                          }}
                        >
                          <Center w="100%" h="100%" justifyContent="start" px={6} py={3} position="relative" onClick={onOpen}>
                            <Center w="30px">
                              <IconChakra icon={item.icon} className="fa-sm" />
                            </Center>
                            {isOpen
                              && (
                              <motion.div
                                variants={showAnimation}
                                initial="hidden"
                                animate="show"
                              >
                                <HStack position="absolute" top={2} w="200px" display={isOpen ? 'flex' : 'none'} fontWeight="500">
                                  <Text>{item.label}</Text>
                                </HStack>
                              </motion.div>
                              )}
                          </Center>
                        </Link>
                      )}
                  </Box>
                ))}
              </AnimatePresence>
            </VStack>
          </motion.div>
        </AnimatePresence>
      </Box>
    </>
  );
}

function TabletSidebar({
  SideItem, isOpenTablet, onCloseTablet, IconChakra,
}) {
  return (
    <Drawer
      isOpen={isOpenTablet}
      placement="left"
      onClose={onCloseTablet}
    >
      <DrawerOverlay />
      <DrawerContent
        h="100vh"
        width="300px"
        bg="primary.500"
      >
        <Center py={5} display="flex" justifyContent="center" borderBottomWidth="1px" borderColor="whiteAlpha.400">
          <Image src={LogoSembakoPutih} h="50px" />
        </Center>
        <VStack mt={5} px={5}>
          {SideItem.map((item) => (
            <Box key={item.label} w="100%">
              {item.isNested
                ? (
                  <Popover trigger="click" placement="right-start" offset={[0, -10]}>
                    <PopoverTrigger>
                      <Flex
                        overflow="hidden"
                        w="100%"
                        borderBottom="1px solid"
                        justifyContent="center"
                        alignItems="center"
                        borderColor="blackAlpha.200"
                        borderRadius={8}
                        color="basic.100"
                        _activeLink={{
                          bg: 'basic.100',
                          color: 'basic.900',
                          boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                        }}
                        _hover={{
                          cursor: 'pointer',
                          bg: 'basic.100',
                          color: 'basic.900',
                          boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                        }}
                      >
                        <Center w="100%" h="100%" justifyContent="start" px={6} py={3}>
                          <Center w="30px">
                            <IconChakra icon={item.icon} className="fa-sm" />
                          </Center>
                          <HStack w="200px" fontWeight="500">
                            <Text>
                              {item.label}
                            </Text>
                            <IconChakra icon={faChevronRight} className="fa-sm" />
                          </HStack>
                        </Center>
                        <PopoverContent borderRadius="0 8px 8px 8px" bg="basic.100" borderColor="white" overflow="hidden" w="100%">
                          {item.isNested
                            && item.nested.map((nestLink) => (
                              <Link
                                key={nestLink.label}
                                as={ReachLink}
                                to={nestLink.to}
                                overflow="hidden"
                                w="100%"
                                display="flex"
                                justifyContent="center"
                                alignItems="center"
                                color="basic.900"
                                _hover={{
                                  cursor: 'pointer',
                                  bg: 'primary.200',
                                }}
                              >
                                <Center w="100%" h="100%" justifyContent="start" px={10} py={3} position="relative">
                                  <Text>{nestLink.label}</Text>
                                </Center>
                              </Link>
                            ))}
                        </PopoverContent>
                      </Flex>
                    </PopoverTrigger>
                  </Popover>
                ) : (
                  <Link
                    as={RouterLink}
                    to={item.to}
                    end
                    overflow="hidden"
                    w="100%"
                    borderBottom="1px solid"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    borderColor="blackAlpha.200"
                    borderRadius={8}
                    color="basic.100"
                    _activeLink={{
                      bg: 'basic.100',
                      color: 'basic.900',
                      boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',
                    }}
                    _hover={{
                      cursor: 'pointer',
                      bg: 'basic.100',
                      color: 'basic.900',
                      boxShadow: 'rgba(0, 0, 0, 0.13) -28px 22px 45px',

                    }}
                  >
                    <Center w="100%" h="100%" justifyContent="start" px={6} py={3}>
                      <Center w="30px">
                        <IconChakra icon={item.icon} className="fa-sm" />
                      </Center>
                      <HStack w="200px" fontWeight="500">
                        <Text>{item.label}</Text>
                      </HStack>
                    </Center>
                  </Link>
                )}
            </Box>
          ))}
        </VStack>
      </DrawerContent>
    </Drawer>
  );
}

export default Sidebar;
