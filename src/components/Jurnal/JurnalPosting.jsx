import React, { useEffect, useReducer, useState } from 'react';
import {
  chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr, Flex, useToast, Box, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faList, faBoxesStacked, faCalendar, faSort, faCoins, faBookOpen, faRightToBracket, faRightFromBracket,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, JurnalReducer } from 'reducer/JurnalReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';

function JurnalPosting() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(JurnalReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllJurnal = () => {
    axios({
      method: 'get',
      url: `journals?limit=${state.showRows}&is_posting=1`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `journals?limit=${state.showRows}&is_posting=1&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllJurnal();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const kategoriTransaksi = {
    hutang:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="primary.100">
    <Text color="primary.600" fontWeight="semibold">Hutang</Text>
  </Box>,
    penjualanRetur:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="blue.100">
    <Text color="blue.600" fontWeight="semibold">Penjualan Retur</Text>
  </Box>,
    pembelian:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="purple.100">
    <Text color="purple.600" fontWeight="semibold">Pembelian</Text>
  </Box>,

    piutang:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="secondary.100">
    <Text color="secondary.600" fontWeight="semibold">Piutang</Text>
  </Box>,
    pembelianRetur:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="blue.100">
    <Text color="blue.600" fontWeight="semibold">Pembelian Retur</Text>
  </Box>,
    penjualan:
  <Box w="fit-content" px={3} py={2} borderRadius={5} bg="pink.100">
    <Text color="pink.600" fontWeight="semibold">Penjualan</Text>
  </Box>,
  };

  const keterangan = {
    masuk:
  <HStack color="success.500">
    <IconChakra icon={faRightToBracket} me={3} />
    <Text>Kas Masuk</Text>
  </HStack>,
    keluar:
  <HStack color="error.500">
    <IconChakra icon={faRightFromBracket} me={3} />
    <Text>Kas Keluar</Text>
  </HStack>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />
          <IconChakra icon={faSort} className="fa-xl" />
        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text ms={2}>Tanggal</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faList} />
                  <Text display={{ base: 'none', lg: 'block' }}>NOMOR REFERENSI</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>NOMOR </Text>
                    <Text>REFERENSI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>KATEGORI TRANSAKSI</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>KATEGORI</Text>
                    <Text>TRANSAKSI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCoins} />
                  <Text ms={2}>DEBIT</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCoins} />
                  <Text ms={2}>KREDIT</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBookOpen} />
                  <Text ms={2}>KETERANGAN</Text>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataJurnal.map((item) => (
              <Tr key={item.id}>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.date}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.reference_id}</Text>
                </Td>
                <Td>
                  {item?.reference_type.toLowerCase() === 'pembayaran piutang' && kategoriTransaksi.piutang}
                  {item?.reference_type.toLowerCase() === 'pembelian retur' && kategoriTransaksi.pembelianRetur}
                  {item?.reference_type.toLowerCase() === 'penjualan' && kategoriTransaksi.penjualan}

                  {item?.reference_type.toLowerCase() === 'pembayaran hutang' && kategoriTransaksi.hutang}
                  {item?.reference_type.toLowerCase() === 'penjualan retur' && kategoriTransaksi.penjualanRetur}
                  {item?.reference_type.toLowerCase() === 'pembelian' && kategoriTransaksi.pembelian}
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.debit ? ConvertMoneyIDR(item.debit) : ''}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.kredit ? ConvertMoneyIDR(item.kredit) : ''}</Text>
                </Td>
                <Td>
                  {item.debit ? keterangan.masuk : keterangan.keluar}
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default JurnalPosting;
