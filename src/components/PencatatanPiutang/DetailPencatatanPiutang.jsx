import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, HStack, Text, useToast, VStack,
  Flex, Button, TableContainer, Table, Thead,
  Tr, Th, Tbody, Td, Spacer, Link,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCashRegister, faTag, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useNavigate, useParams,
} from 'react-router-dom';
import { PencatatanPiutangReducer, INITIAL_STATE } from 'reducer/PencatatanPiutangReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import ConvertISOtoDate from 'utils/ConvertISOtoDate';
import BayarAngsuranButton from './button/BayarAngsuranButton';

function DetailPencatatanPiutang() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(PencatatanPiutangReducer, INITIAL_STATE);

  const setData = (dataBaru) => {
    const form = {
      ...state.formBayarAngsuran,
      kodePenjualan: dataBaru.sale_id,
      sisaTagihan: dataBaru.total,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });
  };

  const fetchGetDetailPencatatanPiutang = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: `credit/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });

        setData(dataBaru);
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetDetailPencatatanPiutang();
  }, []);

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
            <Spacer />
            <Text fontWeight="bold">{state.detailPiutang?.sale_id}</Text>
          </HStack>

          <Flex
            flexDir="column"
            w="100%"
            py={10}
          >
            <VStack w="100%" bg="primary.50" borderRadius={18} py={4} fontWeight="bold">
              <Text fontSize="2xl">Sisa Tagihan</Text>
              <Text fontSize="4xl">{ConvertMoneyIDR(state?.detailPiutang?.total)}</Text>
              <Text color="basic.500">
                Jatuh Tempo
                {' '}
                {state?.detailPiutang?.due_date}
              </Text>
            </VStack>

            <HStack w="100%" mt={3}>
              <BayarAngsuranButton state={state} dispatch={dispatch} />
            </HStack>

            <Box mt={3} textAlign="center">
              <Link
                as={ReachLink}
                to={`/pencatatan-piutang/detail-piutang/riwayat-pembayaran/${state?.detailPiutang?.id}`}
                color="secondary.500"
                fontWeight="bold"
                textDecoration="underline"
                _hover={{ cursor: 'pointer' }}
                textAlign="center"
              >
                Riwayat Pembayaran
              </Link>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Penjualan</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Pelanggan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailPiutang?.sale?.customer.fullname}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Nomor Telepon</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailPiutang?.sale?.customer.phone}</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jatuh Tempo</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.detailPiutang?.sale?.customer?.set_due_date}
                        {' '}
                        hari
                      </Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Batas Kredit</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {ConvertMoneyIDR(state?.detailPiutang?.sale?.customer?.max_credit)}
                      </Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Jual</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{ConvertISOtoDate(state?.detailPiutang?.sale?.date_of_sale)}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="center">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Pembayaran</Text>
                    </Box>
                    <Box
                      w={{
                        base: '50%',
                        lg: '60%',
                      }}
                    >
                      <Box w="fit-content" px={3} borderRadius={5} bg={state?.detailPiutang?.sale?.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                        <Text color="basic.100" fontWeight="semibold">{state?.detailPiutang?.sale?.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                      </Box>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Catatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailPiutang?.sale?.description}</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Penjualan</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailPiutang?.sale?.sale_details.map((item) => (
                      <Tr key={item.id}>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.products.sell_price)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.qty}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.products.warehouse.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subtotal)}</Text>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state?.detailPiutang?.sale?.total)}</Text>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default DetailPencatatanPiutang;
