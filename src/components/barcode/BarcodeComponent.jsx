import {
  Button, Center, Modal, ModalBody, ModalCloseButton, ModalContent, ModalOverlay, useDisclosure,
} from '@chakra-ui/react';
import React from 'react';
import Barcode from 'react-barcode/lib/react-barcode';

function BarcodeComponent({
  val, h, w, fs,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button onClick={onOpen} colorScheme="none">
        <Barcode value={val} height={h} width={w} fontSize={fs} />
      </Button>

      <Modal onClose={onClose} isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalCloseButton />
          <ModalBody>
            <Center>
              <Barcode value={val} />
            </Center>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default BarcodeComponent;
