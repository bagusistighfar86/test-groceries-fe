import React, { useEffect, useState } from 'react';
import {
  Center,
  chakra,
  Heading,
  HStack, Link, Popover, PopoverBody, PopoverContent, PopoverTrigger, Spacer, Text, VStack,
} from '@chakra-ui/react';
import {
  faBars, faChevronDown,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnimatePresence, motion } from 'framer-motion';
import { faBell, faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import LogoutButton from 'components/button/LogoutButton';
import Avvvatars from 'avvvatars-react';
import { getCookie } from 'utils/SetCookies';

function Navbar({
  child, isOpen, onOpen, onClose, isOpenTablet, onOpenTablet, onCloseTablet,
}) {
  const navigate = useNavigate();
  const auth = { accessToken: getCookie('accessToken') };

  useEffect(() => {
    if (!auth.accessToken) navigate('/login');
  }, []);

  // eslint-disable-next-line no-unused-vars
  const [navbarName, setNavbarName] = useState('');

  useEffect(() => {
    setNavbarName(getCookie('navName'));
  });

  const IconChakra = chakra(FontAwesomeIcon);
  return (
    <>
      <TabletNavbar
        child={child}
        isOpenTablet={isOpenTablet}
        onOpenTablet={onOpenTablet}
        onCloseTablet={onCloseTablet}
        IconChakra={IconChakra}
      />
      <AnimatePresence initial={false}>
        <motion.div
          animate={{
            width: isOpen ? 'calc(100% - 300px)' : 'calc(100% - 105px)',
            transition: {
              duration: 0.1,
              type: 'spring',
              damping: 10,
            },
          }}
        >
          <VStack
            display={{
              base: 'none',
              lg: 'flex',
            }}
            h="100vh"
            alignItems="start"
            overflow="hidden"
            spacing={0}
          >
            <HStack w="100%" py={6} borderBottomWidth="1px" borderColor="blackAlpha.200" h="91px">
              <Center onClick={isOpen ? onClose : onOpen} m={3} fontSize="2xl" _hover={{ cursor: 'pointer', bg: 'none' }}>
                <IconChakra icon={faBars} />
              </Center>
              <Heading fontSize="3xl">Groceries</Heading>
              <Spacer />
              <HStack spacing={10}>
                <Center _hover={{ cursor: 'pointer', bg: 'none' }}>
                  <IconChakra icon={faQuestionCircle} className="fa-lg" />
                </Center>
                <Center _hover={{ cursor: 'pointer', bg: 'none' }}>
                  <IconChakra icon={faBell} className="fa-lg" />
                </Center>
                <Center pe={5}>
                  <Avvvatars value="Username" size={45} />
                  <VStack
                    alignItems="start"
                    mx={3}
                  >
                    <Text fontSize="xs">Username</Text>
                    <Text fontSize="xs">Role User</Text>
                  </VStack>
                  <Popover placement="bottom-end" offset={[0, 30]}>
                    <PopoverTrigger>
                      <IconChakra icon={faChevronDown} className="fa-sm" _hover={{ cursor: 'pointer' }} />
                    </PopoverTrigger>
                    <PopoverContent w="100%">
                      <PopoverBody w="150px">
                        <VStack alignItems="start">
                          <Link as={ReachLink} to="/profile" w="100%" py={1} _hover={{ textDecor: 'none' }}>Profile</Link>
                          <LogoutButton />
                        </VStack>
                      </PopoverBody>
                    </PopoverContent>
                  </Popover>
                </Center>
              </HStack>
            </HStack>
            {child}
          </VStack>
        </motion.div>
      </AnimatePresence>
    </>
  );
}

function TabletNavbar({
  child, isOpenTablet, onOpenTablet, onCloseTablet, IconChakra,
}) {
  return (
    <VStack
      display={{
        base: 'flex',
        lg: 'none',
      }}
      h="100vh"
      w="100%"
      alignItems="start"
      overflow="hidden"
      spacing={0}
    >
      <HStack w="100%" py={6} borderBottomWidth="1px" borderColor="blackAlpha.200" h="91px">
        <Center onClick={isOpenTablet ? onCloseTablet : onOpenTablet} m={3} _hover={{ cursor: 'pointer', bg: 'none' }} fontSize="2xl">
          <IconChakra icon={faBars} />
        </Center>
        <Heading fontSize="3xl">Navbar</Heading>
        <Spacer />
        <HStack spacing={10}>
          <Center _hover={{ cursor: 'pointer', bg: 'none' }}>
            <IconChakra icon={faQuestionCircle} className="fa-xl" />
          </Center>
          <Center _hover={{ cursor: 'pointer', bg: 'none' }}>
            <IconChakra icon={faBell} className="fa-xl" />
          </Center>
          <Center pe={5}>
            <Avvvatars value="Username" size={45} />
            <Popover placement="bottom-end" offset={[0, 30]}>
              <PopoverTrigger>
                <IconChakra icon={faChevronDown} className="fa-sm" ms={3} _hover={{ cursor: 'pointer' }} />
              </PopoverTrigger>
              <PopoverContent w="100%">
                <PopoverBody w="150px">
                  <VStack alignItems="start">
                    <Link as={ReachLink} to="/profile" w="100%" py={1} _hover={{ textDecor: 'none' }}>Profile</Link>
                    <LogoutButton />
                  </VStack>
                </PopoverBody>
              </PopoverContent>
            </Popover>
          </Center>
        </HStack>
      </HStack>
      {child}
    </VStack>
  );
}

export default Navbar;
