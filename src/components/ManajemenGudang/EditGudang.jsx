import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text, useToast, VStack, Flex, Button,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import useGudangFormValidator from 'utils/useGudangFormValidator';
import { GudangReducer, INITIAL_STATE } from 'reducer/GudangReducer';
import LoadingPage from 'views/LoadingPage';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import Select from 'react-select';

function EditGudang() {
  const SelectChakra = chakra(Select);
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(GudangReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useGudangFormValidator(state.form);

  const fetchGetAllGudangEmployee = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'employees-warehouse',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataStaffGudang',
            data: dataBaru.map((item) => ({ value: item.id, label: item.name })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetAllGudangEmployee();
  }, []);

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    const selectedStaffGudang = {
      value: e.value, label: e.label,
    };

    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedStaffGudang,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchGetGudangByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `warehouses/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });

        const selectedStaffGudang = {
          value: dataBaru.employee_id, label: dataBaru.employee.name,
        };
        dispatch({
          type: 'SET_SELECTED_SELECT',
          payload: {
            dataName: 'selectedStaffGudang',
            data: selectedStaffGudang,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetGudangByID();
  }, []);

  const fetchEditGudang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'put',
      url: `warehouses/${id}`,
      data: {
        name: state.form.namaGudang,
        phone: state.form.teleponGudang,
        employee_id: state.form.staffGudangID,
        address: state.form.alamatGudang,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Data berhasil diubah',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        navigate('/manajemen-gudang');
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gagal diubah',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    const form = {
      namaGudang: state.detailGudang?.name,
      teleponGudang: state.detailGudang?.phone,
      staffGudangID: state.detailGudang?.employee_id,
      alamatGudang: state.detailGudang?.address,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });
  }, [state.detailGudang]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditGudang = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEditGudang();
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Gudang</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Gudang</FormLabel>
                      <Input
                        type="text"
                        name="namaGudang"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaGudang ? state.form.namaGudang : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaGudang.dirty && errors.namaGudang.error ? (
                        <Text color="red" my={2}>{errors.namaGudang.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Staff Gudang</FormLabel>
                      <SelectChakra
                        name="staffGudangID"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'staffGudangID', 'selectedStaffGudang')}
                        onBlur={(e) => onBlurField(e, 'staffGudangID')}
                        value={state.selectedStaffGudang}
                        options={state.dataStaffGudang}
                        styles={customStyles}
                        w="100%"
                        placeholder="Select supplier"
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.staffGudangID.dirty && errors.staffGudangID.error ? (
                        <Text color="red" my={2}>{errors.staffGudangID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="teleponGudang"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.teleponGudang ? state.form.teleponGudang : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.teleponGudang.dirty && errors.teleponGudang.error ? (
                        <Text color="red" my={2}>{errors.teleponGudang.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Alamat</FormLabel>
                      <Input
                        type="text"
                        name="alamatGudang"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.alamatGudang ? state.form.alamatGudang : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.alamatGudang.dirty && errors.alamatGudang.error ? (
                        <Text color="red" my={2}>{errors.alamatGudang.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack spacing={5} mt={12}>
                  <Button
                    as={ReachLink}
                    onClick={() => navigate(-1)}
                    border="1px solid"
                    borderColor="primary.500"
                    variant="outline"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.700"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                    }}
                  >
                    Batal
                  </Button>
                  <Button
                    type="button"
                    onClick={onSubmitFormEditGudang}
                    bg="primary.500"
                    variant="solid"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.100"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                      bg: 'primary.600',
                    }}
                  >
                    Simpan
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditGudang;
