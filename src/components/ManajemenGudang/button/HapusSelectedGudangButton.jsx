import React from 'react';
import {
  Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';
import axios from 'axios';
import { deleteAllCookies } from 'utils/SetCookies';
import { useNavigate } from 'react-router-dom';

function HapusSelectedGudangButton({
  noSelected, accessToken, selectedData, data, dispatch,
}) {
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const selectedID = selectedData.filter((item) => {
    if (item.isSelect) {
      return true;
    }
    return false;
  }).map((item) => item.id);

  const deleteSomeGudang = async (id) => {
    await axios({
      method: 'delete',
      url: `warehouses/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = data.filter((item) => !selectedID.includes(item.id));
        dispatch({
          type: 'UPDATE_DATA',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
          },
        });

        toast({
          title: `${selectedID.length} data gudang berhasil dihapus`,
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        onClose();
      }
    }).catch(async (err) => {
      onClose();
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gudang gagal dihapus',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const handleDeleteSomeGudang = () => {
    if (selectedID) {
      deleteSomeGudang(selectedID.join());
    }
  };

  return (
    <>
      <Button
        onClick={onOpen}
        border="1px solid"
        borderColor="primary.500"
        variant="outline"
        display={noSelected ? 'none' : 'block'}
        fontSize="sm"
        px={5}
        py={2}
        borderRadius={5}
        color="basic.700"
        fontWeight="bold"
        _hover={{
          textDecoration: 'none',
          cursor: 'pointer',
        }}
      >
        Hapus Data
      </Button>

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="lg">Hapus Gudang</ModalHeader>
          <ModalBody fontSize="md">
            <Text color="basic.500">
              Apakah Anda yakin ingin menghapus data? Anda tidak dapat membatalkan tindakan ini setelahnya.
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
            <Button colorScheme="error" color="basic.100" borderRadius={10} px={5} onClick={handleDeleteSomeGudang}>Hapus</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default HapusSelectedGudangButton;
