import React from 'react';
import {
  Button, chakra, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import { deleteAllCookies } from 'utils/SetCookies';
import { useNavigate } from 'react-router-dom';

function HapusDataPembelianIcon({
  accessToken, id, data, dispatch, status,
}) {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const statusBool = status === 8;

  const handleDeletePembelian = () => {
    axios({
      method: 'delete',
      url: `purchases/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = data.filter((item) => item.id !== id);
        dispatch({
          type: 'UPDATE_DATA',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
          },
        });
        onClose();
        toast({
          title: 'Data pembelian berhasil dihapus',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      onClose();
      toast({
        title: 'Data pembelian gagal dihapus',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  return (
    <>
      <IconChakra
        icon={faTrash}
        onClick={statusBool ? onOpen : null}
        color={statusBool ? 'error.600' : 'basic.400'}
        _hover={statusBool ? { color: 'error.400', cursor: 'pointer' } : null}
      />

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="lg">Hapus Pembelian</ModalHeader>
          <ModalBody fontSize="md">
            <Text color="basic.500">Apakah Anda yakin? Anda tidak dapat membatalkan tindakan ini setelahnya.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
            <Button colorScheme="error" color="basic.100" borderRadius={10} px={5} onClick={handleDeletePembelian}>Hapus</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default HapusDataPembelianIcon;
