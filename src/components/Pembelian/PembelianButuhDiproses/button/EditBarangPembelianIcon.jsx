import {
  Button, chakra, FormControl, FormLabel, HStack, Input, InputGroup,
  InputRightAddon, Modal, ModalBody, ModalCloseButton,
  ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text,
  useDisclosure, useToast, VStack,
} from '@chakra-ui/react';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import Select from 'react-select';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import useBarangPembelianFormValidator from 'utils/useBarangPembelianFormValidator';

function EditBarangPembelianIcon({ data, state, dispatch }) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const SelectChakra = chakra(Select);
  const IconChakra = chakra(FontAwesomeIcon);

  const { errors, validateForm, onBlurField } = useBarangPembelianFormValidator(state.formEditBarang);

  const handleOpen = () => {
    const nextFormState = {
      barangID: data.id,
      kuantitas: data.kuantitas,
      subTotal: data.kuantitas * data.hargaBeli,
    };
    dispatch({
      name: '4',
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });

    onOpen();
  };

  useEffect(() => {
    if (isOpen) {
      const nextFormState = {
        ...state.formEditBarang,
        subTotal: state.formEditBarang.kuantitas * data.hargaBeli,
      };
      dispatch({
        name: '5',
        type: 'SET_FORM_EDIT_BARANG',
        payload: nextFormState,
      });
    }
  }, [state.formEditBarang.kuantitas]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formEditBarang,
      [field]: e.target.value,
    };
    dispatch({
      name: '6',
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditBarangPembelian = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formEditBarang, errors, forceTouchErrors: true });
    const dataBaru = {
      id: data.id,
      namaBarang: data.namaBarang,
      hargaBeli: data.hargaBeli,
      kuantitas: state.formEditBarang.kuantitas,
      gudang: data.gudang,
      subTotal: data.subTotal,
    };
    if (!isValid) return;

    const itemPosition = state.dataPembelian.map((item) => item.id).indexOf(dataBaru.id);
    dispatch({
      type: 'SET_QUANTITY',
      payload: {
        itemPos: itemPosition,
        data: dataBaru,
      },
    });
    toast({
      title: 'Data berhasil diubah',
      position: 'top',
      status: 'success',
      isClosable: true,
    });

    onClose();
    dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' });
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  return (
    <>
      <IconChakra
        icon={faPenToSquare}
        onClick={handleOpen}
        color="basic.700"
        _hover={{ color: 'basic.500', cursor: 'pointer' }}
      />

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Edit Barang Pembelian</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kode Barang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodeBarang"
                      w="100%"
                      value={data.kodeBarang ? data.kodeBarang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                    <SelectChakra
                      isDisabled
                      id="barangID"
                      value={{ value: data.id, label: data.namaBarang }}
                      styles={customStyles}
                      w="100%"
                      placeholder="Select data barang"
                      border="1px solid"
                      borderColor="primary.300"
                      borderRadius={5}
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                    {errors.barangID.dirty && errors.barangID.error ? (
                      <Text color="red" my={2}>{errors.barangID.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Gudang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="gudang"
                      w="100%"
                      value={data.gudang ? data.gudang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Harga Beli</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="hargaBeli"
                      w="100%"
                      value={data.hargaBeli ? ConvertMoneyIDR(data.hargaBeli) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kuantitas</FormLabel>
                    <InputGroup>
                      <Input
                        type="number"
                        onWheel={(e) => e.target.blur()}
                        name="kuantitas"
                        placeholder="Masukkan jumlah barang"
                        w="100%"
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        value={state.formEditBarang.kuantitas ? state.formEditBarang.kuantitas : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                    {errors.kuantitas.dirty && errors.kuantitas.error ? (
                      <Text color="red" my={2}>{errors.kuantitas.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Sub Total</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="subTotal"
                      w="100%"
                      value={state.formEditBarang.subTotal ? ConvertMoneyIDR(state.formEditBarang.subTotal) : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              disabled={state.formEditBarang.subTotal === 0 || !state.formEditBarang.subTotal}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormEditBarangPembelian}
            >
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default EditBarangPembelianIcon;
