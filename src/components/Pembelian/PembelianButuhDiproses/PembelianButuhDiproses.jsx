import React, { useEffect, useReducer, useState } from 'react';
import {
  Box, Button, Center, chakra, Checkbox, Flex, FormControl, HStack,
  Input, InputGroup, InputRightAddon, Link, Modal, ModalBody, ModalContent,
  ModalFooter, ModalHeader, ModalOverlay, Select, Spacer, Table, TableContainer,
  Tbody, Td, Text, Th, Thead, Tr, useDisclosure, useToast,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import HapusSelectedPembelianButton from 'components/Pembelian/PembelianButuhDiproses/button/HapusSelectedPembelianButton';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import { INITIAL_STATE, PembelianDiprosesReducer } from 'reducer/Pembelian/PembelianDiprosesReducer';
import {
  faBoxesStacked,
  faCalendarDays, faChevronLeft, faChevronRight, faCircleInfo, faCoins, faGear, faLock, faPenToSquare, faSort, faUser,
} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import HapusDataPembelianIcon from 'components/Pembelian/PembelianButuhDiproses/button/HapusDataPembelianIcon';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import reactSelect from 'react-select';

function PembelianButuhDiproses() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(reactSelect);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(PembelianDiprosesReducer, INITIAL_STATE);
  const [changeOption, setChangeOption] = useState({
    selected: { value: '', label: '' },
    index: '',
    itemID: '',
  });

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataPembelian.every(checkSelect);

  const fetchGetAllPembelian = async () => {
    axios({
      method: 'get',
      url: `purchases?limit=${state.showRows}&type=8,9`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });

        const selectedStatus = dataBaru.map(
          (item) => ({ value: item.status_id, label: item?.status.name }),
        );

        const dataStatus = dataBaru.map((item) => {
          if (item?.status_id === 8) {
            return [
              { value: 8, label: 'Diproses', disabled: true },
              { value: 9, label: 'Diterima', disabled: true },
              { value: 10, label: 'Selesai', disabled: true },
              { value: 12, label: 'Batal', disabled: false },
            ];
          }
          if (item?.status_id === 9) {
            return [
              { value: 8, label: 'Diproses', disabled: true },
              { value: 9, label: 'Diterima', disabled: true },
              { value: 10, label: 'Selesai', disabled: false },
              { value: 12, label: 'Batal', disabled: true },
            ];
          }
          return null;
        });

        dispatch({
          type: 'SET_STATUS',
          payload: {
            selected: selectedStatus,
            data: dataStatus,
          },
        });

        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `purchases?limit=${state.showRows}&type=8,9&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });

        const selectedStatus = dataBaru.map(
          (item) => ({ value: item.status_id, label: item?.status.name }),
        );

        const dataStatus = dataBaru.map((item) => {
          if (item?.status_id === 8) {
            return [
              { value: 8, label: 'Diproses', disabled: true },
              { value: 9, label: 'Diterima', disabled: true },
              { value: 10, label: 'Selesai', disabled: true },
              { value: 12, label: 'Batal', disabled: false },
            ];
          }
          if (item?.status_id === 9) {
            return [
              { value: 8, label: 'Diproses', disabled: true },
              { value: 9, label: 'Diterima', disabled: true },
              { value: 10, label: 'Selesai', disabled: false },
              { value: 12, label: 'Batal', disabled: true },
            ];
          }
          return null;
        });

        dispatch({
          type: 'SET_STATUS',
          payload: {
            selected: selectedStatus,
            data: dataStatus,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  useEffect(() => {
    fetchGetAllPembelian();
  }, []);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPembelian.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllPembelian();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const fetchChangeStatus = async () => {
    axios({
      method: 'patch',
      url: `purchases/status-order/${changeOption?.itemID}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        status_id: changeOption?.selected?.value,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const selectedStatus = changeOption?.selected;
        dispatch({
          type: 'UPDATE_STATUS',
          payload: {
            itemPos: changeOption?.index,
            value: changeOption?.selected?.value,
            data: selectedStatus,
          },
        });

        onClose();

        toast({
          title: 'Berhasil memperbarui status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });

        const status = changeOption?.selected?.value;

        if (status !== 8 && status !== 9) {
          const dataBaru = state.dataPembelian.filter((item) => item.id !== changeOption?.itemID);
          dispatch({
            type: 'UPDATE_DATA',
            payload: {
              data: dataBaru,
              selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            },
          });
        } else {
          const dataBaru = state.dataPembelian.map((item) => {
            if (item.id === changeOption?.itemID) return { ...item, status_id: changeOption?.selected?.value };
            return item;
          });
          dispatch({
            type: 'UPDATE_DATA',
            payload: {
              data: dataBaru,
              selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            },
          });
        }
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal memperbarui status',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSelectUpdateField = async (e, index, itemID) => {
    await setChangeOption({
      selected: { value: e.value, label: e.label },
      index,
      itemID,
    });
    onOpen();
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari berdasarkan nama"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />

          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <HStack spacing={noSelected ? 0 : 7} mt={{ base: 5, xl: 0 }}>
            <HapusSelectedPembelianButton
              noSelected={noSelected}
              accessToken={auth.accessToken}
              data={state.dataPembelian}
              dispatch={dispatch}
              selectedData={state.selectedDataPembelian}
            />
            <Link
              as={ReachLink}
              to="/pembelian/tambah-pembelian"
              bg="primary.500"
              variant="solid"
              fontSize="sm"
              px={5}
              py={2}
              borderRadius={5}
              color="basic.100"
              fontWeight="bold"
              _hover={{
                textDecoration: 'none',
                cursor: 'pointer',
                bg: 'primary.600',
              }}
            >
              Tambah Pembelian
            </Link>
          </HStack>

        </Flex>
      </Flex>

      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <Center py={3}>
                  <Box w="10px" h="10px" bg="white" />
                </Center>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text ms={2}>ID PEMBELIAN </Text>
                </HStack>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text ms={2}>SUPPLIER </Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendarDays} />
                  <Flex ms={2} flexDir="column">
                    <Text>TANGGAL</Text>
                    <Text>BELI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCoins} />
                  <Flex ms={2} flexDir="column">
                    <Text>TOTAL</Text>
                    <Text>HARGA</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Flex ms={2} flexDir="column">
                    <Text>JENIS</Text>
                    <Text>PEMBAYARAN</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>STATUS</Text>
                </HStack>
              </Th>
              <Th textAlign="center">
                <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </Center>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state?.dataPembelian?.map((item, index) => (
              <Tr key={item.id}>
                <Td>
                  <Center>
                    <Checkbox
                      disabled={item.status_id !== 8}
                      colorScheme="primary"
                      isChecked={state.selectedDataPembelian[index].isSelect}
                      onChange={(e) => handleSelected(e, item.id)}
                    />
                  </Center>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/pembelian/detail-pembelian/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.supplier.fullname}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.date_of_purchase}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total)}</Text>
                </Td>
                <Td>
                  <Box w="fit-content" px={3} py={2} borderRadius={5} bg={item.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                    <Text color="basic.100" fontWeight="semibold">{item.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                  </Box>
                </Td>
                <Td w="auto">
                  <SelectChakra
                    id="status"
                    onChange={(e) => onSelectUpdateField(e, index, item.id)}
                    value={state.selectedStatus[index]}
                    options={state.dataStatus[index]}
                    styles={customStyles}
                    isOptionDisabled={(option) => option.disabled}
                    border="1px solid"
                    borderColor="primary.300"
                    borderRadius={5}
                    color="basic.500"
                    _focus={{
                      color: 'basic.800',
                    }}
                    menuPortalTarget={document.body}
                  />
                </Td>
                <Td textAlign="center">
                  <HStack justifyContent="center" spacing={5}>
                    {item.status_id === 8
                      ? (
                        <Link
                          as={ReachLink}
                          to={`/pembelian/edit-pembelian/${item.id}`}
                          color="basic.700"
                          _hover={{ color: 'basic.500', cursor: 'pointer' }}
                        >
                          <IconChakra icon={faPenToSquare} />
                        </Link>
                      )
                      : (
                        <IconChakra color="basic.400" icon={faPenToSquare} />
                      )}
                    <HapusDataPembelianIcon accessToken={auth.accessToken} id={item.id} data={state.dataPembelian} dispatch={dispatch} status={item.status_id} />
                  </HStack>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>

      <ConfirmChangeStatus isOpen={isOpen} onClose={onClose} changeOption={changeOption} fetchChangeStatus={fetchChangeStatus} />

    </>
  );
}

export function ConfirmChangeStatus({
  isOpen, onClose, changeOption, fetchChangeStatus,
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader fontSize="lg">
          Pembelian
          {' '}
          {changeOption?.selected.label}
        </ModalHeader>
        <ModalBody fontSize="md">
          <Text color="basic.500">
            Apakah Anda yakin pesanan
            {' '}
            {changeOption?.selected.label.toLowerCase()}
            ? Anda tidak dapat membatalkan tindakan ini setelahnya.
          </Text>
        </ModalBody>
        <ModalFooter>
          <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
          <Button colorScheme="primary" color="basic.100" borderRadius={10} px={5} onClick={() => fetchChangeStatus()}>Ya</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default PembelianButuhDiproses;
