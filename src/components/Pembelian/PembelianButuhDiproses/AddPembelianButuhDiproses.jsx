import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text, useToast, VStack, Flex, Button, TableContainer, Table, Thead, Tr, Th, Center, Tbody, Td, Checkbox, Link, InputGroup, InputRightAddon,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCashRegister, faCircleInfo, faPlus, faTag, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useLocation, useNavigate } from 'react-router-dom';
import usePembelianFormValidator from 'utils/usePembelianFormValidator';
import { PembelianDiprosesReducer, INITIAL_STATE } from 'reducer/Pembelian/PembelianDiprosesReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import Select from 'react-select';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import { useDispatch } from 'react-redux';
import { setPrevPath } from 'redux/reducer/navigationReducer';
import AddBarangPembelianButton from './button/AddBarangPembelianButton';
import HapusBarangPembelianIcon from './button/HapusBarangPembelianIcon';
import HapusSelectedBarangPembelianButton from './button/HapusSelectedBarangPembelianButton';
import EditBarangPembelianIcon from './button/EditBarangPembelianIcon';

function AddPembelianButuhDiproses() {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(PembelianDiprosesReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = usePembelianFormValidator(state.form);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    const selectedSupplier = {
      value: e.value, label: e.label,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedSupplier,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchGetAllSupplier = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'suppliers',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataSupplier',
            data: dataBaru.map((item) => ({ value: item.id, label: item.fullname })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetAllBarang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'products',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataBarang',
            data: dataBaru.map((item) => ({
              id: item.id,
              value: item.id,
              label: item.name,
              kodeBarang: item.id,
              gudang: item?.warehouse.name,
              hargaBeli: item.purchase_price,
            })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    if (state.purchase_details.length !== 0) {
      let totalCount = 0;
      state.purchase_details.forEach((item) => { totalCount += item.subtotal; });
      const nextFormState = {
        ...state.form,
        total: totalCount,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    } else {
      const nextFormState = {
        ...state.form,
        total: 0,
      };
      dispatch({
        type: 'SET_FORM',
        payload: nextFormState,
      });
    }
  }, [state.purchase_details]);

  useEffect(() => {
    fetchGetAllSupplier();
    fetchGetAllBarang();
  }, []);

  const fetchAddPembelianButuhDiproses = async () => {
    await axios({
      method: 'post',
      url: 'purchases',
      data: {
        date_of_purchase: state.form.tanggalBeli,
        purchase_number: state.form.noPesanan,
        total: state.form.total,
        payment_method: state.form.jenisPembayaran,
        description: state.form.catatan,
        supplier_id: state.form.supplierID,
        purchase_details: state.purchase_details,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const jenisPembayaran = [
    { value: 'tunai', label: 'Tunai' },
    { value: 'kredit', label: 'Kredit' },
  ];

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }

  const noSelected = state.selectedDataPembelian.every(checkSelect);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPembelian.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const onSubmitFormAddPembelianButuhDiproses = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchAddPembelianButuhDiproses();
    navigate('/pembelian');
  };

  const onSubmitAddFormPembelianAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddPembelianButuhDiproses();
    navigate(0);
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Pembelian</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Supplier Barang</FormLabel>
                      <InputGroup w="100%">
                        <SelectChakra
                          id="supplierID"
                          isSearchable
                          onChange={(e) => onSelectUpdateField(e, 'supplierID', 'selectedSupplier')}
                          onBlur={(e) => onBlurField(e, 'supplierID')}
                          value={state.selectedSupplier}
                          options={state.dataSupplier}
                          styles={customStyles}
                          w="100%"
                          placeholder="Select supplier"
                          border="1px solid"
                          borderColor="primary.300"
                          borderRadius={5}
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                        <InputRightAddon p={0}>
                          <Link as={ReachLink} to="/supplier/tambah-supplier" onClick={() => dispatchRedux(setPrevPath(location.pathname))} px={3}>
                            <IconChakra icon={faPlus} />
                          </Link>
                        </InputRightAddon>
                      </InputGroup>
                      {errors.supplierID.dirty && errors.supplierID.error ? (
                        <Text color="red" my={2}>{errors.supplierID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">No Pesanan</FormLabel>
                      <Input
                        type="text"
                        name="noPesanan"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.noPesanan ? state.form.noPesanan : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.noPesanan.dirty && errors.noPesanan.error ? (
                        <Text color="red" my={2}>{errors.noPesanan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal Beli</FormLabel>
                      <Input
                        isDisabled
                        type="date"
                        name="tanggalBeli"
                        w="100%"
                        onChange={onUpdateField}
                        value={state.form.tanggalBeli ? state.form.tanggalBeli : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jenis Pembayaran</FormLabel>
                      <SelectChakra
                        name="jenisPembayaran"
                        placeholder="Pilih jenis pembayaran"
                        onChange={(e) => onSelectUpdateField(e, 'jenisPembayaran', 'selectedJenisPembayaran')}
                        onBlur={(e) => onBlurField(e, 'jenisPembayaran')}
                        value={state.selectedJenisPembayaran}
                        options={jenisPembayaran}
                        styles={customStyles}
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.jenisPembayaran.dirty && errors.jenisPembayaran.error ? (
                        <Text color="red" my={2}>{errors.jenisPembayaran.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Catatan</FormLabel>
                      <Input
                        type="text"
                        name="catatan"
                        value={state.form.catatan ? state.form.catatan : ''}
                        onChange={onUpdateField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Pembelian</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <HStack
                w="100%"
                justifyContent="space-between"
              >
                <AddBarangPembelianButton
                  state={state}
                  dispatch={dispatch}
                />
                <HapusSelectedBarangPembelianButton
                  noSelected={noSelected}
                  data={state.dataPembelian}
                  dataPurchaseDetails={state.purchase_details}
                  dispatch={dispatch}
                  selectedData={state.selectedDataPembelian}
                />
              </HStack>

              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <Center py={3}>
                          <Box w="10px" h="10px" bg="white" />
                        </Center>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga beli</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                      <Th textAlign="center">
                        <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCircleInfo} />
                          <Text ms={2}>AKSI</Text>
                        </Center>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.dataPembelian.map((item, index) => (
                      <Tr key={item.id}>
                        <Td>
                          <Center>
                            <Checkbox
                              colorScheme="primary"
                              isChecked={state?.selectedDataPembelian[index].isSelect}
                              onChange={(e) => handleSelected(e, item.id)}
                            />
                          </Center>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.namaBarang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.hargaBeli)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.kuantitas}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.gudang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subTotal)}</Text>
                        </Td>
                        <Td textAlign="center">
                          <HStack justifyContent="center" spacing={5}>
                            <EditBarangPembelianIcon
                              data={item}
                              state={state}
                              dispatch={dispatch}
                            />
                            <HapusBarangPembelianIcon
                              id={item.id}
                              data={state.dataPembelian}
                              dataPurchaseDetails={state.purchase_details}
                              dispatch={dispatch}
                            />
                          </HStack>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state.form.total)}</Text>
              </HStack>
              <HStack spacing={5} alignSelf="end">
                <Button
                  isDisabled={state.purchase_details.length === 0}
                  type="button"
                  onClick={onSubmitAddFormPembelianAgain}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Simpan dan Buat Lagi
                </Button>
                <Button
                  isDisabled={state.purchase_details.length === 0}
                  type="button"
                  onClick={onSubmitFormAddPembelianButuhDiproses}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddPembelianButuhDiproses;
