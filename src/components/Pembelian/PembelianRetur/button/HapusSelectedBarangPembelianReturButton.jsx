import React from 'react';
import {
  Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';

function HapusSelectedBarangPembelianReturButton({
  noSelected, selectedData, data, dataPurchaseDetails, dispatch,
}) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const selectedID = selectedData.filter((item) => {
    if (item.isSelect) {
      return true;
    }
    return false;
  }).map((item) => item.id);

  const handleDeleteSomePembelian = () => {
    if (selectedID) {
      const dataBaru = data.filter((item) => !selectedID.includes(item.id));
      const dataBaruPurchaseDetails = dataPurchaseDetails.filter((item) => !selectedID.includes(item.product_id));
      dispatch({
        type: 'UPDATE_DATA',
        payload: {
          data: dataBaru,
          data_purchase_details: dataBaruPurchaseDetails,
          selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
        },
      });

      onClose();

      toast({
        title: `${selectedID.length} barang pembelian retur berhasil dihapus`,
        position: 'top',
        status: 'success',
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Button
        onClick={onOpen}
        border="1px solid"
        borderColor="primary.500"
        variant="outline"
        display={noSelected ? 'none' : 'block'}
        fontSize="sm"
        px={5}
        py={2}
        borderRadius={5}
        color="basic.700"
        fontWeight="bold"
        _hover={{
          textDecoration: 'none',
          cursor: 'pointer',
        }}
      >
        Hapus Data
      </Button>

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="lg">Hapus Barang Pembelian Retur</ModalHeader>
          <ModalBody fontSize="md">
            <Text color="basic.500">
              Apakah Anda yakin ingin menghapus data? Anda tidak dapat membatalkan tindakan ini setelahnya.
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
            <Button colorScheme="error" color="basic.100" borderRadius={10} px={5} onClick={handleDeleteSomePembelian}>Hapus</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default HapusSelectedBarangPembelianReturButton;
