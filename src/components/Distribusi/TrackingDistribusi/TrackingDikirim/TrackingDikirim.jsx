import React, { useEffect, useReducer, useState } from 'react';
import {
  Link, chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr,
  Flex, useToast, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faLock, faGear, faCalendar, faSort, faCircle, faUser, faCar, faMotorcycle,
} from '@fortawesome/free-solid-svg-icons';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import reactSelect from 'react-select';
import { INITIAL_STATE, TrackingDikirimReducer } from 'reducer/Distribusi/TrackingDistribusi/TrackingDikirimReducer';
import axios from 'axios';

function TrackingDikirim() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(reactSelect);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(TrackingDikirimReducer, INITIAL_STATE);
  const [changeOption, setChangeOption] = useState({
    selected: { value: '', label: '' },
    index: '',
    itemID: '',
  });

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetTracking = async () => {
    axios({
      method: 'get',
      url: `tracking-distribution/4?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataTracking',
            data: dataBaru,
          },
        });

        const selectedStatus = dataBaru.map(
          (item) => ({ value: item?.driver?.status_id, label: item?.driver?.status.name }),
        );

        const dataStatus = dataBaru.map((item) => {
          if (item?.driver?.status_id === 14) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: false },
              { value: 16, label: 'Selesai', disabled: true },
            ];
          }
          if (item?.driver?.status_id === 15) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: true },
              { value: 16, label: 'Selesai', disabled: false },
            ];
          }
          if (item?.driver?.status_id === 16) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: true },
              { value: 16, label: 'Selesai', disabled: true },
            ];
          }
          return null;
        });

        dispatch({
          type: 'SET_STATUS',
          payload: {
            selected: selectedStatus,
            data: dataStatus,
          },
        });

        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });

        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `tracking-distribution/4?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataTracking',
            data: dataBaru,
          },
        });

        const selectedStatus = dataBaru.map(
          (item) => ({ value: item?.driver?.status_id, label: item?.driver?.status.name }),
        );

        const dataStatus = dataBaru.map((item) => {
          if (item?.driver?.status_id === 14) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: false },
              { value: 16, label: 'Selesai', disabled: true },
            ];
          }
          if (item?.driver?.status_id === 15) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: true },
              { value: 16, label: 'Selesai', disabled: false },
            ];
          }
          if (item?.driver?.status_id === 16) {
            return [
              { value: 14, label: 'Mengambil', disabled: true },
              { value: 15, label: 'Mengantar', disabled: true },
              { value: 16, label: 'Selesai', disabled: true },
            ];
          }
          return null;
        });

        dispatch({
          type: 'SET_STATUS',
          payload: {
            selected: selectedStatus,
            data: dataStatus,
          },
        });

        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });

        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetTracking();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const fetchChangeStatus = async () => {
    axios({
      method: 'patch',
      url: `tracking-distribution/status/${changeOption.itemID}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
      data: {
        status_id: changeOption.selected.value,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (changeOption.selected.value !== 16) {
          const selectedStatus = changeOption?.selected;
          dispatch({
            type: 'UPDATE_STATUS',
            payload: {
              itemPos: changeOption?.index,
              value: changeOption?.selected?.value,
              data: selectedStatus,
            },
          });
        } else if (changeOption.selected.value === 16) {
          const dataBaru = state.dataTracking.filter((item) => item.id !== changeOption.itemID);
          dispatch({
            type: 'SET_DATA',
            payload: {
              name: 'dataTracking',
              data: dataBaru,
            },
          });
        }

        onClose();

        toast({
          title: 'Berhasil memperbarui status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal memperbarui status',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSelectUpdateField = async (e, index, itemID) => {
    await setChangeOption({
      selected: { value: e.value, label: e.label },
      index,
      itemID,
    });

    onOpen();
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const status = {
    dikirim:
  <HStack color="purple.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Dikirim</Text>
  </HStack>,
    selesai:
  <HStack color="success.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Selesai</Text>
  </HStack>,
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />

          <IconChakra icon={faSort} className="fa-xl" />
        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text ms={2}>ID PENJUALAN</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUser} />
                  <Text ms={2}>PELANGGAN</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text ms={2}>TANGGAL KIRIM</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCar} />
                  <Text ms={2}>KENDARAAN</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>STATUS</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faMotorcycle} />
                  <Text ms={2}>DRIVER</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faMotorcycle} />
                  <Text ms={2}>AKSI</Text>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataTracking.map((item, index) => (
              <Tr key={item.id}>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/tracking-distribusi/tracking-dikirim/detail-tracking/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.sale_id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.sale?.customer?.fullname}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.sale?.delivery_date}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.sale?.transportation_type?.name}</Text>
                </Td>
                <Td>
                  {item.status_id === 4 && status.dikirim}
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.driver?.fullname}</Text>
                </Td>
                <Td w="auto">
                  <SelectChakra
                    id="status"
                    onChange={(e) => onSelectUpdateField(e, index, item.id)}
                    value={state.selectedStatus[index]}
                    options={state.dataStatus[index]}
                    styles={customStyles}
                    isOptionDisabled={(option) => option.disabled}
                    border="1px solid"
                    borderColor="primary.300"
                    borderRadius={5}
                    color="basic.500"
                    _focus={{
                      color: 'basic.800',
                    }}
                    menuPortalTarget={document.body}
                  />
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>

      <ConfirmChooseDriver isOpen={isOpen} onClose={onClose} changeOption={changeOption} fetchChangeStatus={fetchChangeStatus} />
    </>
  );
}

export function ConfirmChooseDriver({
  isOpen, onClose, changeOption, fetchChangeStatus,
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader fontSize="lg">
          Tracking Distribusi
          {' '}
          {changeOption?.selected.value === 15 && 'Diantar'}
          {changeOption?.selected.value === 16 && 'Selesai'}
        </ModalHeader>
        <ModalBody fontSize="md">
          <Text color="basic.500">
            Apakah Anda yakin akan
            {' '}
            {changeOption?.selected.value === 15 && 'diantar'}
            {changeOption?.selected.value === 16 && 'selesai'}
            ? Anda tidak dapat membatalkan tindakan ini setelahnya.
          </Text>
        </ModalBody>
        <ModalFooter>
          <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
          <Button colorScheme="primary" color="basic.100" borderRadius={10} px={5} onClick={() => fetchChangeStatus()}>Ya</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default TrackingDikirim;
