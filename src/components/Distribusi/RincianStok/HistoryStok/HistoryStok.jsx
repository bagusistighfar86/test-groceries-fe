import React, { useEffect, useReducer, useState } from 'react';
import {
  chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr, Flex, useToast, Box, Link, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faLock, faList, faBoxesStacked, faCalendar, faUsers, faBookOpen, faDownLong, faUpLong, faSort, faFileExcel,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, HistoryStokReducer } from 'reducer/RincianStok/HistoryStokReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';

function HistoryStok() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(HistoryStokReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllHistoryStok = () => {
    axios({
      method: 'get',
      url: `history-mutation-stock?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `history-mutation-stock?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllHistoryStok();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const getDetailURL = (stockType, referenceNumber) => {
    if (stockType === 'pembelian') return `/pembelian/detail-pembelian/${referenceNumber}`;
    if (stockType === 'pembelian retur') return `/pembelian/pembelian-retur/detail-retur/${referenceNumber}`;
    if (stockType === 'penjualan') return `/penjualan/detail-penjualan/${referenceNumber}`;
    if (stockType === 'penjualan retur') return `/penjualan/penjualan-retur/detail-retur/${referenceNumber}`;
    return null;
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />
          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <Button
            bg="primary.500"
            variant="solid"
            fontSize="sm"
            px={5}
            py={2}
            borderRadius={5}
            color="basic.100"
            fontWeight="bold"
            _hover={{
              textDecoration: 'none',
              cursor: 'pointer',
              bg: 'primary.600',
            }}
          >
            <IconChakra icon={faFileExcel} className="fa-xl" me={3} />
            <Text>Download Excel</Text>
          </Button>
        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text display={{ base: 'none', lg: 'block' }}>NAMA BARANG</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>NAMA</Text>
                    <Text>BARANG</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text display={{ base: 'none', lg: 'block' }}>TANGGAL PROSES</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>TANGGAL</Text>
                    <Text>PROSES</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faList} />
                  <Text display={{ base: 'none', lg: 'block' }}>NOMOR REFERENSI</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>NOMOR </Text>
                    <Text>REFERENSI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>JENIS STOK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>JENIS</Text>
                    <Text>STOK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUsers} />
                  <Text ms={2}>PEGAWAI</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>UPDATE STOK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>UPDATE</Text>
                    <Text>STOK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBookOpen} />
                  <Text ms={2}>KETERANGAN</Text>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataHistoryStok.map((item) => (
              <Tr key={item.id}>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.product_name}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{(item.process_date.split(' '))[0]}</Text>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={getDetailURL(item.stock_type.toLowerCase(), item.reference_number)}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.reference_number}
                  </Link>
                </Td>
                <Td>
                  <Box
                    w="fit-content"
                    px={3}
                    py={1}
                    borderRadius={5}
                    bg={item.stock_type.toLowerCase() === 'pembelian' ? 'purple.100' : 'blue.100'}
                    color={item.stock_type.toLowerCase() === 'pembelian' ? 'purple.600' : 'blue.600'}
                  >
                    <Text fontWeight="semibold">{item.stock_type}</Text>
                  </Box>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.user_name}</Text>
                </Td>
                <Td>
                  <HStack color={item.update_stock.includes('+') ? 'primary.500' : 'error.500'}>
                    <IconChakra icon={item.update_stock.includes('+') ? faUpLong : faDownLong} />
                    <Text fontWeight="semibold">
                      {item.update_stock}
                      {' '}
                      pcs
                    </Text>
                  </HStack>
                </Td>
                <Td color={item.update_stock.includes('+') ? 'primary.500' : 'error.500'}>
                  <Text fontWeight="semibold">{item.note}</Text>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default HistoryStok;
