import {
  Button, chakra, FormControl, FormLabel, HStack, Input, InputGroup,
  InputRightAddon, Modal, ModalBody, ModalCloseButton,
  ModalContent, ModalFooter, ModalHeader, ModalOverlay, NumberInput, NumberInputField, Text,
  useDisclosure, useToast, VStack,
} from '@chakra-ui/react';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Select from 'react-select';
import useBarangOpnameFormValidator from 'utils/useBarangOpnameFormValidator';

function EditBarangOpnameIcon({ data, state, dispatch }) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const SelectChakra = chakra(Select);
  const IconChakra = chakra(FontAwesomeIcon);

  const { errors, validateForm, onBlurField } = useBarangOpnameFormValidator(state.formEditBarang);

  const handleOpen = () => {
    const nextFormState = {
      barangID: data.id,
      stokAkhir: parseInt(data.stokAkhir, 10),
    };
    dispatch({
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });

    onOpen();
  };

  const checkValue = (stokAkhir) => {
    if (stokAkhir <= 0) {
      return 1;
    } if (stokAkhir >= data.stokAwal) {
      return data.stokAwal - 1;
    }
    return stokAkhir;
  };

  // Ubah stok akhir
  const onUpdateField = async (e) => {
    const value = parseInt(e.target.value, 10);
    const field = e.target.name;
    const nextFormState = {
      ...state.formEditBarang,
      [field]: checkValue(value),
    };
    dispatch({
      type: 'SET_FORM_EDIT_BARANG',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditBarangOpname = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formEditBarang, errors, forceTouchErrors: true });
    const dataBaru = {
      id: data.id,
      kodeBarang: data.kodeBarang,
      namaBarang: data.namaBarang,
      stokAwal: data.stokAwal,
      stokAkhir: state.formEditBarang.stokAkhir,
    };
    if (!isValid) return;

    const itemPosition = state.dataOpname.map((item) => item.id).indexOf(dataBaru.id);
    dispatch({
      type: 'SET_QUANTITY',
      payload: {
        itemPos: itemPosition,
        data: dataBaru,
      },
    });
    toast({
      title: 'Data berhasil diubah',
      position: 'top',
      status: 'success',
      isClosable: true,
    });

    onClose();
    dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' });
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  return (
    <>
      <IconChakra
        icon={faPenToSquare}
        onClick={handleOpen}
        color="basic.700"
        _hover={{ color: 'basic.500', cursor: 'pointer' }}
      />

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Edit Barang Opname</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_EDIT_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kode Barang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodeBarang"
                      w="100%"
                      value={data.id ? data.id : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                    <SelectChakra
                      isDisabled
                      id="barangID"
                      value={{ value: data.id, label: data.namaBarang }}
                      styles={customStyles}
                      w="100%"
                      placeholder="Pilih data barang"
                      border="1px solid"
                      borderColor="primary.300"
                      borderRadius={5}
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                    {errors.barangID.dirty && errors.barangID.error ? (
                      <Text color="red" my={2}>{errors.barangID.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok Awal</FormLabel>
                    <InputGroup>
                      <Input
                        isDisabled
                        type="text"
                        name="stokAwal"
                        w="100%"
                        value={data.stokAwal ? data.stokAwal : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok Akhir</FormLabel>
                    <InputGroup>
                      <NumberInput
                        isDisabled={data.stokAwal === 0}
                        min={1}
                        max={data.stokAwal - 1}
                        w="100%"
                        value={state.formEditBarang.stokAkhir ? state.formEditBarang.stokAkhir : ''}
                      >
                        <NumberInputField
                          name="stokAkhir"
                          placeholder="Masukkan stok akhir"
                          w="100%"
                          onChange={onUpdateField}
                          onBlur={onBlurField}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </NumberInput>
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                    {errors.stokAkhir.dirty && errors.stokAkhir.error ? (
                      <Text color="red" my={2}>{errors.stokAkhir.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              isDisabled={data.stokAwal === '' || state.formEditBarang.stokAkhir === ''}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormEditBarangOpname}
            >
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default EditBarangOpnameIcon;
