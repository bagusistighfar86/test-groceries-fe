import {
  Button, chakra, FormControl, FormLabel, HStack, Input, InputGroup, InputRightAddon,
  Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay,
  NumberInput, NumberInputField, Text, useDisclosure, VStack,
} from '@chakra-ui/react';
import React from 'react';
import Select from 'react-select';
import useBarangOpnameFormValidator from 'utils/useBarangOpnameFormValidator';

function AddBarangOpnameButton({ state, dispatch }) {
  const SelectChakra = chakra(Select);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { errors, validateForm, onBlurField } = useBarangOpnameFormValidator(state.formAddBarang);

  const dataSudahDitambahkan = (id) => state.selectedDataOpname.some((item) => id === item.id);

  const checkValue = (stokAkhir) => {
    if (stokAkhir <= 0) {
      return 1;
    } if (stokAkhir >= state.selectedBarang.stokAwal) {
      return state.selectedBarang.stokAwal - 1;
    }
    return stokAkhir;
  };
  // Ubah stok akhir
  const onUpdateField = async (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.formAddBarang,
      [field]: await checkValue(e.target.value),
    };
    dispatch({
      type: 'SET_FORM_ADD_BARANG',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  // Ubah kodeBarang
  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.formAddBarang,
      [field]: e.value,
    };
    const selectedBarang = {
      id: e.id,
      value: e.value,
      label: e.label,
      kodeBarang: e.kodeBarang,
      stokAwal: e.stokAwal,
    };
    dispatch({
      type: 'SET_FORM_ADD_BARANG',
      payload: nextFormState,
    });
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedBarang,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  const onSubmitFormAddBarangOpname = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.formAddBarang, errors, forceTouchErrors: true });
    const dataBaru = {
      id: state.selectedBarang.id,
      kodeBarang: state.selectedBarang.kodeBarang,
      namaBarang: state.selectedBarang.label,
      stokAwal: state.selectedBarang.stokAwal,
      stokAkhir: state.formAddBarang.stokAkhir,
    };
    if (!isValid) return;
    if (!dataSudahDitambahkan(dataBaru.id)) {
      dispatch({
        type: 'SET_DATA',
        payload: {
          name: 'dataOpname',
          data: [
            ...state.dataOpname,
            dataBaru,
          ],
        },
      });
      dispatch({
        type: 'SET_DATA',
        payload: {
          name: 'opname_stok_details',
          data: [
            ...state.opname_stok_details,
            {
              product_id: dataBaru.id,
              init_stock: dataBaru.stokAwal,
              end_stock: dataBaru.stokAkhir,
            },
          ],
        },
      });
      dispatch({
        type: 'SET_SELECTED_SELECT',
        payload: {
          dataName: 'selectedDataOpname',
          data: [
            ...state.selectedDataOpname,
            { id: dataBaru.id, isSelect: false },
          ],
        },
      });
    } else {
      const itemPosition = state.dataOpname.map((item) => item.id).indexOf(dataBaru.id);
      dispatch({
        type: 'ADD_QUANTITY',
        payload: {
          itemPos: itemPosition,
          data: dataBaru,
        },
      });
    }
    onClose();
    dispatch({ type: 'RESET_STATE_ADD_BARANG_FORM' });
  };

  return (
    <>
      <Button
        onClick={onOpen}
        bg="basic.700"
        color="basic.100"
        fontWeight="normal"
        px={6}
        _hover={{
          bg: 'basic.900',
        }}
      >
        + Barang Opname
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} size="4xl" isCentered>
        <ModalOverlay />
        <ModalContent p={5}>
          <ModalHeader fontSize="2xl" alignSelf="center">Tambah Barang Opname</ModalHeader>
          <ModalCloseButton onClick={() => dispatch({ type: 'RESET_STATE_ADD_BARANG_FORM' })} />
          <ModalBody fontSize="md">
            <VStack spacing={5}>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Kode Barang</FormLabel>
                    <Input
                      isDisabled
                      type="text"
                      name="kodeBarang"
                      w="100%"
                      value={state.selectedBarang.kodeBarang ? state.selectedBarang.kodeBarang : ''}
                      variant="outline"
                      border="2px solid"
                      borderColor="primary.300"
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Nama Barang</FormLabel>
                    <SelectChakra
                      id="barangID"
                      isSearchable
                      onChange={(e) => onSelectUpdateField(e, 'barangID', 'selectedBarang')}
                      onBlur={(e) => onBlurField(e, 'barangID')}
                      value={state.selectedBarang}
                      options={state.dataBarang}
                      isOptionDisabled={(option) => dataSudahDitambahkan(option.id) || option.stokAwal === 0}
                      styles={customStyles}
                      w="100%"
                      placeholder="Pilih data barang"
                      border="1px solid"
                      borderColor="primary.300"
                      borderRadius={5}
                      color="basic.500"
                      _focus={{
                        color: 'basic.800',
                      }}
                    />
                    {errors.barangID.dirty && errors.barangID.error ? (
                      <Text color="red" my={2}>{errors.barangID.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
              <HStack w="100%" justifyContent="space-between" alignItems="start">
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok Awal</FormLabel>
                    <InputGroup>
                      <Input
                        isDisabled
                        type="text"
                        name="stokAwal"
                        w="100%"
                        value={state.selectedBarang.stokAwal ? state.selectedBarang.stokAwal : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                  </FormControl>
                </VStack>
                <VStack flexBasis="45%" alignItems="start">
                  <FormControl isRequired>
                    <FormLabel fontWeight="bold">Stok Akhir</FormLabel>
                    <InputGroup>
                      <NumberInput
                        isDisabled={state.selectedBarang.stokAwal === 0}
                        min={1}
                        max={state.selectedBarang.stokAwal - 1}
                        w="100%"
                      >
                        <NumberInputField
                          name="stokAkhir"
                          placeholder="Masukkan stok akhir"
                          w="100%"
                          onChange={onUpdateField}
                          onBlur={onBlurField}
                          value={state.formAddBarang.stokAkhir ? state.formAddBarang.stokAkhir : ''}
                          variant="outline"
                          border="2px solid"
                          borderColor="primary.300"
                          color="basic.500"
                          _focus={{
                            color: 'basic.800',
                          }}
                        />
                      </NumberInput>
                      <InputRightAddon>pcs</InputRightAddon>
                    </InputGroup>
                    {errors.stokAkhir.dirty && errors.stokAkhir.error ? (
                      <Text color="red" my={2}>{errors.stokAkhir.message}</Text>
                    ) : null}
                  </FormControl>
                </VStack>
              </HStack>
            </VStack>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              isDisabled={state.selectedBarang.stokAwal === '' || state.formAddBarang.stokAkhir === ''}
              colorScheme="primary"
              color="basic.100"
              borderRadius={10}
              px={10}
              py={5}
              onClick={onSubmitFormAddBarangOpname}
            >
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default AddBarangOpnameButton;
