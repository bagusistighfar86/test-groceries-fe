import React from 'react';
import {
  Button, Link, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, useToast,
} from '@chakra-ui/react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { deleteAllCookies } from 'utils/SetCookies';

function HapusDataOpnameButton({
  accessToken, id,
}) {
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleDeleteOpname = () => {
    axios({
      method: 'delete',
      url: `opname-stok/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        onClose();
        navigate('/rincian-stok');
        toast({
          title: 'Data opname berhasil dihapus',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data opname gagal dihapus',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      onClose();
    });
  };

  return (
    <>
      <Link
        to="/rincian-stok"
        onClick={onOpen}
        border="1px solid"
        borderColor="primary.500"
        variant="outline"
        fontSize="sm"
        px={5}
        py={2}
        borderRadius={5}
        color="basic.700"
        fontWeight="bold"
        _hover={{
          textDecoration: 'none',
          cursor: 'pointer',
        }}
      >
        Hapus Data
      </Link>

      <Modal isOpen={isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="lg">Hapus Opname</ModalHeader>
          <ModalBody fontSize="md">
            <Text color="basic.500">Apakah Anda yakin? Anda tidak dapat membatalkan tindakan ini setelahnya.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
            <Button colorScheme="error" color="basic.100" borderRadius={10} px={5} onClick={handleDeleteOpname}>Hapus</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default HapusDataOpnameButton;
