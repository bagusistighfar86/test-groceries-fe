import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text,
  useToast, VStack, Flex, Button, TableContainer, Table, Thead, Tr, Th, Center, Checkbox, Td, Tbody,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBookOpen, faBoxOpen, faCartFlatbed, faCircleInfo, faDolly, faDownLong,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import Select from 'react-select';
import LoadingPage from 'views/LoadingPage';
import { INITIAL_STATE, OpnameReducer } from 'reducer/RincianStok/OpnameReducer';
import useOpnameFormValidator from 'utils/useOpnameFormValidator';
import AddBarangOpnameButton from './button/AddBarangOpnameButton';
import HapusBarangOpnameIcon from './button/HapusBarangOpnameIcon';
import HapusSelectedBarangOpnameButton from './button/HapusSelectedBarangOpnameButton';
import EditBarangOpnameIcon from './button/EditBarangOpnameIcon';

function AddOpname() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(OpnameReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useOpnameFormValidator(state.formEditBarang);

  useEffect(() => {
    dispatch({ type: 'SET_TIME' });
  }, []);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });

    const selectedData = {
      value: e.value, label: e.label,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedData,
      },
    });

    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchGetAllGudang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'warehouses',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataGudang',
            data: dataBaru.map((item) => ({ value: item.id, label: item.name })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetAllBarang = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'products',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataBarang',
            data: dataBaru.map((item) => ({
              id: item.id,
              value: item.id,
              label: item.name,
              kodeBarang: item.id,
              stokAwal: item.stock,
            })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetAllGudang();
    fetchGetAllBarang();
  }, []);

  const fetchAddOpname = async () => {
    await axios({
      method: 'post',
      url: 'opname-stok',
      data: {
        warehouse_id: state.form.gudangID,
        opname_stok_details: state.opname_stok_details,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataOpname.every(checkSelect);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataOpname.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const onSubmitFormAddOpname = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    dispatch({ type: 'SET_TIME' });
    await fetchAddOpname();
    navigate('/rincian-stok');
  };

  const onSubmitFormAddOpnameAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    dispatch({ type: 'SET_TIME' });
    await fetchAddOpname();
    navigate(0);
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Opname</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Gudang</FormLabel>
                      <SelectChakra
                        id="gudangID"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'gudangID', 'selectedGudang')}
                        onBlur={(e) => onBlurField(e, 'gudangID')}
                        value={state.selectedGudang}
                        options={state.dataGudang}
                        styles={customStyles}
                        w="100%"
                        placeholder="Select gudang"
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.gudangID.dirty && errors.gudangID.error ? (
                        <Text color="red" my={2}>{errors.gudangID.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Tanggal dan Jam Pengecekan</FormLabel>
                      <Input
                        isDisabled
                        type="text"
                        name="tanggalJamPengecekan"
                        w="100%"
                        value={state.form.tanggalJamPengecekan ? state.form.tanggalJamPengecekan : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%">
                      <FormLabel fontWeight="bold">Catatan</FormLabel>
                      <Input
                        type="text"
                        name="catatan"
                        value={state.form.catatan ? state.form.catatan : ''}
                        onChange={onUpdateField}
                        w="100%"
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <HStack
                w="100%"
                justifyContent="space-between"
              >
                <AddBarangOpnameButton
                  state={state}
                  dispatch={dispatch}
                />
                <HapusSelectedBarangOpnameButton
                  noSelected={noSelected}
                  data={state.dataOpname}
                  dataOpnameDetails={state.opname_stok_details}
                  dispatch={dispatch}
                  selectedData={state.selectedDataOpname}
                />
              </HStack>

              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <Center py={3}>
                          <Box w="10px" h="10px" bg="white" />
                        </Center>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCartFlatbed} />
                          <Text ms={2}>Stok Awal</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faDolly} />
                          <Text ms={2}>Stok Akhir</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBookOpen} />
                          <Text ms={2}>Keterangan</Text>
                        </HStack>
                      </Th>
                      <Th textAlign="center">
                        <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCircleInfo} />
                          <Text ms={2}>AKSI</Text>
                        </Center>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.dataOpname.map((item, index) => (
                      <Tr key={item.id}>
                        <Td>
                          <Center>
                            <Checkbox
                              colorScheme="primary"
                              isChecked={state?.selectedDataOpname[index].isSelect}
                              onChange={(e) => handleSelected(e, item.id)}
                            />
                          </Center>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.namaBarang}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.stokAwal}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item.stokAkhir}</Text>
                        </Td>
                        <Td>
                          <HStack color={(item.stokAwal - item.stokAkhir) !== 0 ? 'error.500' : 'basic.100'} fontWeight="semibold">
                            <IconChakra icon={faDownLong} />
                            <Text>
                              {(item.stokAwal - item.stokAkhir) !== 0 ? '-' : ''}
                              {item.stokAwal - item.stokAkhir}
                              {' '}
                              pcs
                            </Text>
                          </HStack>
                        </Td>
                        <Td textAlign="center">
                          <HStack justifyContent="center" spacing={5}>
                            <EditBarangOpnameIcon
                              data={item}
                              state={state}
                              dispatch={dispatch}
                            />
                            <HapusBarangOpnameIcon
                              id={item.id}
                              data={state.dataOpname}
                              dataOpnameDetails={state.opname_stok_details}
                              dispatch={dispatch}
                            />
                          </HStack>
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>

              <HStack spacing={5} alignSelf="end">
                <Button
                  isDisabled={state.opname_stok_details.length === 0}
                  type="button"
                  onClick={onSubmitFormAddOpnameAgain}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Simpan dan Buat Lagi
                </Button>
                <Button
                  isDisabled={state.opname_stok_details.length === 0}
                  type="button"
                  onClick={onSubmitFormAddOpname}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddOpname;
