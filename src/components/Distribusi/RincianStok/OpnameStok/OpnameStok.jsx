import React, { useEffect, useReducer, useState } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Spacer, Table, TableContainer, Tbody, Td, Text, Th,
  Thead, Tr, Flex, Center, useToast, Select, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faLock, faWarehouse, faClock, faCalendar, faUsers, faSort,
} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import { OpnameReducer, INITIAL_STATE } from 'reducer/RincianStok/OpnameReducer';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import HapusDataOpnameIcon from './button/HapusDataOpnameIcon';
import HapusSelectedOpnameButton from './button/HapusSelectedOpnameButton';

function OpnameStok() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(OpnameReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataOpname.every(checkSelect);

  const fetchGetAllOpname = async () => {
    axios({
      method: 'get',
      url: `opname-stok?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `opname-stok?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllOpname();
    }
  }, [state.showRows]);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataOpname.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">

          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari berdasarkan nama"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />
          <IconChakra icon={faSort} className="fa-xl" />

          <Spacer />

          <HStack spacing={noSelected ? 0 : 7} mt={{ base: 5, xl: 0 }}>
            <HapusSelectedOpnameButton
              noSelected={noSelected}
              accessToken={auth.accessToken}
              data={state.dataOpname}
              dispatch={dispatch}
              selectedData={state.selectedDataOpname}
              fetchGetAllOpname={fetchGetAllOpname}
            />
            <Link
              as={ReachLink}
              to="/rincian-stok/opname-stok/tambah-opname"
              bg="primary.500"
              variant="solid"
              fontSize="sm"
              px={5}
              py={2}
              borderRadius={5}
              color="basic.100"
              fontWeight="bold"
              _hover={{
                textDecoration: 'none',
                cursor: 'pointer',
                bg: 'primary.600',
              }}
            >
              Tambah Opname
            </Link>
          </HStack>

        </Flex>
      </Flex>

      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <Center py={3}>
                  <Box w="10px" h="10px" bg="white" />
                </Center>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text display={{ base: 'none', lg: 'block' }}>ID OPNAME STOK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>ID OPNAME</Text>
                    <Text>STOK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faWarehouse} />
                  <Text ms={2}>GUDANG</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text ms={2}>TANGGAL CEK</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faClock} />
                  <Text ms={2}>JAM PENGECEKAN</Text>
                </HStack>
              </Th>
              <Th textAlign="center">
                <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faUsers} />
                  <Text ms={2}>PEGAWAI</Text>
                </Center>
              </Th>
              <Th textAlign="center">
                <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCircleInfo} />
                  <Text ms={2}>AKSI</Text>
                </Center>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state?.dataOpname?.map((item, index) => (
              <Tr key={item.id}>
                <Td>
                  <Center>
                    <Checkbox
                      colorScheme="primary"
                      isChecked={state.selectedDataOpname[index].isSelect}
                      onChange={(e) => handleSelected(e, item.id)}
                    />
                  </Center>
                </Td>
                <Td>
                  <Link
                    as={ReachLink}
                    to={`/rincian-stok/opname-stok/detail-opname/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item?.warehouse?.name}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{(item.date_of_check.split(' '))[0]}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{(item.date_of_check.split(' '))[1]}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.user_name}</Text>
                </Td>
                <Td textAlign="center">
                  <HapusDataOpnameIcon accessToken={auth.accessToken} id={item.id} data={state.dataOpname} dispatch={dispatch} />
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>

  );
}

export default OpnameStok;
