import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, HStack, Text, useToast, VStack, Flex, Button, TableContainer, Table, Thead,
  Tr, Th, Center, Tbody, Td, Spacer, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, useDisclosure,
} from '@chakra-ui/react';
import {
  faArrowLeft, faBagShopping, faBoxOpen, faCashRegister, faCircle, faCircleInfo, faTag, faWarehouse,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Select from 'react-select';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import {
  Link as ReachLink, useNavigate, useParams,
} from 'react-router-dom';
import { StokMasukReducer, INITIAL_STATE } from 'reducer/RincianStok/StokMasukReducer';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import ConvertISOtoDate from 'utils/ConvertISOtoDate';

function DetailStokMasukRetur() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(Select);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(StokMasukReducer, INITIAL_STATE);

  const setData = (dataBaru) => {
    const dataSelectedStatus = dataBaru?.return_sale?.return_sales_details.map((item) => (
      { value: item.is_posting, label: item.is_posting === 0 ? 'Pending' : 'Posting' }
    ));

    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: 'dataSelectedStatus',
        data: dataSelectedStatus,
      },
    });
  };

  const fetchGetDetailStokMasuk = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: `history-stocks/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        setData(dataBaru);
      }
      dispatch({ type: 'SET_IS_LOADING', payload: false });
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetDetailStokMasuk();
  }, []);

  function checkSelect(item) {
    if (item.value === 1) return true;
    return false;
  }

  const isPosting = state.dataSelectedStatus.every(checkSelect);

  const fetchChangeStatus = () => {
    axios({
      method: 'patch',
      url: `history-stocks/status/${state?.selectedStatus?.itemID}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataSelectedStatus = { value: state?.selectedStatus?.value, label: state?.selectedStatus?.label };
        dispatch({
          type: 'SET_STATUS',
          payload: {
            itemPos: state?.selectedStatus?.index,
            dataName: 'dataSelectedStatus',
            data: dataSelectedStatus,
          },
        });

        toast({
          title: 'Berhasil memperbarui status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });

        onClose();
        return res;
      }
      return null;
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal memperbarui status',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSelectUpdateField = (e, idx, idSelected, itemId) => {
    const selectedStatus = {
      value: e.value, label: e.label, itemID: itemId, index: idx,
    };
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: selectedStatus,
      },
    });

    onOpen();
  };

  const status = {
    pending:
  <HStack color="basic.400">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Pending</Text>
  </HStack>,
    posting:
  <HStack color="success.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Posting</Text>
  </HStack>,
    batal:
  <HStack color="error.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Batal</Text>
  </HStack>,
    retur:
  <HStack color="blue.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Retur</Text>
  </HStack>,
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>

            <Spacer />

            <VStack>
              <Text fontWeight="bold" display={isPosting ? 'block' : 'none'}>{ConvertISOtoDate(state?.detailStokMasuk?.updated_at)}</Text>
              {isPosting ? status.posting : status.pending}
            </VStack>
          </HStack>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Retur</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Pelanggan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailStokMasuk?.return_sale?.customer.fullname}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Tujuan Pengiriman</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>
                        {state?.detailStokMasuk?.return_sale?.customer.address}
                        {', '}
                        {state?.detailStokMasuk?.return_sale?.customer.subdistrict}
                        {', '}
                        {state?.detailStokMasuk?.return_sale?.customer.city}
                        {', '}
                        {state?.detailStokMasuk?.return_sale?.customer.province}
                        {', '}
                        {state?.detailStokMasuk?.return_sale?.customer.country}
                        {', '}
                        {state?.detailStokMasuk?.return_sale?.customer.postcode}
                      </Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Jual</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailStokMasuk?.return_sale?.sales?.date_of_sale}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="center">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Pembayaran</Text>
                    </Box>
                    <Box
                      w={{
                        base: '50%',
                        lg: '60%',
                      }}
                    >
                      <Box w="fit-content" px={3} borderRadius={5} bg={state?.detailStokMasuk?.return_sale?.sales?.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                        <Text color="basic.100" fontWeight="semibold">{state?.detailStokMasuk?.return_sale?.sales?.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                      </Box>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Retur</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailStokMasuk?.return_sale?.date}</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Jenis Pengembalian</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailStokMasuk?.return_sale?.return_type.toLowerCase() === 'barang' ? 'Barang' : 'Uang' }</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Catatan</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>{state?.detailStokMasuk?.return_sale?.note}</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
            </Box>
          </Flex>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box
              w={{
                base: '100%',
                lg: '20%',
              }}
              mb={5}
            >
              <Text fontWeight="bold" fontSize="xl">Data Barang Retur</Text>
            </Box>
            <VStack
              w={{
                base: '100%',
                lg: '80%',
              }}
              spacing={5}
              alignItems="start"
            >
              <TableContainer
                w="100%"
                border="2px solid"
                borderColor="basic.300"
                borderRadius={10}
              >
                <Table
                  size={{
                    base: 'md',
                    lg: 'sm',
                    xl: 'md',
                  }}
                  fontSize={{
                    base: 'sm',
                    lg: 'md',
                  }}
                  variant="unstyled"
                >
                  <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
                    <Tr>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBoxOpen} />
                          <Text ms={2}>Nama Barang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faTag} />
                          <Text ms={2}>Harga Jual</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faBagShopping} />
                          <Text ms={2}>Kuantitas</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faWarehouse} />
                          <Text ms={2}>Gudang</Text>
                        </HStack>
                      </Th>
                      <Th>
                        <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCashRegister} />
                          <Text ms={2}>Sub Total</Text>
                        </HStack>
                      </Th>
                      <Th textAlign="center">
                        <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                          <IconChakra icon={faCircleInfo} />
                          <Text ms={2}>Status</Text>
                        </Center>
                      </Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {state?.detailStokMasuk?.return_sale?.return_sales_details.map((item, index) => (
                      <Tr key={item.id}>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.product.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item?.product.sell_price)}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">
                            {item.qty}
                            {' '}
                            pcs
                          </Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{item?.product.warehouse.name}</Text>
                        </Td>
                        <Td>
                          <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.subtotal)}</Text>
                        </Td>
                        <Td>
                          <SelectChakra
                            isDisabled={state.dataSelectedStatus[index].value === 1}
                            id="status"
                            isSearchable
                            onChange={(e) => onSelectUpdateField(e, index, 'selectedStatus', item.id)}
                            value={state.dataSelectedStatus[index]}
                            options={state.dataStatus}
                            isOptionDisabled={(option) => option.value === 0}
                            styles={customStyles}
                            w="100%"
                            border="1px solid"
                            borderColor="primary.300"
                            borderRadius={5}
                            color="basic.500"
                            _focus={{
                              color: 'basic.800',
                            }}
                            menuPortalTarget={document.body}
                          />
                        </Td>
                      </Tr>
                    ))}
                  </Tbody>
                </Table>
              </TableContainer>
              <HStack w="100%" justifyContent="space-between">
                <Text fontWeight="bold" fontSize="xl">Total Harga</Text>
                <Text fontSize="xl">{ConvertMoneyIDR(state?.detailStokMasuk?.return_sale?.total)}</Text>
              </HStack>
            </VStack>

          </Flex>
        </VStack>
      </Box>

      <ConfirmChangeStatus isOpen={isOpen} onClose={onClose} fetchChangeStatus={fetchChangeStatus} />

    </DashboardLayout>
  );
}

export function ConfirmChangeStatus({
  isOpen, onClose, fetchChangeStatus,
}) {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader fontSize="lg">
          Status Barang Diposting
        </ModalHeader>
        <ModalBody fontSize="md">
          <Text color="basic.500">
            Apakah Anda yakin status barang diposting?
            Anda tidak dapat membatalkan tindakan ini setelahnya.
          </Text>
        </ModalBody>
        <ModalFooter>
          <Button bg="basic.200" color="basic.700" borderRadius={10} onClick={onClose} me={2} px={5}>Batal</Button>
          <Button colorScheme="primary" color="basic.100" borderRadius={10} px={5} onClick={() => fetchChangeStatus()}>Ya</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
export default DetailStokMasukRetur;
