import React, { useEffect, useReducer, useState } from 'react';
import {
  Link, chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr, Flex, useToast, Box, Button,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faLock, faGear, faList, faBoxesStacked, faCalendar, faCircle, faSort,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, StokMasukReducer } from 'reducer/RincianStok/StokMasukReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';

function StokMasuk() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(StokMasukReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllStokMasuk = () => {
    axios({
      method: 'get',
      url: `history-stocks?limit=${state.showRows}&type=in`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `history-stocks?limit=${state.showRows}&type=in&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllStokMasuk();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const status = {
    pending:
  <HStack color="basic.400">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Pending</Text>
  </HStack>,
    posting:
  <HStack color="success.500">
    <IconChakra icon={faCircle} size="xs" />
    <Text fontWeight="semibold">Posting</Text>
  </HStack>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <>
      <Flex
        flexDir={{
          base: 'column',
          lg: 'row',
        }}
        w="100%"
        mb={7}
        alignItems={{
          base: 'start',
          lg: 'center',
        }}
      >
        <Flex w="100%" flexWrap="wrap" justifyContent="start">
          <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
            <InputGroup>
              <Input
                type="text"
                name="search"
                onChange={onUpdateField}
                value={state.search}
                placeholder="Cari"
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="1px solid"
                borderColor="primary.300"
              />
              <InputRightAddon
                border="1px solid"
                borderColor="primary.300"
              >
                Cari
              </InputRightAddon>
            </InputGroup>
          </FormControl>

          <RangeDate
            propsConfigs={{
              dateNavBtnProps: {
                colorScheme: 'primary',
                variant: 'solid',
              },
              dayOfMonthBtnProps: {
                defaultBtnProps: {
                  borderColor: 'primary.500',
                  _hover: {
                    background: 'primary.500',
                  },
                },
                isInRangeBtnProps: {
                  background: 'primary.400',
                  color: 'basic.100',
                },
                selectedBtnProps: {
                  background: 'primary.500',
                  color: 'basic.100',
                },
                todayBtnProps: {
                  background: 'blue.500',
                  color: 'basic.100',
                },
              },
              inputProps: {
                border: '1px solid',
                borderColor: 'primary.300',
                borderRadius: 5,
                color: 'basic.500',
                w: { base: '40%', xl: '30%' },
                me: 7,
              },
            }}
            selectedDates={selectedDates}
            onDateChange={setSelectedDates}
            w={{ base: '40%', xl: '30%' }}
            border="1px solid"
            borderColor="primary.300"
            borderRadius={5}
            color="basic.500"
            _focus={{
              color: 'basic.800',
            }}
          />

          <IconChakra icon={faSort} className="fa-xl" />
        </Flex>
      </Flex>
      <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
        <Table
          size={{
            base: 'sm',
            lg: 'md',
          }}
          fontSize={{
            base: 'sm',
            lg: 'md',
          }}
          variant="unstyled"
        >
          <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
            <Tr>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faLock} />
                  <Text display={{ base: 'none', lg: 'block' }}>ID STOK MASUK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>ID</Text>
                    <Text>STOK MASUK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faCalendar} />
                  <Text display={{ base: 'none', lg: 'block' }}>TANGGAL MASUK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>TANGGAL</Text>
                    <Text>MASUK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faList} />
                  <Text display={{ base: 'none', lg: 'block' }}>NOMOR REFERENSI</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>NOMOR </Text>
                    <Text>REFERENSI</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>JENIS STOK</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>JENIS</Text>
                    <Text>STOK</Text>
                  </Flex>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faGear} />
                  <Text ms={2}>STATUS</Text>
                </HStack>
              </Th>
              <Th>
                <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                  <IconChakra icon={faBoxesStacked} />
                  <Text display={{ base: 'none', lg: 'block' }}>JENIS PEMBAYARAN</Text>
                  <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                    <Text>JENIS</Text>
                    <Text>PEMBAYARAN</Text>
                  </Flex>
                </HStack>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {state.dataStokMasuk.map((item) => (
              <Tr key={item.id}>
                <Td>
                  <Link
                    as={ReachLink}
                    to={item.reference_type.toLowerCase() === 'pembelian' ? `/rincian-stok/stok-masuk/detail-stok-masuk/${item.id}`
                      : `/rincian-stok/stok-masuk/detail-stok-masuk-retur/${item.id}`}
                    color="secondary.500"
                    fontWeight="bold"
                    textDecoration="underline"
                    _hover={{ cursor: 'pointer' }}
                  >
                    {item.id}
                  </Link>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.date}</Text>
                </Td>
                <Td>
                  <Text fontWeight="semibold" color="basic.700">{item.reference_number}</Text>
                </Td>
                <Td>
                  <Box
                    w="fit-content"
                    px={3}
                    py={1}
                    borderRadius={5}
                    bg={item.reference_type.toLowerCase() === 'pembelian' ? 'purple.100' : 'blue.100'}
                    color={item.reference_type.toLowerCase() === 'pembelian' ? 'purple.600' : 'blue.600'}
                  >
                    <Text fontWeight="semibold">{item.reference_type}</Text>
                  </Box>
                </Td>
                <Td>
                  {item.is_posting === 0 ? status.pending : status.posting}
                </Td>
                <Td>
                  <Box w="fit-content" px={3} py={2} borderRadius={5} bg={item.payment_method.toLowerCase() === 'tunai' ? 'success.500' : 'error.500'}>
                    <Text color="basic.100" fontWeight="semibold">{item.payment_method.toLowerCase() === 'tunai' ? 'Tunai' : 'Kredit'}</Text>
                  </Box>
                </Td>
              </Tr>
            ))}
            <Tr borderTop="2px solid" borderColor="basic.300">
              <Td colSpan="99">
                <HStack>
                  <Text>Show rows per page</Text>
                  <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                    {showRows.map((item) => (
                      <option key={item.id} value={item.row}>{item.row}</option>
                    ))}
                  </Select>
                  <Spacer />
                  <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                    <IconChakra
                      icon={faChevronLeft}
                      fontSize="sm"
                    />
                  </Button>
                  <Text>
                    {state?.dataPaginasi.current_page}
                    {' '}
                    of
                    {' '}
                    {state?.dataPaginasi.last_page}
                  </Text>
                  <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                    <IconChakra
                      icon={faChevronRight}
                      fontSize="sm"
                    />
                  </Button>
                </HStack>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </>
  );
}

export default StokMasuk;
