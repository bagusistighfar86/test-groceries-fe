import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Text, useToast, VStack, Flex, Button,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import useTransportasiFormValidator from 'utils/useTransportasiFormValidator';
import { INITIAL_STATE, TransportasiReducer } from 'reducer/TransportasiReducer';
import ReactSelect from 'react-select';

function AddTransportasi() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(ReactSelect);
  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(TransportasiReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useTransportasiFormValidator(state.form);

  const fetchGetAllTipeKendaraan = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'transportation_types',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataTipeKendaraan',
            data: dataBaru.map((item) => ({ value: item.id, label: item.name })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchGetAllDriver = () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    axios({
      method: 'get',
      url: 'drivers',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data;
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataDriver',
            data: dataBaru.map((item) => ({ value: item.id, label: item.fullname })),
          },
        });
        dispatch({ type: 'SET_IS_LOADING', payload: false });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const fetchAddTransportasi = async () => {
    await axios({
      method: 'post',
      url: 'transportations',
      data: {
        license_plate: state.form.nomorKendaraan,
        transportation_type_id: state.form.tipeKendaraan,
        driver_id: state.form.driver,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil menambahkan data',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal menambahkan data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetAllTipeKendaraan();
    fetchGetAllDriver();
  }, []);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSelectUpdateField = (e, name, idSelected) => {
    const field = name;
    const nextFormState = {
      ...state.form,
      [field]: e.value,
    };
    const dataSelected = {
      value: e.value, label: e.label,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    dispatch({
      type: 'SET_SELECTED_SELECT',
      payload: {
        dataName: idSelected,
        data: dataSelected,
      },
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  const onSubmitFormAddTransportasi = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchAddTransportasi();
    navigate('/manajemen-transportasi');
  };

  const onSubmitAddFormDriverAgain = async (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    await fetchAddTransportasi();
    navigate(0);
  };

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Transportasi</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nomor Kendaraan</FormLabel>
                      <Input
                        type="text"
                        name="nomorKendaraan"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.nomorKendaraan ? state.form.nomorKendaraan : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.nomorKendaraan.dirty && errors.nomorKendaraan.error ? (
                        <Text color="red" my={2}>{errors.nomorKendaraan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Tipe Kendaraan</FormLabel>
                      <SelectChakra
                        id="tipeKendaraan"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'tipeKendaraan', 'selectedTipeKendaraan')}
                        value={state.selectedTipeKendaraan}
                        options={state.dataTipeKendaraan}
                        styles={customStyles}
                        placeholder="Pilih tipe kendaraan"
                        w="100% "
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.tipeKendaraan.dirty && errors.tipeKendaraan.error ? (
                        <Text color="red" my={2}>{errors.tipeKendaraan.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Driver</FormLabel>
                      <SelectChakra
                        id="driver"
                        isSearchable
                        onChange={(e) => onSelectUpdateField(e, 'driver', 'selectedDriver')}
                        value={state.selectedDriver}
                        options={state.dataDriver}
                        styles={customStyles}
                        placeholder="Pilih driver"
                        w="100% "
                        border="1px solid"
                        borderColor="primary.300"
                        borderRadius={5}
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.driver.dirty && errors.driver.error ? (
                        <Text color="red" my={2}>{errors.driver.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack spacing={5} mt={12}>
                  <Button
                    type="button"
                    onClick={onSubmitAddFormDriverAgain}
                    border="1px solid"
                    borderColor="primary.500"
                    variant="outline"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.700"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                    }}
                  >
                    Simpan dan Buat Lagi
                  </Button>
                  <Button
                    type="button"
                    onClick={onSubmitFormAddTransportasi}
                    bg="primary.500"
                    variant="solid"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.100"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                      bg: 'primary.600',
                    }}
                  >
                    Simpan
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default AddTransportasi;
