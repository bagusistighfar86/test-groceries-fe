import React, { useEffect, useReducer } from 'react';
import {
  Box, chakra, FormControl, FormLabel, HStack, Input, Select, Text, useToast, VStack, Flex, Button,
} from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate, useParams } from 'react-router-dom';
import { INITIAL_STATE, DriverReducer } from 'reducer/DriverReducer';
import useDriverFormValidator from 'utils/useDriverFormValidator';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';

function EditDriver() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const { id } = useParams();

  const auth = { accessToken: getCookie('accessToken') };

  const jenisKelamin = [
    { id: '1', gender: 'Laki - Laki' },
    { id: '2', gender: 'Perempuan' },
  ];

  const [state, dispatch] = useReducer(DriverReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useDriverFormValidator(state.form);

  const fetchGetDriverByID = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'get',
      url: `drivers/${id}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({
          type: 'ON_FETCH_GET_BY_ID',
          payload: {
            data: res.data.data,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      navigate(-1);
      toast({
        title: 'Gagal mengambil data',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    fetchGetDriverByID();
  }, []);

  const fetchEditDriver = async () => {
    dispatch({ type: 'SET_IS_LOADING', payload: true });
    await axios({
      method: 'put',
      url: `drivers/${id}`,
      data: {
        fullname: state.form?.namaLengkap,
        phone: state.form?.telepon,
        gender: state.form?.jenisKelamin,
        nik_or_npwp: state.form?.nik,
        address: state.form?.alamat,
        status_id: state.form?.status,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        navigate('/manajemen-driver');
        toast({
          title: 'Data berhasil diubah',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Data gagal diubah',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  useEffect(() => {
    const form = {
      namaLengkap: state.detailDriver?.fullname,
      telepon: state.detailDriver?.phone,
      jenisKelamin: state.detailDriver?.gender,
      nik: state.detailDriver?.nik_or_npwp,
      alamat: state.detailDriver?.address,
      status: state.detailDriver?.status_id,
    };
    dispatch({
      type: 'SET_FORM',
      payload: form,
    });
  }, [state.detailDriver]);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const onSubmitFormEditDriver = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEditDriver();
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <Box w="100%" py={5} borderBottom="1px solid" borderColor="basic.200">
            <Button as={ReachLink} onClick={() => navigate(-1)} variant="unstyled" display="flex" justifyContent="start" _hover={{ cursor: 'pointer' }} fontSize="xl" fontWeight="bold">
              <IconChakra icon={faArrowLeft} me={3} />
              <Text>Kembali</Text>
            </Button>
          </Box>

          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Driver</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Lengkap</FormLabel>
                      <Input
                        type="text"
                        name="namaLengkap"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.namaLengkap ? state.form.namaLengkap : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.namaLengkap.dirty && errors.namaLengkap.error ? (
                        <Text color="red" my={2}>{errors.namaLengkap.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="telepon"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.telepon ? state.form.telepon : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.telepon.dirty && errors.telepon.error ? (
                        <Text color="red" my={2}>{errors.telepon.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Jenis Kelamin</FormLabel>
                      <Select
                        name="jenisKelamin"
                        value={state.form.jenisKelamin ? state.form.jenisKelamin : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      >
                        {jenisKelamin.map((item) => (
                          <option key={item.id} value={item.gender}>{item.gender}</option>
                        ))}
                      </Select>
                      {errors.jenisKelamin.dirty && errors.jenisKelamin.error ? (
                        <Text color="red" my={2}>{errors.jenisKelamin.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">NIK / NPWP</FormLabel>
                      <Input
                        name="nik"
                        type="text"
                        w="100%"
                        value={state.form.nik ? state.form.nik : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.nik.dirty && errors.nik.error ? (
                        <Text color="red" my={2}>{errors.nik.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Alamat</FormLabel>
                      <Input
                        type="text"
                        name="alamat"
                        w="100%"
                        value={state.form.alamat ? state.form.alamat : ''}
                        onChange={onUpdateField}
                        onBlur={onBlurField}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      {errors.alamat.dirty && errors.alamat.error ? (
                        <Text color="red" my={2}>{errors.alamat.message}</Text>
                      ) : null}
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack spacing={5} mt={12}>
                  <Button
                    as={ReachLink}
                    onClick={() => navigate(-1)}
                    border="1px solid"
                    borderColor="primary.500"
                    variant="outline"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.700"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                    }}
                  >
                    Batal
                  </Button>
                  <Button
                    type="button"
                    onClick={onSubmitFormEditDriver}
                    bg="primary.500"
                    variant="solid"
                    fontSize="sm"
                    px={5}
                    py={2}
                    borderRadius={5}
                    color="basic.100"
                    fontWeight="bold"
                    _hover={{
                      textDecoration: 'none',
                      cursor: 'pointer',
                      bg: 'primary.600',
                    }}
                  >
                    Simpan
                  </Button>
                </HStack>
              </VStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditDriver;
