import React, { useReducer } from 'react';
import {
  Box, FormControl, FormLabel, HStack, Input, Text, useToast, Button, VStack, Flex,
} from '@chakra-ui/react';
import Avvvatars from 'avvvatars-react';
import { EditProfileReducer, INITIAL_STATE } from 'reducer/EditProfileReducer';
import useEditProfileFormValidator from 'utils/useEditProfileFormValidator';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { deleteAllCookies } from 'utils/SetCookies';

function EditProfile() {
  const navigate = useNavigate();
  const toast = useToast();

  const [state, dispatch] = useReducer(EditProfileReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useEditProfileFormValidator(state);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
    if (errors[field].dirty) {
      validateForm({
        form: nextFormState,
        errors,
        field,
      });
    }
  };

  const fetchEdit = () => {
    axios({
      method: 'post',
      url: 'api/login',
      data: {
        email: state.email,
        password: state.password,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        return res;
      }
      return res;
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Failed Edit Profile',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    const { isValid } = validateForm({ form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchEdit();
  };

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" borderBottom="1px solid" borderColor="basic.200" py={10}>
            <Box flexBasis="20%">
              <Text fontWeight="bold" fontSize="xl">Foto Profil</Text>
            </Box>
            <Box w="80%">
              <Avvvatars value="Username" size={100} />
            </Box>
          </HStack>
          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Diri</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Nama Lengkap</FormLabel>
                      <Input
                        type="text"
                        name="nama"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.nama ? state.nama : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.nama.dirty && errors.nama.error ? (
                      <Text color="red" my={2}>{errors.nama.message}</Text>
                    ) : null}
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl isRequired>
                      <FormLabel fontWeight="bold">Nomor Telepon</FormLabel>
                      <Input
                        type="text"
                        name="telepon"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.telepon ? state.telepon : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                    {errors.telepon.dirty && errors.telepon.error ? (
                      <Text color="red" my={2}>{errors.telepon.message}</Text>
                    ) : null}
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Tanggal Lahir</FormLabel>
                      <Input
                        type="date"
                        w="100%"
                        variant="filled"
                        disabled
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Role</FormLabel>
                      <Input
                        type="text"
                        w="100%"
                        variant="filled"
                        disabled
                        value="Administration"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <VStack flexBasis="50%" alignItems="start">
                    <FormControl w="95%" isRequired>
                      <FormLabel fontWeight="bold">Email</FormLabel>
                      <Input
                        type="email"
                        w="100%"
                        variant="filled"
                        disabled
                        value="username@gmail.com"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                    </FormControl>
                  </VStack>
                </HStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Button
                  as={ReachLink}
                  onClick={() => navigate(-1)}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Batal
                </Button>
                <Button
                  type="button"
                  onClick={onSubmitForm}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default EditProfile;
