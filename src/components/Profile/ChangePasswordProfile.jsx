import React, { useReducer } from 'react';
import {
  Box, FormControl, FormLabel, HStack, chakra, Input, Text, useToast, VStack, InputRightElement, Button, InputGroup,
} from '@chakra-ui/react';
import axios from 'axios';
import DashboardLayout from 'components/layout/DashboardLayout';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import useChangePasswordProfileFormValidator from 'utils/useChangePasswordProfileFormValidator';
import { INITIAL_STATE, ChangePasswordProfileReducer } from 'reducer/ChangePasswordProfileReduce';

function ChangePasswordProfile() {
  const toast = useToast();
  const navigate = useNavigate();
  const IconChakra = chakra(FontAwesomeIcon);

  const auth = { accessToken: getCookie('accessToken') };

  const [state, dispatch] = useReducer(ChangePasswordProfileReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useChangePasswordProfileFormValidator(state.form);

  const handleShowOldPassword = () => dispatch({ type: 'SET_SHOW_OLD_PASSWORD' });

  const handleShowPassword = () => dispatch({ type: 'SET_SHOW_PASSWORD' });

  const handleShowConfirmPassword = () => dispatch({ type: 'SET_SHOW_CONFIRM_PASSWORD' });

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchChangePassword = () => {
    dispatch({ type: 'BEFORE_FETCH' });
    axios({
      method: 'post',
      url: 'change-password',
      data: {
        old_password: state.form.oldPassword,
        new_password: state.form.newPassword,
        confirm_password: state.form.confirmNewPassword,
      },
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.data.status >= 200 && res.data.status < 300) {
        toast({
          title: 'Berhasil mengubah kata sandi',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        navigate(-1);
      } else {
        toast({
          title: 'Gagal mengubah kata sandi',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengubah kata sandi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchChangePassword();
  };

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack
            alignItems="start"
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Ubah Kata Sandi</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" spacing={12}>
                <VStack
                  w={{
                    base: '60%',
                    lg: '50%',
                    xl: '40%',
                  }}
                  alignItems="start"
                >
                  <FormControl w="100%" isRequired>
                    <FormLabel fontWeight="bold">Kata Sandi Lama</FormLabel>
                    <InputGroup>
                      <Input
                        type={state.showOldPassword ? 'text' : 'password'}
                        name="oldPassword"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.oldPassword ? state.form.oldPassword : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowOldPassword}>
                          {state.showOldPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                  </FormControl>
                  {errors.oldPassword.dirty && errors.oldPassword.error ? (
                    <Text color="red" my={2}>{errors.oldPassword.message}</Text>
                  ) : null}
                </VStack>
                <VStack
                  w={{
                    base: '60%',
                    lg: '50%',
                    xl: '40%',
                  }}
                  alignItems="start"
                >
                  <FormControl w="100%" isRequired>
                    <FormLabel fontWeight="bold">Kata Sandi Baru</FormLabel>
                    <InputGroup>
                      <Input
                        type={state.showNewPassword ? 'text' : 'password'}
                        name="newPassword"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.newPassword ? state.form.newPassword : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowPassword}>
                          {state.showNewPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                  </FormControl>
                  {errors.newPassword.dirty && errors.newPassword.error ? (
                    <Text color="red" my={2}>{errors.newPassword.message}</Text>
                  ) : null}
                </VStack>
                <VStack
                  w={{
                    base: '60%',
                    lg: '50%',
                    xl: '40%',
                  }}
                  alignItems="start"
                >
                  <FormControl w="100%" isRequired>
                    <FormLabel fontWeight="bold">Konfirmasi Kata Sandi Baru</FormLabel>
                    <InputGroup>
                      <Input
                        type={state.showConfirmNewPassword ? 'text' : 'password'}
                        name="confirmNewPassword"
                        w="100%"
                        onBlur={onBlurField}
                        onChange={onUpdateField}
                        value={state.form.confirmNewPassword ? state.form.confirmNewPassword : ''}
                        variant="outline"
                        border="2px solid"
                        borderColor="primary.300"
                        color="basic.500"
                        _focus={{
                          color: 'basic.800',
                        }}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowConfirmPassword}>
                          {state.showConfirmNewPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                  </FormControl>
                  {errors.confirmNewPassword.dirty && errors.confirmNewPassword.error ? (
                    <Text color="red" my={2}>{errors.confirmNewPassword.message}</Text>
                  ) : null}
                </VStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Button
                  as={ReachLink}
                  onClick={() => navigate(-1)}
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Batal
                </Button>
                <Button
                  type="button"
                  onClick={onSubmitForm}
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Simpan
                </Button>
              </HStack>
            </Box>
          </HStack>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default ChangePasswordProfile;
