import AddKaryawan from 'components/ManajemenKaryawan/AddKaryawan';
import DetailKaryawan from 'components/ManajemenKaryawan/DetailKaryawan';
import EditKaryawan from 'components/ManajemenKaryawan/EditKaryawan';
import AddGudang from 'components/ManajemenGudang/AddGudang';
import DetailGudang from 'components/ManajemenGudang/DetailGudang';
import EditGudang from 'components/ManajemenGudang/EditGudang';
import ChangePasswordProfile from 'components/Profile/ChangePasswordProfile';
import EditProfile from 'components/Profile/EditProfile';
import React from 'react';
import { Routes, Route } from 'react-router-dom';
import ChangePassword from 'views/ChangePassword';
import Dashboard from 'views/Dashboard';
import Keuangan from 'views/Keuangan';
import Laporan from 'views/Laporan';
import Login from 'views/Login';
import ManajemenKaryawan from 'views/ManajemenKaryawan';
import MasterBarang from 'views/MasterBarang';
import ManajemenGudang from 'views/ManajemenGudang';
import Pelanggan from 'views/Pelanggan';
import Profile from 'views/Profile';
import VerificationForgotPassword from 'views/VerificationForgotPassword';
import LoadingPage from 'views/LoadingPage';
import EditKategoriBarang from 'components/MasterBarang/KategoriBarang/EditKategoriBarang';
import AddKategoriBarang from 'components/MasterBarang/KategoriBarang/AddKategoriBarang';
import AddMerekBarang from 'components/MasterBarang/MerekBarang/AddMerekBarang';
import EditMerekBarang from 'components/MasterBarang/MerekBarang/EditMerekBarang';
import DetailBarang from 'components/MasterBarang/Barang/DetailBarang';
import EditBarang from 'components/MasterBarang/Barang/EditBarang';
import AddBarang from 'components/MasterBarang/Barang/AddBarang';
import DetailPelanggan from 'components/Pelanggan/DetailPelanggan';
import EditPelanggan from 'components/Pelanggan/EditPelanggan';
import AddPelanggan from 'components/Pelanggan/AddPelanggan';
import ManajemenDriver from 'views/ManajemenDriver';
import DetailDriver from 'components/Distribusi/Driver/DetailDriver';
import EditDriver from 'components/Distribusi/Driver/EditDriver';
import AddDriver from 'components/Distribusi/Driver/AddDriver';
import Pembelian from 'views/Pembelian';
import AddPembelianButuhDiproses from 'components/Pembelian/PembelianButuhDiproses/AddPembelianButuhDiproses';
import DetailPembelianButuhDiproses from 'components/Pembelian/PembelianButuhDiproses/DetailPembelianButuhDiproses';
import EditPembelianButuhDiproses from 'components/Pembelian/PembelianButuhDiproses/EditPembelianButuhDiproses';
import ManajemenTransportasi from 'views/ManajemenTransportasi';
import AddTransportasi from 'components/Distribusi/Transportasi/AddTransportasi';
import EditTransportasi from 'components/Distribusi/Transportasi/EditTransportasi';
import RincianStok from 'views/RincianStok';
import DetailStokMasuk from 'components/Distribusi/RincianStok/StokMasuk/DetailStokMasuk';
import Penjualan from 'views/Penjualan';
import AddPenjualan from 'components/Penjualan/PenjualanButuhDiproses/AddPenjualan';
import EditPenjualan from 'components/Penjualan/PenjualanButuhDiproses/EditPenjualan';
import DetailPenjualanDiproses from 'components/Penjualan/PenjualanButuhDiproses/DetailPenjualanDiproses';
import DetailPenjualanSelesai from 'components/Penjualan/PenjualanSelesai/DetailPenjualanSelesai';
import DetailPenjualanRetur from 'components/Penjualan/PenjualanRetur/DetailPenjualanRetur';
import DetailPenjualanBatal from 'components/Penjualan/PenjualanDibatalkan/DetailPenjualanBatal';
import AddPenjualanRetur from 'components/Penjualan/PenjualanRetur/AddPenjualanRetur';
import DetailPembelianSelesai from 'components/Pembelian/PembelianSelesai/DetailPembelianSelesai';
import DetailPembelianBatal from 'components/Pembelian/PembelianDibatalkan/DetailPembelianBatal';
import AddPembelianRetur from 'components/Pembelian/PembelianRetur/AddPembelianRetur';
import DetailPembelianRetur from 'components/Pembelian/PembelianRetur/DetailPembelianRetur';
import TrackingDistribusi from 'views/TrackingDistribusi';
import DetailTrackingDikirim from 'components/Distribusi/TrackingDistribusi/TrackingDikirim/DetailTrackingDikirim';
import DetailTrackingSelesai from 'components/Distribusi/TrackingDistribusi/TrackingSelesai/DetailTrackingSelesai';
import DetailStokKeluar from 'components/Distribusi/RincianStok/StokKeluar/DetailStokKeluar';
import AddOpname from 'components/Distribusi/RincianStok/OpnameStok/AddOpname';
import DetailOpnameStok from 'components/Distribusi/RincianStok/OpnameStok/DetailOpnameStok';
import DetailKasMasuk from 'components/Keuangan/KasMasuk/DetailKasMasuk';
import PencatatanHutang from 'views/PencatatanHutang';
import DetailLaporanPembelianSelesai from 'components/Laporan/LaporanPembelian/DetailLaporanPembelianSelesai';
import DetailLaporanPembelianRetur from 'components/Laporan/LaporanPembelian/DetailLaporanPembelianRetur';
import DetailLaporanPenjualanSelesai from 'components/Laporan/LaporanPenjualan/DetailLaporanPenjualanSelesai';
import DetailLaporanPenjualanRetur from 'components/Laporan/LaporanPenjualan/DetailLaporanPenjualanRetur';
import PencatatanPiutang from 'views/PencatatanPiutang';
import DetailPencatatanHutang from 'components/PencatatanHutang/DetailPencatatanHutang';
import RiwayatPembayaranHutang from 'components/PencatatanHutang/RiwayatPembayaranHutang';
import RiwayatPembayaranPiutang from 'components/PencatatanPiutang/RiwayatPembayaranPiutang';
import DetailPencatatanPiutang from 'components/PencatatanPiutang/DetailPencatatanPiutang';
import DetailStokMasukRetur from 'components/Distribusi/RincianStok/StokMasuk/DetailStokMasukRetur';
import DetailStokKeluarRetur from 'components/Distribusi/RincianStok/StokKeluar/DetailStokKeluarRetur';
import DetailKasMasukRetur from 'components/Keuangan/KasMasuk/DetailKasMasukRetur';
import DetailKasKeluar from 'components/Keuangan/KasKeluar/DetailKasKeluar';
import DetailKasKeluarRetur from 'components/Keuangan/KasKeluar/DetailKasKeluarRetur';
import Jurnal from 'views/Jurnal';
import ManajemenSupplier from 'views/ManajemenSupplier';
import AddSupplier from 'components/ManajemenSupplier/Supplier/AddSuppllier';
import DetailSupplier from 'components/ManajemenSupplier/Supplier/DetailSupplier';
import EditSupplier from 'components/ManajemenSupplier/Supplier/EditSupplier';
import DetailPersetujuanSupplier from 'components/ManajemenSupplier/Persetujuan/DetailPersetujuanSupplier';

function App() {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/lupa-password" element={<VerificationForgotPassword />} />
      <Route path="/reset-password/:accessToken" element={<ChangePassword />} />

      <Route path="/" element={<Dashboard />} />
      <Route path="/manajemen-karyawan" element={<ManajemenKaryawan />} />
      <Route path="/manajemen-karyawan/tambah-karyawan" element={<AddKaryawan />} />
      <Route path="/manajemen-karyawan/detail-karyawan/:id" element={<DetailKaryawan />} />
      <Route path="/manajemen-karyawan/edit-karyawan/:id" element={<EditKaryawan />} />

      <Route path="/pembelian" element={<Pembelian />} />
      <Route path="/pembelian/tambah-pembelian" element={<AddPembelianButuhDiproses />} />
      <Route path="/pembelian/detail-pembelian/:id" element={<DetailPembelianButuhDiproses />} />
      <Route path="/pembelian/edit-pembelian/:id" element={<EditPembelianButuhDiproses />} />

      <Route path="/pembelian/pembelian-selesai/detail-pembelian/:id" element={<DetailPembelianSelesai />} />

      <Route path="/pembelian/pembelian-retur/tambah-retur/:id" element={<AddPembelianRetur />} />
      <Route path="/pembelian/pembelian-retur/detail-retur/:id" element={<DetailPembelianRetur />} />

      <Route path="/pembelian/pembelian-batal/detail-pembelian/:id" element={<DetailPembelianBatal />} />

      <Route path="/penjualan" element={<Penjualan />} />
      <Route path="/penjualan/tambah-penjualan" element={<AddPenjualan />} />
      <Route path="/penjualan/detail-penjualan/:id" element={<DetailPenjualanDiproses />} />
      <Route path="/penjualan/penjualan-butuh-diproses/edit-penjualan/:id" element={<EditPenjualan />} />

      <Route path="/penjualan/penjualan-selesai/detail-penjualan/:id" element={<DetailPenjualanSelesai />} />

      <Route path="/penjualan/penjualan-retur/tambah-retur/:id" element={<AddPenjualanRetur />} />
      <Route path="/penjualan/penjualan-retur/detail-retur/:id" element={<DetailPenjualanRetur />} />

      <Route path="/penjualan/penjualan-batal/detail-penjualan/:id" element={<DetailPenjualanBatal />} />

      <Route path="/pencatatan-hutang" element={<PencatatanHutang />} />
      <Route path="/pencatatan-hutang/detail-hutang/:id" element={<DetailPencatatanHutang />} />
      <Route path="/pencatatan-hutang/detail-hutang/riwayat-pembayaran/:id" element={<RiwayatPembayaranHutang />} />
      <Route path="/pencatatan-piutang" element={<PencatatanPiutang />} />
      <Route path="/pencatatan-piutang/detail-piutang/:id" element={<DetailPencatatanPiutang />} />
      <Route path="/pencatatan-piutang/detail-piutang/riwayat-pembayaran/:id" element={<RiwayatPembayaranPiutang />} />

      <Route path="/manajemen-gudang" element={<ManajemenGudang />} />
      <Route path="/manajemen-gudang/tambah-gudang" element={<AddGudang />} />
      <Route path="/manajemen-gudang/detail-gudang/:id" element={<DetailGudang />} />
      <Route path="/manajemen-gudang/edit-gudang/:id" element={<EditGudang />} />

      <Route path="/rincian-stok" element={<RincianStok />} />
      <Route path="/rincian-stok/stok-masuk/detail-stok-masuk/:id" element={<DetailStokMasuk />} />
      <Route path="/rincian-stok/stok-masuk/detail-stok-masuk-retur/:id" element={<DetailStokMasukRetur />} />
      <Route path="/rincian-stok/stok-keluar/detail-stok-keluar/:id" element={<DetailStokKeluar />} />
      <Route path="/rincian-stok/stok-keluar/detail-stok-keluar-retur/:id" element={<DetailStokKeluarRetur />} />

      <Route path="/rincian-stok/opname-stok/tambah-opname" element={<AddOpname />} />
      <Route path="/rincian-stok/opname-stok/detail-opname/:id" element={<DetailOpnameStok />} />

      <Route path="/pelanggan" element={<Pelanggan />} />
      <Route path="/pelanggan/tambah-pelanggan" element={<AddPelanggan />} />
      <Route path="/pelanggan/detail-pelanggan/:id" element={<DetailPelanggan />} />
      <Route path="/pelanggan/edit-pelanggan/:id" element={<EditPelanggan />} />

      <Route path="/supplier" element={<ManajemenSupplier />} />
      <Route path="/supplier/tambah-supplier" element={<AddSupplier />} />
      <Route path="/supplier/detail-supplier/:id" element={<DetailSupplier />} />
      <Route path="/supplier/detail-persetujuan-supplier/:id" element={<DetailPersetujuanSupplier />} />
      <Route path="/supplier/edit-supplier/:id" element={<EditSupplier />} />

      <Route path="/master-barang" element={<MasterBarang />} />
      <Route path="/master-barang/tambah-barang" element={<AddBarang />} />
      <Route path="/master-barang/detail-barang/:id" element={<DetailBarang />} />
      <Route path="/master-barang/edit-barang/:id" element={<EditBarang />} />
      <Route path="/master-barang/tambah-kategori-barang/" element={<AddKategoriBarang />} />
      <Route path="/master-barang/edit-kategori-barang/:id" element={<EditKategoriBarang />} />
      <Route path="/master-barang/tambah-merek-barang/" element={<AddMerekBarang />} />
      <Route path="/master-barang/edit-merek-barang/:id" element={<EditMerekBarang />} />

      <Route path="/keuangan" element={<Keuangan />} />
      <Route path="/keuangan/kas-masuk/detail-kas-masuk/:id" element={<DetailKasMasuk />} />
      <Route path="/keuangan/kas-masuk/detail-kas-masuk-retur/:id" element={<DetailKasMasukRetur />} />
      <Route path="/keuangan/kas-keluar/detail-kas-keluar/:id" element={<DetailKasKeluar />} />
      <Route path="/keuangan/kas-keluar/detail-kas-keluar-retur/:id" element={<DetailKasKeluarRetur />} />

      <Route path="/laporan" element={<Laporan />} />
      <Route path="/laporan/laporan-pembelian/detail-laporan-selesai/:id" element={<DetailLaporanPembelianSelesai />} />
      <Route path="/laporan/laporan-pembelian/detail-laporan-retur/:id" element={<DetailLaporanPembelianRetur />} />
      <Route path="/laporan/laporan-penjualan/detail-laporan-selesai/:id" element={<DetailLaporanPenjualanSelesai />} />
      <Route path="/laporan/laporan-penjualan/detail-laporan-retur/:id" element={<DetailLaporanPenjualanRetur />} />

      <Route path="/manajemen-driver" element={<ManajemenDriver />} />
      <Route path="/manajemen-driver/tambah-driver" element={<AddDriver />} />
      <Route path="/manajemen-driver/detail-driver/:id" element={<DetailDriver />} />
      <Route path="/manajemen-driver/edit-driver/:id" element={<EditDriver />} />

      <Route path="/tracking-distribusi" element={<TrackingDistribusi />} />
      <Route path="/tracking-distribusi/tracking-dikirim/detail-tracking/:id" element={<DetailTrackingDikirim />} />
      <Route path="/tracking-distribusi/tracking-selesai/detail-tracking/:id" element={<DetailTrackingSelesai />} />

      <Route path="/manajemen-transportasi" element={<ManajemenTransportasi />} />
      <Route path="/manajemen-transportasi/tambah-transportasi" element={<AddTransportasi />} />
      <Route path="/manajemen-transportasi/edit-transportasi/:id" element={<EditTransportasi />} />

      <Route path="/jurnal" element={<Jurnal />} />

      <Route path="/profile" element={<Profile />} />
      <Route path="/profile/edit-profile" element={<EditProfile />} />
      <Route path="/profile/change-password" element={<ChangePasswordProfile />} />

      <Route path="/loadingPage" element={<LoadingPage />} />
    </Routes>

  );
}

export default App;
