/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const laporanSlicer = createSlice({
  name: 'laporan',
  initialState: {
    indexTab: 0,
  },
  reducers: {
    setIndexTab: (state, action) => {
      if (!action.payload) {
        state.indexTab = 0;
      } else {
        state.indexTab = action.payload;
      }
    },
  },
});

export const { setIndexTab } = laporanSlicer.actions;
export default laporanSlicer.reducer;
