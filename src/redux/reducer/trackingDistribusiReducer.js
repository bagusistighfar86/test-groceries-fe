/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const trackingDistribusiSlicer = createSlice({
  name: 'trackingDistribusi',
  initialState: {
    indexTab: 0,
  },
  reducers: {
    setIndexTab: (state, action) => {
      if (!action.payload) {
        state.indexTab = 0;
      } else {
        state.indexTab = action.payload;
      }
    },
  },
});

export const { setIndexTab } = trackingDistribusiSlicer.actions;
export default trackingDistribusiSlicer.reducer;
