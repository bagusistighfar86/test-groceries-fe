/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import { deleteCookie, setCookie } from 'utils/SetCookies';

export const navigationSlicer = createSlice({
  name: 'navigation',
  initialState: {
    prevPath: '',
  },
  reducers: {
    setPrevPath: (state, action) => {
      if (!action.payload) {
        state.prevPath = '';
        deleteCookie('prevPath');
      } else {
        state.prevPath = action.payload;
        setCookie('prevPath', action.payload, 30);
      }
    },
  },
});

export const { setPrevPath } = navigationSlicer.actions;
export default navigationSlicer.reducer;
