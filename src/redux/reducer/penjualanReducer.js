/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const penjualanSlicer = createSlice({
  name: 'penjualan',
  initialState: {
    indexTab: 0,
  },
  reducers: {
    setIndexTab: (state, action) => {
      if (!action.payload) {
        state.indexTab = 0;
      } else {
        state.indexTab = action.payload;
      }
    },
  },
});

export const { setIndexTab } = penjualanSlicer.actions;
export default penjualanSlicer.reducer;
