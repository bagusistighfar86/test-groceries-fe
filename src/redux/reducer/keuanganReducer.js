/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const keuanganSlicer = createSlice({
  name: 'keuangan',
  initialState: {
    indexTab: 0,
  },
  reducers: {
    setIndexTab: (state, action) => {
      if (!action.payload) {
        state.indexTab = 0;
      } else {
        state.indexTab = action.payload;
      }
    },
  },
});

export const { setIndexTab } = keuanganSlicer.actions;
export default keuanganSlicer.reducer;
