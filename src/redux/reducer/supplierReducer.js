/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const supplierSlicer = createSlice({
  name: 'supplier',
  initialState: {
    indexTab: 0,
  },
  reducers: {
    setIndexTab: (state, action) => {
      if (!action.payload) {
        state.indexTab = 0;
      } else {
        state.indexTab = action.payload;
      }
    },
  },
});

export const { setIndexTab } = supplierSlicer.actions;
export default supplierSlicer.reducer;
