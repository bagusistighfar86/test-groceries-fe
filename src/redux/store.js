import { configureStore } from '@reduxjs/toolkit';
import supplierReducer from './reducer/supplierReducer';
import barangReducer from './reducer/barangReducer';
import pembelianReducer from './reducer/pembelianReducer';
import penjualanReducer from './reducer/penjualanReducer';
import trackingDistribusiReducer from './reducer/trackingDistribusiReducer';
import keuanganReducer from './reducer/keuanganReducer';
import laporanReducer from './reducer/laporanReducer';
import jurnalReducer from './reducer/jurnalReducer';
import navigationReducer from './reducer/navigationReducer';

export default configureStore({
  reducer: {
    supplier: supplierReducer,
    barang: barangReducer,
    pembelian: pembelianReducer,
    penjualan: penjualanReducer,
    trackingDistribusi: trackingDistribusiReducer,
    keuangan: keuanganReducer,
    laporan: laporanReducer,
    jurnal: jurnalReducer,
    navigation: navigationReducer,
  },
});
