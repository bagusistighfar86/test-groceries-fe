export const barangIDValidator = (barangID) => {
  if (!barangID) {
    return 'Barang wajib dipilih';
  }
  return '';
};

export const kuantitasValidator = (kuantitas) => {
  if (!kuantitas) {
    return 'Kuantitas wajib diisi';
  }
  return '';
};
