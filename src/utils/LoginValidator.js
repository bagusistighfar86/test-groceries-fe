export const emailValidator = (email) => {
  const emailRgx = /\S+@\S+\.\S+/;

  if (!email) {
    return 'Email wajib diisi';
  } if (!email.match(emailRgx)) {
    return 'Masukkan email anda dengan format ex: nama@contoh.com';
  }
  return '';
};

export const passwordValidator = (password) => {
  if (!password) {
    return 'Kata sandi wajib diisi';
  } if (password.length < 8) {
    return 'Kata sandi minimal 8 karakter';
  }
  return '';
};
