export const supplierIDValidator = (supplierID) => {
  if (!supplierID) {
    return 'Supplier wajib dipilih';
  }
  return '';
};

export const noPesananValidator = (noPesanan) => {
  if (!noPesanan) {
    return 'No pesanan wajib diisi';
  }
  return '';
};

export const jenisPembayaranValidator = (jenisPembayaran) => {
  if (!jenisPembayaran) {
    return 'Jenis pembayaran wajib dipilih';
  }
  return '';
};
