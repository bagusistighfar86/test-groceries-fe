const emailValidator = (email) => {
  const emailRgx = /\S+@\S+\.\S+/;

  if (!email) {
    return 'Email wajib diisi';
  } if (!email.match(emailRgx)) {
    return 'Masukkan email anda dengan format ex: nama@contoh.com';
  }
  return '';
};

export default emailValidator;
