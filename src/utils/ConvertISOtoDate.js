function ConvertISOtoDate(string) {
  const [yyyy, mm, dd] = string.split(/[/:\-T]/);

  return `${dd}/${mm}/${yyyy}`;
}

export default ConvertISOtoDate;
