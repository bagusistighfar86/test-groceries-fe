const jenisPengembalianValidator = (jenisPengembalian) => {
  if (!jenisPengembalian) {
    return 'Jenis pengembalian wajib dipilih';
  }
  return '';
};

export default jenisPengembalianValidator;
