/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaKategoriBarangValidator,
} from 'utils/KategoriBarangValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useGudangFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaKategoriBarang: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaKategoriBarang,
    } = form;

    if (nextErrors.namaKategoriBarang.dirty && (field ? field === 'namaKategoriBarang' : true)) {
      const namaKategoriBarangMessage = namaKategoriBarangValidator(namaKategoriBarang, form);
      nextErrors.namaKategoriBarang.error = !!namaKategoriBarangMessage;
      nextErrors.namaKategoriBarang.message = namaKategoriBarangMessage;
      if (namaKategoriBarangMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useGudangFormValidator;
