const gudangIDValidator = (gudangID) => {
  if (!gudangID) {
    return 'Gudang wajib dipilih';
  }
  return '';
};

export default gudangIDValidator;
