/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  nomorKendaraanValidator,
  tipeKendaraanValidator,
  driverValidator,
} from 'utils/TransportasiValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useTransportasiFormValidator = (form) => {
  const [errors, setErrors] = useState({
    nomorKendaraan: {
      dirty: false,
      error: false,
      message: '',
    },
    tipeKendaraan: {
      dirty: false,
      error: false,
      message: '',
    },
    driver: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      nomorKendaraan, tipeKendaraan, driver,
    } = form;

    if (nextErrors.nomorKendaraan.dirty && (field ? field === 'nomorKendaraan' : true)) {
      const nomorKendaraanMessage = nomorKendaraanValidator(nomorKendaraan, form);
      nextErrors.nomorKendaraan.error = !!nomorKendaraanMessage;
      nextErrors.nomorKendaraan.message = nomorKendaraanMessage;
      if (nomorKendaraanMessage) isValid = false;
    }

    if (nextErrors.tipeKendaraan.dirty && (field ? field === 'tipeKendaraan' : true)) {
      const tipeKendaraanMessage = tipeKendaraanValidator(tipeKendaraan, form);
      nextErrors.tipeKendaraan.error = !!tipeKendaraanMessage;
      nextErrors.tipeKendaraan.message = tipeKendaraanMessage;
      if (tipeKendaraanMessage) isValid = false;
    }

    if (nextErrors.driver.dirty && (field ? field === 'driver' : true)) {
      const driverMessage = driverValidator(driver, form);
      nextErrors.driver.error = !!driverMessage;
      nextErrors.driver.message = driverMessage;
      if (driverMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useTransportasiFormValidator;
