export const namaValidator = (nama) => {
  if (!nama) {
    return 'Nama lengkap wajib diisi';
  }
  return '';
};

export const teleponValidator = (telepon) => {
  if (!telepon) {
    return 'Nomor telepon wajib diisi';
  }
  return '';
};

export const jenisKelaminValidator = (jenisKelamin) => {
  if (!jenisKelamin) {
    return 'Jenis Kelamin wajib diisi';
  }
  return '';
};

export const tanggalLahirValidator = (tanggalLahir) => {
  if (!tanggalLahir) {
    return 'Tanggal Lahir wajib diisi';
  }
  return '';
};

export const nikValidator = (nik) => {
  if (!nik) {
    return 'NIK / NPWP wajib diisi';
  }
  return '';
};

export const alamatValidator = (alamat) => {
  if (!alamat) {
    return 'Alamat wajib diisi';
  }
  return '';
};

export const emailValidator = (email) => {
  const emailRgx = /\S+@\S+\.\S+/;

  if (!email) {
    return 'Email wajib diisi';
  } if (!email.match(emailRgx)) {
    return 'Masukkan email anda dengan format ex: nama@contoh.com';
  }
  return '';
};

export const passwordValidator = (password) => {
  if (!password) {
    return 'Kata sandi wajib diisi';
  } if (password.length < 8) {
    return 'Kata sandi minimal 8 karakter';
  }
  return '';
};

export const jabatanValidator = (jabatan) => {
  if (!jabatan) {
    return 'Jabatan wajib diisi';
  }
  return '';
};

export const roleValidator = (role) => {
  if (!role) {
    return 'Role wajib diisi';
  }
  return '';
};
