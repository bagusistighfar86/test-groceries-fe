function getDateTimeNow() {
  const today = new Date();
  const yyyy = today.getFullYear();
  let mm = today.getMonth() + 1; // Months start at 0!
  let dd = today.getDate();
  let HH = today.getHours();
  const MM = today.getMinutes();
  let SS = today.getSeconds();

  if (dd < 10) dd = `0${dd}`;
  if (mm < 10) mm = `0${mm}`;
  if (HH < 10) HH = `0${HH}`;
  if (SS < 10) SS = `0${SS}`;

  return `${dd}/${mm}/${yyyy}, ${HH}:${MM}:${SS}`;
}

export default getDateTimeNow;
