/* eslint-disable no-shadow */
import { useState } from 'react';
import jenisPengembalianValidator from './PenjualanReturValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const usePenjualanReturFormValidator = (form) => {
  const [errors, setErrors] = useState({
    jenisPengembalian: {
      dirty: true,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      jenisPengembalian,
    } = form;

    if (nextErrors.jenisPengembalian.dirty && (field ? field === 'jenisPengembalian' : true)) {
      const jenisPengembalianMessage = jenisPengembalianValidator(jenisPengembalian, form);
      nextErrors.jenisPengembalian.error = !!jenisPengembalianMessage;
      nextErrors.jenisPengembalian.message = jenisPengembalianMessage;
      if (jenisPengembalianMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default usePenjualanReturFormValidator;
