export const namaGudangValidator = (namaGudang) => {
  if (!namaGudang) {
    return 'Nama Gudang wajib diisi';
  }
  return '';
};

export const staffGudangIDValidator = (staffGudangID) => {
  if (!staffGudangID) {
    return 'Staff Gudang wajib diisi';
  }
  return '';
};

export const teleponGudangValidator = (teleponGudang) => {
  if (!teleponGudang) {
    return 'Nomor telepon wajib diisi';
  } if (teleponGudang.length < 10) {
    return 'Nomor telepon minimal 10 digit';
  }
  return '';
};

export const alamatGudangValidator = (alamatGudang) => {
  if (!alamatGudang) {
    return 'Alamat wajib diisi';
  }
  return '';
};
