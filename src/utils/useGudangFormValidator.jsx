/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaGudangValidator,
  staffGudangIDValidator,
  teleponGudangValidator,
  alamatGudangValidator,
} from 'utils/GudangValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useGudangFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaGudang: {
      dirty: false,
      error: false,
      message: '',
    },
    staffGudangID: {
      dirty: false,
      error: false,
      message: '',
    },
    teleponGudang: {
      dirty: false,
      error: false,
      message: '',
    },
    alamatGudang: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaGudang, staffGudangID, teleponGudang, alamatGudang,
    } = form;

    if (nextErrors.namaGudang.dirty && (field ? field === 'namaGudang' : true)) {
      const namaGudangMessage = namaGudangValidator(namaGudang, form);
      nextErrors.namaGudang.error = !!namaGudangMessage;
      nextErrors.namaGudang.message = namaGudangMessage;
      if (namaGudangMessage) isValid = false;
    }

    if (nextErrors.staffGudangID.dirty && (field ? field === 'staffGudangID' : true)) {
      const staffGudangIDMessage = staffGudangIDValidator(staffGudangID, form);
      nextErrors.staffGudangID.error = !!staffGudangIDMessage;
      nextErrors.staffGudangID.message = staffGudangIDMessage;
      if (staffGudangIDMessage) isValid = false;
    }

    if (nextErrors.teleponGudang.dirty && (field ? field === 'teleponGudang' : true)) {
      const teleponGudangMessage = teleponGudangValidator(teleponGudang, form);
      nextErrors.teleponGudang.error = !!teleponGudangMessage;
      nextErrors.teleponGudang.message = teleponGudangMessage;
      if (teleponGudangMessage) isValid = false;
    }

    if (nextErrors.alamatGudang.dirty && (field ? field === 'alamatGudang' : true)) {
      const alamatGudangMessage = alamatGudangValidator(alamatGudang, form);
      nextErrors.alamatGudang.error = !!alamatGudangMessage;
      nextErrors.alamatGudang.message = alamatGudangMessage;
      if (alamatGudangMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useGudangFormValidator;
