export const oldPasswordValidator = (oldPassword) => {
  if (!oldPassword) {
    return 'Kata sandi lama wajib diisi';
  } if (oldPassword.length < 8) {
    return 'Kata sandi lama minimal 8 karakter';
  }
  return '';
};

export const newPasswordValidator = (newPassword) => {
  if (!newPassword) {
    return 'Kata sandi baru wajib diisi';
  } if (newPassword.length < 8) {
    return 'Kata sandi baru minimal 8 karakter';
  }
  return '';
};

export const confirmNewPasswordValidator = (confirmNewPassword, form) => {
  if (!confirmNewPassword) {
    return 'Konfirmasi kata sandi baru wajib diisi';
  } if (confirmNewPassword.length < 8) {
    return 'Kata sandi baru minimal 8 karakter';
  } if (confirmNewPassword !== form.newPassword) {
    return 'Kata sandi baru tidak sesuai';
  }
  return '';
};
