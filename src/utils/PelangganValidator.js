export const namaLengkapValidator = (namaLengkap) => {
  if (!namaLengkap) {
    return 'Nama lengkap wajib diisi';
  }
  return '';
};

export const teleponValidator = (telepon) => {
  if (!telepon) {
    return 'Nomor telepon wajib diisi';
  } if (telepon.length < 10) {
    return 'Nomor telepon minimal 10 digit';
  }
  return '';
};

export const jenisKelaminValidator = (jenisKelamin) => {
  if (!jenisKelamin) {
    return 'Jenis Kelamin wajib diisi';
  }
  return '';
};

export const nikValidator = (nik) => {
  if (!nik) {
    return 'NIK / NPWP wajib diisi';
  } if (nik.length !== 16) {
    return 'NIK / NPWP harus 16 digit';
  }
  return '';
};

export const namaTokoValidator = (namaToko) => {
  if (!namaToko) {
    return 'Nama toko wajib diisi';
  }
  return '';
};

export const batasKreditValidator = (batasKredit) => {
  if (!batasKredit) {
    return 'Batas kredit wajib diisi';
  }
  return '';
};

export const jatuhTempoValidator = (jatuhTempo) => {
  if (!jatuhTempo) {
    return 'Jatuh tempo wajib diisi';
  }
  return '';
};

export const negaraValidator = (negara) => {
  if (!negara) {
    return 'Negara wajib diisi';
  }
  return '';
};

export const provinsiValidator = (provinsi) => {
  if (!provinsi) {
    return 'Provinsi wajib diisi';
  }
  return '';
};

export const kotaValidator = (kota) => {
  if (!kota) {
    return 'Kota wajib diisi';
  }
  return '';
};

export const kecamatanValidator = (kecamatan) => {
  if (!kecamatan) {
    return 'Kecamatan wajib diisi';
  }
  return '';
};

export const kodePosValidator = (kodePos) => {
  if (!kodePos) {
    return 'Kode pos wajib diisi';
  }
  return '';
};

export const alamatTokoValidator = (alamatToko) => {
  if (!alamatToko) {
    return 'Alamat toko wajib diisi';
  }
  return '';
};
