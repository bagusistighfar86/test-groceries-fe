/* eslint-disable no-shadow */
import { useState } from 'react';

import gudangIDValidator from 'utils/OpnameValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useOpnameFormValidator = (form) => {
  const [errors, setErrors] = useState({
    gudangID: {
      dirty: true,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      gudangID,
    } = form;

    if (nextErrors.gudangID.dirty && (field ? field === 'gudangID' : true)) {
      const gudangIDMessage = gudangIDValidator(gudangID, form);
      nextErrors.gudangID.error = !!gudangIDMessage;
      nextErrors.gudangID.message = gudangIDMessage;
      if (gudangIDMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useOpnameFormValidator;
