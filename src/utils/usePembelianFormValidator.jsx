/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  supplierIDValidator,
  noPesananValidator,
  jenisPembayaranValidator,
} from 'utils/PembelianValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const usePembelianFormValidator = (form) => {
  const [errors, setErrors] = useState({
    supplierID: {
      dirty: true,
      error: false,
      message: '',
    },
    noPesanan: {
      dirty: false,
      error: false,
      message: '',
    },
    jenisPembayaran: {
      dirty: true,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      supplierID, noPesanan, jenisPembayaran,
    } = form;

    if (nextErrors.supplierID.dirty && (field ? field === 'supplierID' : true)) {
      const supplierIDMessage = supplierIDValidator(supplierID, form);
      nextErrors.supplierID.error = !!supplierIDMessage;
      nextErrors.supplierID.message = supplierIDMessage;
      if (supplierIDMessage) isValid = false;
    }

    if (nextErrors.noPesanan.dirty && (field ? field === 'noPesanan' : true)) {
      const noPesananMessage = noPesananValidator(noPesanan, form);
      nextErrors.noPesanan.error = !!noPesananMessage;
      nextErrors.noPesanan.message = noPesananMessage;
      if (noPesananMessage) isValid = false;
    }

    if (nextErrors.jenisPembayaran.dirty && (field ? field === 'jenisPembayaran' : true)) {
      const jenisPembayaranMessage = jenisPembayaranValidator(jenisPembayaran, form);
      nextErrors.jenisPembayaran.error = !!jenisPembayaranMessage;
      nextErrors.jenisPembayaran.message = jenisPembayaranMessage;
      if (jenisPembayaranMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default usePembelianFormValidator;
