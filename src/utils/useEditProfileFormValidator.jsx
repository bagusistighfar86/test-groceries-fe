/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaValidator,
  teleponValidator,
  passwordValidator,
} from 'utils/EditProfileValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useEditProfileFormValidator = (form) => {
  const [errors, setErrors] = useState({
    nama: {
      dirty: false,
      error: false,
      message: '',
    },
    telepon: {
      dirty: false,
      error: false,
      message: '',
    },
    password: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      nama, telepon, password,
    } = form;

    if (nextErrors.nama.dirty && (field ? field === 'nama' : true)) {
      const namaMessage = namaValidator(nama, form);
      nextErrors.nama.error = !!namaMessage;
      nextErrors.nama.message = namaMessage;
      if (namaMessage) isValid = false;
    }

    if (nextErrors.telepon.dirty && (field ? field === 'telepon' : true)) {
      const teleponMessage = teleponValidator(telepon, form);
      nextErrors.telepon.error = !!teleponMessage;
      nextErrors.telepon.message = teleponMessage;
      if (teleponMessage) isValid = false;
    }

    if (nextErrors.password.dirty && (field ? field === 'password' : true)) {
      const passwordMessage = passwordValidator(password, form);
      nextErrors.password.error = !!passwordMessage;
      nextErrors.password.message = passwordMessage;
      if (passwordMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useEditProfileFormValidator;
