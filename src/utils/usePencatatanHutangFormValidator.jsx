/* eslint-disable no-shadow */
import { useState } from 'react';

import { nominalBayarValidator, buktiPembayaranValidator } from 'utils/PencatatanHutangValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const usePencatatanHutangFormValidator = (form) => {
  const [errors, setErrors] = useState({
    nominalBayar: {
      dirty: true,
      error: false,
      message: '',
    },
    buktiPembayaran: {
      dirty: true,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      nominalBayar, buktiPembayaran,
    } = form;

    if (nextErrors.nominalBayar.dirty && (field ? field === 'nominalBayar' : true)) {
      const nominalBayarMessage = nominalBayarValidator(nominalBayar, form);
      nextErrors.nominalBayar.error = !!nominalBayarMessage;
      nextErrors.nominalBayar.message = nominalBayarMessage;
      if (nominalBayarMessage) isValid = false;
    }

    if (nextErrors.buktiPembayaran.dirty && (field ? field === 'buktiPembayaran' : true)) {
      const buktiPembayaranMessage = buktiPembayaranValidator(buktiPembayaran, form);
      nextErrors.buktiPembayaran.error = !!buktiPembayaranMessage;
      nextErrors.buktiPembayaran.message = buktiPembayaranMessage;
      if (buktiPembayaranMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default usePencatatanHutangFormValidator;
