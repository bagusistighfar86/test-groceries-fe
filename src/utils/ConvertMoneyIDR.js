const ConvertMoneyIDR = (num) => {
  const numString = num.toString();
  const split = numString.split(',');
  const sisa = split[0].length % 3;
  let rupiah = split[0].substr(0, sisa);
  const ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi num ribuan
  if (ribuan) {
    const separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] !== undefined ? `${rupiah},${split[1]}` : rupiah;

  if (rupiah) {
    return `Rp. ${rupiah},-`;
  }
  return '';
};

export default ConvertMoneyIDR;
