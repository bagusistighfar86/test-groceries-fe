/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  barangIDValidator,
  stokAkhirValidator,
} from 'utils/BarangOpnameFormValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useBarangOpnameFormValidator = (form) => {
  const [errors, setErrors] = useState({
    barangID: {
      dirty: true,
      error: false,
      message: '',
    },
    stokAkhir: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      barangID, stokAkhir,
    } = form;

    if (nextErrors.barangID.dirty && (field ? field === 'barangID' : true)) {
      const barangIDMessage = barangIDValidator(barangID, form);
      nextErrors.barangID.error = !!barangIDMessage;
      nextErrors.barangID.message = barangIDMessage;
      if (barangIDMessage) isValid = false;
    }

    if (nextErrors.stokAkhir.dirty && (field ? field === 'stokAkhir' : true)) {
      const stokAkhirMessage = stokAkhirValidator(stokAkhir, form);
      nextErrors.stokAkhir.error = !!stokAkhirMessage;
      nextErrors.stokAkhir.message = stokAkhirMessage;
      if (stokAkhirMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useBarangOpnameFormValidator;
