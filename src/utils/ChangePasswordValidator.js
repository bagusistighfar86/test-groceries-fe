export const emailValidator = (email) => {
  const emailRgx = /\S+@\S+\.\S+/;

  if (!email) {
    return 'Email wajib diisi';
  } if (!email.match(emailRgx)) {
    return 'Masukkan email anda dengan format ex: nama@contoh.com';
  }
  return '';
};

export const newPasswordValidator = (newPassword) => {
  if (!newPassword) {
    return 'Kata sandi baru wajib diisi';
  } if (newPassword.length < 8) {
    return 'Kata sandi baru minimal 8 karakter';
  }
  return '';
};

export const confirmNewPasswordValidator = (confirmNewPassword, form) => {
  if (!confirmNewPassword) {
    return 'Konfirmasi kata sandi baru wajib diisi';
  } if (confirmNewPassword.length < 8) {
    return 'Kata sandi baru minimal 8 karakter';
  } if (confirmNewPassword !== form.newPassword) {
    return 'Kata sandi baru tidak sesuai';
  }
  return '';
};
