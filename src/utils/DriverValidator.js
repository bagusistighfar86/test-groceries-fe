export const namaLengkapValidator = (namaLengkap) => {
  if (!namaLengkap) {
    return 'Nama lengkap wajib diisi';
  }
  return '';
};

export const teleponValidator = (telepon) => {
  if (!telepon) {
    return 'Nomor telepon wajib diisi';
  } if (telepon.length < 10) {
    return 'Nomor telepon minimal 10 digit';
  }
  return '';
};

export const jenisKelaminValidator = (jenisKelamin) => {
  if (!jenisKelamin) {
    return 'Jenis Kelamin wajib diisi';
  }
  return '';
};

export const nikValidator = (nik) => {
  if (!nik) {
    return 'NIK / NPWP wajib diisi';
  } if (nik.length !== 16) {
    return 'NIK / NPWP harus 16 digit';
  }
  return '';
};

export const alamatValidator = (alamat) => {
  if (!alamat) {
    return 'Alamat wajib diisi';
  }
  return '';
};
