/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  barangIDValidator,
  kuantitasReturValidator,
} from 'utils/BarangPembelianReturFormValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useBarangPembelianReturFormValidator = (form) => {
  const [errors, setErrors] = useState({
    barangID: {
      dirty: true,
      error: false,
      message: '',
    },
    kuantitasRetur: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      barangID, kuantitasRetur,
    } = form;

    if (nextErrors.barangID.dirty && (field ? field === 'barangID' : true)) {
      const barangIDMessage = barangIDValidator(barangID, form);
      nextErrors.barangID.error = !!barangIDMessage;
      nextErrors.barangID.message = barangIDMessage;
      if (barangIDMessage) isValid = false;
    }

    if (nextErrors.kuantitasRetur.dirty && (field ? field === 'kuantitasRetur' : true)) {
      const kuantitasReturMessage = kuantitasReturValidator(kuantitasRetur, form);
      nextErrors.kuantitasRetur.error = !!kuantitasReturMessage;
      nextErrors.kuantitasRetur.message = kuantitasReturMessage;
      if (kuantitasReturMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useBarangPembelianReturFormValidator;
