export const nomorKendaraanValidator = (nomorKendaraan) => {
  if (!nomorKendaraan) {
    return 'Nomor kendaraan wajib diisi';
  }
  return '';
};

export const tipeKendaraanValidator = (tipeKendaraan) => {
  if (!tipeKendaraan) {
    return 'Tipe kendaraan wajib diisi';
  }
  return '';
};

export const driverValidator = (driver) => {
  if (!driver) {
    return 'Driver wajib diisi';
  }
  return '';
};
