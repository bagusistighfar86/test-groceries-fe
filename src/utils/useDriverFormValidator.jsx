/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaLengkapValidator,
  teleponValidator,
  jenisKelaminValidator,
  nikValidator,
  alamatValidator,
} from 'utils/DriverValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useDriverFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaLengkap: {
      dirty: false,
      error: false,
      message: '',
    },
    telepon: {
      dirty: false,
      error: false,
      message: '',
    },
    jenisKelamin: {
      dirty: false,
      error: false,
      message: '',
    },
    nik: {
      dirty: false,
      error: false,
      message: '',
    },
    alamat: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaLengkap, telepon, jenisKelamin, nik, alamat,
    } = form;

    if (nextErrors.namaLengkap.dirty && (field ? field === 'namaLengkap' : true)) {
      const namaLengkapMessage = namaLengkapValidator(namaLengkap, form);
      nextErrors.namaLengkap.error = !!namaLengkapMessage;
      nextErrors.namaLengkap.message = namaLengkapMessage;
      if (namaLengkapMessage) isValid = false;
    }

    if (nextErrors.telepon.dirty && (field ? field === 'telepon' : true)) {
      const teleponMessage = teleponValidator(telepon, form);
      nextErrors.telepon.error = !!teleponMessage;
      nextErrors.telepon.message = teleponMessage;
      if (teleponMessage) isValid = false;
    }

    if (nextErrors.jenisKelamin.dirty && (field ? field === 'jenisKelamin' : true)) {
      const jenisKelaminMessage = jenisKelaminValidator(jenisKelamin, form);
      nextErrors.jenisKelamin.error = !!jenisKelaminMessage;
      nextErrors.jenisKelamin.message = jenisKelaminMessage;
      if (jenisKelaminMessage) isValid = false;
    }

    if (nextErrors.nik.dirty && (field ? field === 'nik' : true)) {
      const nikMessage = nikValidator(nik, form);
      nextErrors.nik.error = !!nikMessage;
      nextErrors.nik.message = nikMessage;
      if (nikMessage) isValid = false;
    }

    if (nextErrors.alamat.dirty && (field ? field === 'alamat' : true)) {
      const alamatMessage = alamatValidator(alamat, form);
      nextErrors.alamat.error = !!alamatMessage;
      nextErrors.alamat.message = alamatMessage;
      if (alamatMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useDriverFormValidator;
