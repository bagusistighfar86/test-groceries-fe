export const barangIDValidator = (barangID) => {
  if (!barangID) {
    return 'Barang wajib dipilih';
  }
  return '';
};

export const kuantitasReturValidator = (kuantitasRetur) => {
  if (!kuantitasRetur) {
    return 'Kuantitas wajib diisi';
  }
  return '';
};
