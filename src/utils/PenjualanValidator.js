export const pelangganIDValidator = (pelangganID) => {
  if (!pelangganID) {
    return 'Pelanggan wajib dipilih';
  }
  return '';
};

export const jenisPembayaranValidator = (jenisPembayaran) => {
  if (!jenisPembayaran) {
    return 'Jenis pembayaran wajib dipilih';
  }
  return '';
};

export const tipeKendaraanValidator = (tipeKendaraan) => {
  if (!tipeKendaraan) {
    return 'Tipe kendaraan wajib diisi';
  }
  return '';
};

export const tanggalPengirimanValidator = (tanggalPengiriman) => {
  if (!tanggalPengiriman) {
    return 'Tanggal pengiriman wajib diisi';
  }
  return '';
};
