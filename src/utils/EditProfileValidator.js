export const namaValidator = (nama) => {
  if (!nama) {
    return 'Nama lengkap wajib diisi';
  }
  return '';
};

export const teleponValidator = (telepon) => {
  if (!telepon) {
    return 'Nomor telepon wajib diisi';
  }
  return '';
};

export const passwordValidator = (password) => {
  if (!password) {
    return 'Kata sandi wajib diisi';
  } if (password.length < 8) {
    return 'Kata sandi minimal 8 karakter';
  }
  return '';
};
