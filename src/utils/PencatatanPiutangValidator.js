export const nominalBayarValidator = (nominalBayar) => {
  if (!nominalBayar) {
    return 'Nominal bayar dipilih';
  }
  return '';
};

export const buktiPembayaranValidator = (buktiPembayaran) => {
  if (!buktiPembayaran) {
    return 'Bukti pembayaran dipilih';
  }
  return '';
};
