/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaMerekBarangValidator,
} from 'utils/MerekBarangValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useGudangFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaMerekBarang: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaMerekBarang,
    } = form;

    if (nextErrors.namaMerekBarang.dirty && (field ? field === 'namaMerekBarang' : true)) {
      const namaMerekBarangMessage = namaMerekBarangValidator(namaMerekBarang, form);
      nextErrors.namaMerekBarang.error = !!namaMerekBarangMessage;
      nextErrors.namaMerekBarang.message = namaMerekBarangMessage;
      if (namaMerekBarangMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useGudangFormValidator;
