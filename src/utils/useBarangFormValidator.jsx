/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaBarangValidator,
  hargaJualValidator,
  hargaBeliValidator,
  limitStokValidator,
  kategoriProdukIDValidator,
  supplierIDValidator,
  gudangIDValidator,
  merekIDValidator,
} from 'utils/BarangValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useBarangFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaBarang: {
      dirty: false,
      error: false,
      message: '',
    },
    hargaJual: {
      dirty: false,
      error: false,
      message: '',
    },
    hargaBeli: {
      dirty: false,
      error: false,
      message: '',
    },
    limitStok: {
      dirty: false,
      error: false,
      message: '',
    },
    kategoriProdukID: {
      dirty: false,
      error: false,
      message: '',
    },
    supplierID: {
      dirty: false,
      error: false,
      message: '',
    },
    gudangID: {
      dirty: false,
      error: false,
      message: '',
    },
    merekID: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaBarang, hargaJual, hargaBeli, limitStok,
      kategoriProdukID, supplierID, gudangID, merekID,
    } = form;

    if (nextErrors.namaBarang.dirty && (field ? field === 'namaBarang' : true)) {
      const namaBarangMessage = namaBarangValidator(namaBarang, form);
      nextErrors.namaBarang.error = !!namaBarangMessage;
      nextErrors.namaBarang.message = namaBarangMessage;
      if (namaBarangMessage) isValid = false;
    }

    if (nextErrors.hargaJual.dirty && (field ? field === 'hargaJual' : true)) {
      const hargaJualMessage = hargaJualValidator(hargaJual, form);
      nextErrors.hargaJual.error = !!hargaJualMessage;
      nextErrors.hargaJual.message = hargaJualMessage;
      if (hargaJualMessage) isValid = false;
    }

    if (nextErrors.hargaBeli.dirty && (field ? field === 'hargaBeli' : true)) {
      const hargaBeliMessage = hargaBeliValidator(hargaBeli, form);
      nextErrors.hargaBeli.error = !!hargaBeliMessage;
      nextErrors.hargaBeli.message = hargaBeliMessage;
      if (hargaBeliMessage) isValid = false;
    }

    if (nextErrors.limitStok.dirty && (field ? field === 'limitStok' : true)) {
      const limitStokMessage = limitStokValidator(limitStok, form);
      nextErrors.limitStok.error = !!limitStokMessage;
      nextErrors.limitStok.message = limitStokMessage;
      if (limitStokMessage) isValid = false;
    }

    if (nextErrors.kategoriProdukID.dirty && (field ? field === 'kategoriProdukID' : true)) {
      const kategoriProdukIDMessage = kategoriProdukIDValidator(kategoriProdukID, form);
      nextErrors.kategoriProdukID.error = !!kategoriProdukIDMessage;
      nextErrors.kategoriProdukID.message = kategoriProdukIDMessage;
      if (kategoriProdukIDMessage) isValid = false;
    }

    if (nextErrors.supplierID.dirty && (field ? field === 'supplierID' : true)) {
      const supplierIDMessage = supplierIDValidator(supplierID, form);
      nextErrors.supplierID.error = !!supplierIDMessage;
      nextErrors.supplierID.message = supplierIDMessage;
      if (supplierIDMessage) isValid = false;
    }

    if (nextErrors.gudangID.dirty && (field ? field === 'gudangID' : true)) {
      const gudangIDMessage = gudangIDValidator(gudangID, form);
      nextErrors.gudangID.error = !!gudangIDMessage;
      nextErrors.gudangID.message = gudangIDMessage;
      if (gudangIDMessage) isValid = false;
    }

    if (nextErrors.merekID.dirty && (field ? field === 'merekID' : true)) {
      const merekIDMessage = merekIDValidator(merekID, form);
      nextErrors.merekID.error = !!merekIDMessage;
      nextErrors.merekID.message = merekIDMessage;
      if (merekIDMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useBarangFormValidator;
