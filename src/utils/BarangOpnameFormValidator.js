export const barangIDValidator = (barangID) => {
  if (!barangID) {
    return 'Barang wajib dipilih';
  }
  return '';
};

export const stokAkhirValidator = (stokAkhir) => {
  if (!stokAkhir) {
    return 'Stok akhir wajib diisi';
  }
  return '';
};
