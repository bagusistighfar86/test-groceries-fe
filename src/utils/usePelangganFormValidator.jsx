/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaLengkapValidator,
  teleponValidator,
  jenisKelaminValidator,
  nikValidator,
  namaTokoValidator,
  batasKreditValidator,
  jatuhTempoValidator,
  negaraValidator,
  provinsiValidator,
  kotaValidator,
  kecamatanValidator,
  kodePosValidator,
  alamatTokoValidator,
} from 'utils/PelangganValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useSupplierFormValidator = (form) => {
  const [errors, setErrors] = useState({
    namaLengkap: {
      dirty: false,
      error: false,
      message: '',
    },
    telepon: {
      dirty: false,
      error: false,
      message: '',
    },
    jenisKelamin: {
      dirty: false,
      error: false,
      message: '',
    },
    nik: {
      dirty: false,
      error: false,
      message: '',
    },
    namaToko: {
      dirty: false,
      error: false,
      message: '',
    },
    batasKredit: {
      dirty: false,
      error: false,
      message: '',
    },
    jatuhTempo: {
      dirty: false,
      error: false,
      message: '',
    },
    negara: {
      dirty: false,
      error: false,
      message: '',
    },
    provinsi: {
      dirty: false,
      error: false,
      message: '',
    },
    kota: {
      dirty: false,
      error: false,
      message: '',
    },
    kecamatan: {
      dirty: false,
      error: false,
      message: '',
    },
    kodePos: {
      dirty: false,
      error: false,
      message: '',
    },
    alamatToko: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      namaLengkap, telepon, jenisKelamin, nik, namaToko, batasKredit, jatuhTempo, negara, provinsi, kota, kecamatan, kodePos, alamatToko,
    } = form;

    if (nextErrors.namaLengkap.dirty && (field ? field === 'namaLengkap' : true)) {
      const namaLengkapMessage = namaLengkapValidator(namaLengkap, form);
      nextErrors.namaLengkap.error = !!namaLengkapMessage;
      nextErrors.namaLengkap.message = namaLengkapMessage;
      if (namaLengkapMessage) isValid = false;
    }

    if (nextErrors.telepon.dirty && (field ? field === 'telepon' : true)) {
      const teleponMessage = teleponValidator(telepon, form);
      nextErrors.telepon.error = !!teleponMessage;
      nextErrors.telepon.message = teleponMessage;
      if (teleponMessage) isValid = false;
    }

    if (nextErrors.jenisKelamin.dirty && (field ? field === 'jenisKelamin' : true)) {
      const jenisKelaminMessage = jenisKelaminValidator(jenisKelamin, form);
      nextErrors.jenisKelamin.error = !!jenisKelaminMessage;
      nextErrors.jenisKelamin.message = jenisKelaminMessage;
      if (jenisKelaminMessage) isValid = false;
    }

    if (nextErrors.nik.dirty && (field ? field === 'nik' : true)) {
      const nikMessage = nikValidator(nik, form);
      nextErrors.nik.error = !!nikMessage;
      nextErrors.nik.message = nikMessage;
      if (nikMessage) isValid = false;
    }

    if (nextErrors.namaToko.dirty && (field ? field === 'namaToko' : true)) {
      const namaTokoMessage = namaTokoValidator(namaToko, form);
      nextErrors.namaToko.error = !!namaTokoMessage;
      nextErrors.namaToko.message = namaTokoMessage;
      if (namaTokoMessage) isValid = false;
    }

    if (nextErrors.batasKredit.dirty && (field ? field === 'batasKredit' : true)) {
      const batasKreditMessage = batasKreditValidator(batasKredit, form);
      nextErrors.batasKredit.error = !!batasKreditMessage;
      nextErrors.batasKredit.message = batasKreditMessage;
      if (batasKreditMessage) isValid = false;
    }

    if (nextErrors.jatuhTempo.dirty && (field ? field === 'jatuhTempo' : true)) {
      const jatuhTempoMessage = jatuhTempoValidator(jatuhTempo, form);
      nextErrors.jatuhTempo.error = !!jatuhTempoMessage;
      nextErrors.jatuhTempo.message = jatuhTempoMessage;
      if (jatuhTempoMessage) isValid = false;
    }

    if (nextErrors.negara.dirty && (field ? field === 'negara' : true)) {
      const negaraMessage = negaraValidator(negara, form);
      nextErrors.negara.error = !!negaraMessage;
      nextErrors.negara.message = negaraMessage;
      if (negaraMessage) isValid = false;
    }

    if (nextErrors.provinsi.dirty && (field ? field === 'provinsi' : true)) {
      const provinsiMessage = provinsiValidator(provinsi, form);
      nextErrors.provinsi.error = !!provinsiMessage;
      nextErrors.provinsi.message = provinsiMessage;
      if (provinsiMessage) isValid = false;
    }

    if (nextErrors.kota.dirty && (field ? field === 'kota' : true)) {
      const kotaMessage = kotaValidator(kota, form);
      nextErrors.kota.error = !!kotaMessage;
      nextErrors.kota.message = kotaMessage;
      if (kotaMessage) isValid = false;
    }

    if (nextErrors.kecamatan.dirty && (field ? field === 'kecamatan' : true)) {
      const kecamatanMessage = kecamatanValidator(kecamatan, form);
      nextErrors.kecamatan.error = !!kecamatanMessage;
      nextErrors.kecamatan.message = kecamatanMessage;
      if (kecamatanMessage) isValid = false;
    }

    if (nextErrors.kodePos.dirty && (field ? field === 'kodePos' : true)) {
      const kodePosMessage = kodePosValidator(kodePos, form);
      nextErrors.kodePos.error = !!kodePosMessage;
      nextErrors.kodePos.message = kodePosMessage;
      if (kodePosMessage) isValid = false;
    }

    if (nextErrors.alamatToko.dirty && (field ? field === 'alamatToko' : true)) {
      const alamatTokoMessage = alamatTokoValidator(alamatToko, form);
      nextErrors.alamatToko.error = !!alamatTokoMessage;
      nextErrors.alamatToko.message = alamatTokoMessage;
      if (alamatTokoMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useSupplierFormValidator;
