/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  oldPasswordValidator,
  newPasswordValidator,
  confirmNewPasswordValidator,
} from './ChangePasswordProfileValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useChangePasswordProfileFormValidator = (form) => {
  const [errors, setErrors] = useState({
    oldPassword: {
      dirty: false,
      error: false,
      message: '',
    },
    newPassword: {
      dirty: false,
      error: false,
      message: '',
    },
    confirmNewPassword: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      oldPassword, newPassword, confirmNewPassword,
    } = form;

    if (nextErrors.oldPassword.dirty && (field ? field === 'oldPassword' : true)) {
      const oldPasswordMessage = oldPasswordValidator(oldPassword, form);
      nextErrors.oldPassword.error = !!oldPasswordMessage;
      nextErrors.oldPassword.message = oldPasswordMessage;
      if (oldPasswordMessage) isValid = false;
    }

    if (nextErrors.newPassword.dirty && (field ? field === 'newPassword' : true)) {
      const newPasswordMessage = newPasswordValidator(newPassword, form);
      nextErrors.newPassword.error = !!newPasswordMessage;
      nextErrors.newPassword.message = newPasswordMessage;
      if (newPasswordMessage) isValid = false;
    }

    if (
      nextErrors.confirmNewPassword.dirty
        && (field ? field === 'confirmNewPassword' : true)
    ) {
      const confirmNewPasswordMessage = confirmNewPasswordValidator(
        confirmNewPassword,
        form,
      );
      nextErrors.confirmNewPassword.error = !!confirmNewPasswordMessage;
      nextErrors.confirmNewPassword.message = confirmNewPasswordMessage;
      if (confirmNewPasswordMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useChangePasswordProfileFormValidator;
