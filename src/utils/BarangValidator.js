export const namaBarangValidator = (namaBarang) => {
  if (!namaBarang) {
    return 'Nama barang wajib diisi';
  }
  return '';
};

export const hargaJualValidator = (hargaBeli) => {
  if (!hargaBeli) {
    return 'Harga Jual wajib diisi';
  }
  return '';
};

export const hargaBeliValidator = (hargaBeli) => {
  if (!hargaBeli) {
    return 'Harga Beli wajib diisi';
  }
  return '';
};

export const limitStokValidator = (limitStok) => {
  if (!limitStok) {
    return 'Limit stok wajib diisi';
  }
  return '';
};

export const kategoriProdukIDValidator = (kategoriProdukID) => {
  if (!kategoriProdukID) {
    return 'Kategori Produk wajib diisi';
  }
  return '';
};

export const supplierIDValidator = (supplierID) => {
  if (!supplierID) {
    return 'Supplier wajib diisi';
  }
  return '';
};

export const gudangIDValidator = (gudangID) => {
  if (!gudangID) {
    return 'Gudang wajib diisi';
  }
  return '';
};

export const merekIDValidator = (merekID) => {
  if (!merekID) {
    return 'Merek wajib diisi';
  }
  return '';
};
