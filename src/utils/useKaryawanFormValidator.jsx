/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  namaValidator,
  teleponValidator,
  jenisKelaminValidator,
  tanggalLahirValidator,
  nikValidator,
  alamatValidator,
  emailValidator,
  passwordValidator,
  jabatanValidator,
  roleValidator,
} from 'utils/KaryawanValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const useKaryawanFormValidator = (form) => {
  const [errors, setErrors] = useState({
    nama: {
      dirty: false,
      error: false,
      message: '',
    },
    telepon: {
      dirty: false,
      error: false,
      message: '',
    },
    jenisKelamin: {
      dirty: false,
      error: false,
      message: '',
    },
    tanggalLahir: {
      dirty: false,
      error: false,
      message: '',
    },
    nik: {
      dirty: false,
      error: false,
      message: '',
    },
    alamat: {
      dirty: false,
      error: false,
      message: '',
    },
    email: {
      dirty: false,
      error: false,
      message: '',
    },
    password: {
      dirty: false,
      error: false,
      message: '',
    },
    jabatan: {
      dirty: false,
      error: false,
      message: '',
    },
    role: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      nama, telepon, jenisKelamin, tanggalLahir, nik, alamat, email, password, jabatan, role,
    } = form;

    if (nextErrors.nama.dirty && (field ? field === 'nama' : true)) {
      const namaMessage = namaValidator(nama, form);
      nextErrors.nama.error = !!namaMessage;
      nextErrors.nama.message = namaMessage;
      if (namaMessage) isValid = false;
    }

    if (nextErrors.telepon.dirty && (field ? field === 'telepon' : true)) {
      const teleponMessage = teleponValidator(telepon, form);
      nextErrors.telepon.error = !!teleponMessage;
      nextErrors.telepon.message = teleponMessage;
      if (teleponMessage) isValid = false;
    }

    if (nextErrors.jenisKelamin.dirty && (field ? field === 'jenisKelamin' : true)) {
      const jenisKelaminMessage = jenisKelaminValidator(jenisKelamin, form);
      nextErrors.jenisKelamin.error = !!jenisKelaminMessage;
      nextErrors.jenisKelamin.message = jenisKelaminMessage;
      if (jenisKelaminMessage) isValid = false;
    }

    if (nextErrors.tanggalLahir.dirty && (field ? field === 'tanggalLahir' : true)) {
      const tanggalLahirMessage = tanggalLahirValidator(tanggalLahir, form);
      nextErrors.tanggalLahir.error = !!tanggalLahirMessage;
      nextErrors.tanggalLahir.message = tanggalLahirMessage;
      if (tanggalLahirMessage) isValid = false;
    }

    if (nextErrors.nik.dirty && (field ? field === 'nik' : true)) {
      const nikMessage = nikValidator(nik, form);
      nextErrors.nik.error = !!nikMessage;
      nextErrors.nik.message = nikMessage;
      if (nikMessage) isValid = false;
    }

    if (nextErrors.alamat.dirty && (field ? field === 'alamat' : true)) {
      const alamatMessage = alamatValidator(alamat, form);
      nextErrors.alamat.error = !!alamatMessage;
      nextErrors.alamat.message = alamatMessage;
      if (alamatMessage) isValid = false;
    }

    if (nextErrors.email.dirty && (field ? field === 'email' : true)) {
      const emailMessage = emailValidator(email, form);
      nextErrors.email.error = !!emailMessage;
      nextErrors.email.message = emailMessage;
      if (emailMessage) isValid = false;
    }

    if (nextErrors.password.dirty && (field ? field === 'password' : true)) {
      const passwordMessage = passwordValidator(password, form);
      nextErrors.password.error = !!passwordMessage;
      nextErrors.password.message = passwordMessage;
      if (passwordMessage) isValid = false;
    }

    if (nextErrors.jabatan.dirty && (field ? field === 'jabatan' : true)) {
      const jabatanMessage = jabatanValidator(jabatan, form);
      nextErrors.jabatan.error = !!jabatanMessage;
      nextErrors.jabatan.message = jabatanMessage;
      if (jabatanMessage) isValid = false;
    }

    if (nextErrors.role.dirty && (field ? field === 'role' : true)) {
      const roleMessage = roleValidator(role, form);
      nextErrors.role.error = !!roleMessage;
      nextErrors.role.message = roleMessage;
      if (roleMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e) => {
    const field = e.target.name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default useKaryawanFormValidator;
