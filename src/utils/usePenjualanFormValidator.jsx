/* eslint-disable no-shadow */
import { useState } from 'react';

import {
  pelangganIDValidator,
  jenisPembayaranValidator,
  tipeKendaraanValidator,
  tanggalPengirimanValidator,
} from 'utils/PenjualanValidator';

const touchErrors = (errors) => Object.entries(errors).reduce((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

const usePenjualanFormValidator = (form) => {
  const [errors, setErrors] = useState({
    pelangganID: {
      dirty: true,
      error: false,
      message: '',
    },
    tipeKendaraan: {
      dirty: false,
      error: false,
      message: '',
    },
    jenisPembayaran: {
      dirty: true,
      error: false,
      message: '',
    },
    tanggalPengiriman: {
      dirty: true,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    form, field, errors, forceTouchErrors = false,
  }) => {
    let isValid = true;

    // Create a deep copy of the errors
    let nextErrors = JSON.parse(JSON.stringify(errors));

    // Force validate all the fields
    if (forceTouchErrors) {
      nextErrors = touchErrors(errors);
    }

    const {
      pelangganID, jenisPembayaran, tipeKendaraan, tanggalPengiriman,
    } = form;

    if (nextErrors.pelangganID.dirty && (field ? field === 'pelangganID' : true)) {
      const pelangganIDMessage = pelangganIDValidator(pelangganID, form);
      nextErrors.pelangganID.error = !!pelangganIDMessage;
      nextErrors.pelangganID.message = pelangganIDMessage;
      if (pelangganIDMessage) isValid = false;
    }

    if (nextErrors.jenisPembayaran.dirty && (field ? field === 'jenisPembayaran' : true)) {
      const jenisPembayaranMessage = jenisPembayaranValidator(jenisPembayaran, form);
      nextErrors.jenisPembayaran.error = !!jenisPembayaranMessage;
      nextErrors.jenisPembayaran.message = jenisPembayaranMessage;
      if (jenisPembayaranMessage) isValid = false;
    }

    if (nextErrors.tipeKendaraan.dirty && (field ? field === 'tipeKendaraan' : true)) {
      const tipeKendaraanMessage = tipeKendaraanValidator(tipeKendaraan, form);
      nextErrors.tipeKendaraan.error = !!tipeKendaraanMessage;
      nextErrors.tipeKendaraan.message = tipeKendaraanMessage;
      if (tipeKendaraanMessage) isValid = false;
    }

    if (nextErrors.tanggalPengiriman.dirty && (field ? field === 'tanggalPengiriman' : true)) {
      const tanggalPengirimanMessage = tanggalPengirimanValidator(tanggalPengiriman, form);
      nextErrors.tanggalPengiriman.error = !!tanggalPengirimanMessage;
      nextErrors.tanggalPengiriman.message = tanggalPengirimanMessage;
      if (tanggalPengirimanMessage) isValid = false;
    }

    setErrors(nextErrors);

    return {
      isValid,
      errors: nextErrors,
    };
  };

  const onBlurField = (e, name) => {
    let field;
    if (!name) field = e.target.name;
    else field = name;
    const fieldError = errors[field];
    if (fieldError.dirty) return;

    const updatedErrors = {
      ...errors,
      [field]: {
        ...errors[field],
        dirty: true,
      },
    };

    validateForm({ form, field, errors: updatedErrors });
  };

  return {
    validateForm,
    onBlurField,
    errors,
  };
};

export default usePenjualanFormValidator;
