import React from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/barangReducer';
import Barang from 'components/MasterBarang/Barang/Barang';
import KategoriBarang from '../components/MasterBarang/KategoriBarang/KategoriBarang';
import MerekBarang from '../components/MasterBarang/MerekBarang/MerekBarang';

function MasterBarang() {
  const indexTab = useSelector((state) => state.barang.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Barang' },
    { id: '2', tab: 'Kategori Barang' },
    { id: '3', tab: 'Merek Barang' },
  ];

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <Barang />
            </TabPanel>
            <TabPanel>
              <KategoriBarang />
            </TabPanel>
            <TabPanel>
              <MerekBarang />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default MasterBarang;
