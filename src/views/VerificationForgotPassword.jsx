import React, { useEffect, useReducer } from 'react';
import axios from 'axios';
import {
  Box, Heading, Text, VStack, chakra, useToast, FormControl, FormLabel, Input, useDisclosure, HStack,
} from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import useVerificationEmailFormValidator from 'utils/useVerificationEmailFormValidator';
import forgotPasswordVector from 'assets/forgot-password-vector.png';
import LoginForgotPasswordLayout from 'components/layout/LoginForgotPasswordLayout';
import SubmitVerificationEmailButton from 'components/button/SubmitVerificationEmailButton';
import { INITIAL_STATE, VerificationEmailReducer } from 'reducer/VerificationEmailReducer';
import { faCircleXmark } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function VerificationForgotPassword() {
  const IconChakra = chakra(FontAwesomeIcon);
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen } = useDisclosure();

  const [state, dispatch] = useReducer(VerificationEmailReducer, INITIAL_STATE);

  const { errors, validateForm, onBlurField } = useVerificationEmailFormValidator(state);

  useEffect(() => {
    const auth = { accessToken: getCookie('accessToken') };

    if (auth.accessToken) navigate('/');
  }, []);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
    if (errors[field].dirty) {
      validateForm({
        form: nextFormState,
        errors,
        field,
      });
    }
  };

  const fetchVerificationEmail = () => {
    axios({
      method: 'post',
      url: 'forgot-password',
      data: {
        email: state.email,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        dispatch({ type: 'RESET_FORM' });
        toast({
          title: 'Verifikasi telah dikirim ke email',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        navigate('/login');
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      dispatch({ type: 'RESET_FORM' });
      toast({
        title: 'Verifikasi gagal dikirim ke email',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    const { isValid } = validateForm({ form: state, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchVerificationEmail();
  };

  return (
    <LoginForgotPasswordLayout vector={forgotPasswordVector}>
      <VStack className="login-wrapper" w="100%" alignItems="center">
        <Box
          className="heading"
          color="basic.900"
          w={{
            base: '90%',
            xl: '80%',
          }}
          mb={10}
        >
          <Heading
            size={{
              base: 'md',
              lg: 'lg',
            }}
            fontWeight="semibold"
            mb={3}
          >
            Lupa Kata Sandi
          </Heading>
          <Heading
            size={{
              base: 'xl',
              lg: '2xl',
            }}
            fontWeight="bold"
          >
            Verifikasi Akun!
          </Heading>
        </Box>
        <chakra.form
          w={{
            base: '90%',
            xl: '80%',
          }}
          onSubmit={onSubmitForm}
        >
          <FormControl mb={10}>
            <FormLabel color="basic.700" fontWeight="semibold">Email</FormLabel>
            <Input
              type="email"
              name="email"
              onBlur={onBlurField}
              onChange={onUpdateField}
              value={state.email}
              color="basic.500"
              _focus={{
                color: 'basic.800',
              }}
              border="2px solid"
              borderColor="primary.300"
              borderRadius="full"
            />
            {errors.email.dirty && errors.email.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>
                  {errors.email.message}
                </Text>
              </HStack>
            ) : null}
          </FormControl>
          <SubmitVerificationEmailButton isLoading={state.isLoading} isOpen={isOpen} />
        </chakra.form>
      </VStack>
    </LoginForgotPasswordLayout>
  );
}

export default VerificationForgotPassword;
