import React from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/barangReducer';
import StokMasuk from 'components/Distribusi/RincianStok/StokMasuk/StokMasuk';
import StokKeluar from 'components/Distribusi/RincianStok/StokKeluar/StokKeluar';
import HistoryStok from 'components/Distribusi/RincianStok/HistoryStok/HistoryStok';
import OpnameStok from 'components/Distribusi/RincianStok/OpnameStok/OpnameStok';

function RincianStok() {
  const indexTab = useSelector((state) => state.barang.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Stok Masuk' },
    { id: '2', tab: 'Stok Keluar' },
    { id: '3', tab: 'Opname Stok' },
    { id: '4', tab: 'Histori Mutasi Stok' },
  ];

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <StokMasuk />
            </TabPanel>
            <TabPanel>
              <StokKeluar />
            </TabPanel>
            <TabPanel>
              <OpnameStok />
            </TabPanel>
            <TabPanel>
              <HistoryStok />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default RincianStok;
