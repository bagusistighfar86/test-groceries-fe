import React, { useEffect } from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/laporanReducer';
import LaporanPembelian from 'components/Laporan/LaporanPembelian/LaporanPembelian';
import LaporanPenjualan from 'components/Laporan/LaporanPenjualan/LaporanPenjualan';
import { getCookie, setCookie } from 'utils/SetCookies';
import LaporanLabaRugi from 'components/Laporan/LaporanLabaRugi/LaporanLabaRugi';

function Laporan() {
  const auth = { accessToken: getCookie('accessToken') };
  const indexTab = useSelector((state) => state.laporan.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Laporan Pembelian' },
    { id: '2', tab: 'Laporan Penjualan' },
    { id: '3', tab: 'Laporan Laba Rugi' },
  ];

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Laporan');
    }
  }, []);

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <LaporanPembelian />
            </TabPanel>
            <TabPanel>
              <LaporanPenjualan />
            </TabPanel>
            <TabPanel>
              <LaporanLabaRugi />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default Laporan;
