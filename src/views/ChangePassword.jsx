import React, { useEffect, useReducer } from 'react';
import axios from 'axios';
import {
  Box, Heading, Text, VStack, chakra, useToast, FormControl, FormLabel, Input, InputGroup, InputRightElement, Button, HStack,
} from '@chakra-ui/react';
import { useNavigate, useParams } from 'react-router-dom';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import forgotPasswordVector from 'assets/forgot-password-vector.png';
import LoginForgotPasswordLayout from 'components/layout/LoginForgotPasswordLayout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { ChangePasswordReducer, INITIAL_STATE } from 'reducer/ChangePasswordReducer';
import { faCircleXmark } from '@fortawesome/free-regular-svg-icons';
import useChangePasswordFormValidator from 'utils/useChangePasswordFormValidator';

function ChangePassword() {
  const IconChakra = chakra(FontAwesomeIcon);
  const navigate = useNavigate();
  const toast = useToast();
  const { accessToken } = useParams();

  const [state, dispatch] = useReducer(ChangePasswordReducer, INITIAL_STATE);

  const handleShowPassword = () => dispatch({ type: 'SET_SHOW_PASSWORD' });

  const handleShowConfirmPassword = () => dispatch({ type: 'SET_SHOW_CONFIRM_PASSWORD' });

  const { errors, validateForm, onBlurField } = useChangePasswordFormValidator(state.form);

  useEffect(() => {
    const auth = { accessToken: getCookie('accessToken') };

    if (auth.accessToken) navigate('/');
  }, []);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state.form,
      [field]: e.target.value,
    };
    dispatch({
      type: 'SET_FORM',
      payload: nextFormState,
    });
    if (errors[field]) {
      if (errors[field].dirty) {
        validateForm({
          form: nextFormState,
          errors,
          field,
        });
      }
    }
  };

  const fetchChangePassword = () => {
    axios({
      method: 'post',
      url: 'reset-password',
      data: {
        token: accessToken,
        email: state.form.email,
        password: state.form.newPassword,
        password_confirmation: state.form.confirmNewPassword,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        toast({
          title: 'Berhasil mengubah kata sandi',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
        navigate('/login');
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengubah kata sandi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: state.form, errors, forceTouchErrors: true });
    if (!isValid) return;

    fetchChangePassword();
  };

  return (
    <LoginForgotPasswordLayout vector={forgotPasswordVector}>
      <VStack className="login-wrapper" w="100%" alignItems="center">
        <Box
          className="heading"
          color="basic.900"
          w={{
            base: '90%',
            xl: '80%',
          }}
          mb={10}
        >
          <Heading
            size={{
              base: 'md',
              lg: 'lg',
            }}
            fontWeight="semibold"
            mb={3}
          >
            Atur Ulang Kata Sandi
          </Heading>
          <Heading
            size={{
              base: 'xl',
              lg: '2xl',
            }}
            fontWeight="bold"
          >
            Buat Kata Sandi
          </Heading>
        </Box>
        <Box w={{
          base: '90%',
          xl: '80%',
        }}
        >
          <FormControl mb={5} isRequired>
            <FormLabel color="basic.700" fontWeight="semibold">Email</FormLabel>
            <Input
              type="email"
              name="email"
              onBlur={onBlurField}
              onChange={onUpdateField}
              value={state.form.email}
              color="basic.500"
              _focus={{
                color: 'basic.800',
              }}
              border="2px solid"
              borderColor="primary.300"
              borderRadius="full"
            />
            {errors.email.dirty && errors.email.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>
                  {errors.email.message}
                </Text>
              </HStack>
            ) : null}
          </FormControl>
          <FormControl mb={5} isRequired>
            <FormLabel color="basic.700" fontWeight="semibold">Kata sandi baru</FormLabel>
            <InputGroup>
              <Input
                type={state.showNewPassword ? 'text' : 'password'}
                name="newPassword"
                value={state.form.newPassword}
                onChange={onUpdateField}
                onBlur={onBlurField}
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="2px solid"
                borderColor="primary.300"
                borderRadius="full"
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowPassword}>
                  {state.showNewPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                </Button>
              </InputRightElement>
            </InputGroup>
            {errors.newPassword.dirty && errors.newPassword.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>{errors.newPassword.message}</Text>
              </HStack>
            ) : null}
          </FormControl>
          <FormControl mb={10} isRequired>
            <FormLabel color="basic.700" fontWeight="semibold">Konfirmasi kata sandi</FormLabel>
            <InputGroup>
              <Input
                type={state.showConfirmNewPassword ? 'text' : 'password'}
                name="confirmNewPassword"
                value={state.form.confirmNewPassword}
                onChange={onUpdateField}
                onBlur={onBlurField}
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="2px solid"
                borderColor="primary.300"
                borderRadius="full"
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowConfirmPassword}>
                  {state.showConfirmNewPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                </Button>
              </InputRightElement>
            </InputGroup>
            {errors.confirmNewPassword.dirty && errors.confirmNewPassword.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>{errors.confirmNewPassword.message}</Text>
              </HStack>
            ) : null}
          </FormControl>
          <Button
            type="button"
            py={7}
            borderRadius="full"
            colorScheme="primary"
            variant="solid"
            w="100%"
            fontSize="lg"
            boxShadow="rgba(0, 0, 0, 0.15) 0px 12px 21px"
            onClick={onSubmitForm}
          >
            Submit
          </Button>
        </Box>
      </VStack>
    </LoginForgotPasswordLayout>
  );
}

export default ChangePassword;
