import React, { useEffect } from 'react';
import {
  Box,
  Heading,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import { getCookie, setCookie } from 'utils/SetCookies';

function Dashboard() {
  const auth = { accessToken: getCookie('accessToken') };
  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Dashboard');
    }
  }, []);

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Heading>Ini Dashboard</Heading>
      </Box>
    </DashboardLayout>
  );
}

export default Dashboard;
