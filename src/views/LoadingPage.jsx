import React, { useEffect } from 'react';
import {
  Center, Heading, Spinner, VStack,
} from '@chakra-ui/react';
import { getCookie } from 'utils/SetCookies';
import { useNavigate } from 'react-router-dom';

function LoadingPage() {
  const navigate = useNavigate();
  const auth = { accessToken: getCookie('accessToken') };

  useEffect(() => {
    if (!auth.accessToken) navigate('/login');
  }, []);

  return (
    <Center w="100%" h="100vh">
      <VStack spacing={10}>
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
        <Heading>Loading...</Heading>
      </VStack>
    </Center>
  );
}

export default LoadingPage;
