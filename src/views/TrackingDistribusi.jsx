import React from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/trackingDistribusiReducer';
import TrackingSiapDikirim from 'components/Distribusi/TrackingDistribusi/TrackingSiapDikirim/TrackingSiapDikirim';
import TrackingDikirim from 'components/Distribusi/TrackingDistribusi/TrackingDikirim/TrackingDikirim';
import TrackingSelesai from 'components/Distribusi/TrackingDistribusi/TrackingSelesai/TrackingSelesai';

function TrackingDistribusi() {
  const indexTab = useSelector((state) => state.trackingDistribusi.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Siap Dikirim' },
    { id: '2', tab: 'Dikirim' },
    { id: '3', tab: 'Selesai' },
  ];

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <TrackingSiapDikirim />
            </TabPanel>
            <TabPanel>
              <TrackingDikirim />
            </TabPanel>
            <TabPanel>
              <TrackingSelesai />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default TrackingDistribusi;
