import React, { useEffect, useReducer } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Spacer, Table, TableContainer, Tbody, Td, Text, Th,
  Thead, Tr, Flex, Center, useToast, Select, Button,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faLock, faPenToSquare, faStore, faUser, faGear,
} from '@fortawesome/free-solid-svg-icons';
import HapusSelectedDriverButton from 'components/Distribusi/Driver/button/HapusSelectedDriverButton';
import axios from 'axios';
import { deleteAllCookies, getCookie, setCookie } from 'utils/SetCookies';
import HapusDataDriverIcon from 'components/Distribusi/Driver/button/HapusDataDriverIcon';
import { DriverReducer, INITIAL_STATE } from 'reducer/DriverReducer';
import ReactSelect from 'react-select';
import LoadingPage from './LoadingPage';

function ManajemenDriver() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const SelectChakra = chakra(ReactSelect);

  const [state, dispatch] = useReducer(DriverReducer, INITIAL_STATE);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataDriver.every(checkSelect);

  const fetchGetAllDriver = async () => {
    axios({
      method: 'get',
      url: `drivers?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `drivers?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Manajemen Driver');
      fetchGetAllDriver();
    }
  }, [state.showRows]);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataDriver.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const customStyles = {
    option: (styles, {
      isSelected,
    }) => ({
      ...styles,
      backgroundColor: isSelected ? '#29B912' : null,
    }),
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Flex
          flexDir={{
            base: 'column',
            lg: 'row',
          }}
          w="100%"
          mb={7}
          alignItems={{
            base: 'start',
            lg: 'center',
          }}
        >
          <Flex w="100%" flexWrap="wrap" justifyContent="start">

            <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  onChange={onUpdateField}
                  value={state.search}
                  placeholder="Cari berdasarkan nama"
                  color="basic.500"
                  _focus={{
                    color: 'basic.800',
                  }}
                  border="1px solid"
                  borderColor="primary.300"
                />
                <InputRightAddon
                  border="1px solid"
                  borderColor="primary.300"
                >
                  Cari
                </InputRightAddon>
              </InputGroup>
            </FormControl>

            <SelectChakra
              id="status"
              placeholder="Pilih status driver"
              onChange={(e) => dispatch({
                type: 'SET_FILTER_STATUS',
                payload: {
                  value: e.value, label: e.label,
                },
              })}
              value={state.selectedFilterStatus}
              options={[
                { value: 13, label: 'Menunggu' },
                { value: 14, label: 'Mengambil' },
                { value: 15, label: 'Mengantar' },
              ]}
              styles={customStyles}
              w={{ base: '40%', xl: '30%' }}
              border="1px solid"
              borderColor="primary.300"
              borderRadius={5}
              color="basic.500"
              _focus={{
                color: 'basic.800',
              }}
              menuPortalTarget={document.body}
            />

            <Spacer />

            <HStack spacing={noSelected ? 0 : 7} mt={{ base: 5, xl: 0 }}>
              <HapusSelectedDriverButton
                noSelected={noSelected}
                accessToken={auth.accessToken}
                data={state.dataDriver}
                dispatch={dispatch}
                selectedData={state.selectedDataDriver}
                fetchGetAllDriver={fetchGetAllDriver}
              />
              <Link
                as={ReachLink}
                to="/manajemen-driver/tambah-driver"
                bg="primary.500"
                variant="solid"
                fontSize="sm"
                px={5}
                py={2}
                borderRadius={5}
                color="basic.100"
                fontWeight="bold"
                _hover={{
                  textDecoration: 'none',
                  cursor: 'pointer',
                  bg: 'primary.600',
                }}
              >
                Tambah Driver
              </Link>
            </HStack>

          </Flex>
        </Flex>

        <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
          <Table
            size={{
              base: 'sm',
              lg: 'md',
            }}
            fontSize={{
              base: 'sm',
              lg: 'md',
            }}
            variant="unstyled"
          >
            <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
              <Tr>
                <Th>
                  <Center py={3}>
                    <Box w="10px" h="10px" bg="white" />
                  </Center>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faLock} />
                    <Text ms={2}>ID DRIVER </Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faUser} />
                    <Text ms={2}>NAMA</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faStore} />
                    <Text ms={2}>NO TELEPON</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faGear} />
                    <Text ms={2}>Status</Text>
                  </HStack>
                </Th>
                <Th textAlign="center">
                  <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faCircleInfo} />
                    <Text ms={2}>AKSI</Text>
                  </Center>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {state?.dataDriver?.map((item, index) => (
                <Tr key={item.id}>
                  <Td>
                    <Center>
                      <Checkbox
                        disabled={item.status_id !== 13}
                        colorScheme="primary"
                        isChecked={state.selectedDataDriver[index].isSelect}
                        onChange={(e) => handleSelected(e, item.id)}
                      />
                    </Center>
                  </Td>
                  <Td>
                    <Link
                      as={ReachLink}
                      to={`/manajemen-driver/detail-driver/${item.id}`}
                      color="secondary.500"
                      fontWeight="bold"
                      textDecoration="underline"
                      _hover={{ cursor: 'pointer' }}
                    >
                      {item.id}
                    </Link>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.fullname}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.phone}</Text>
                  </Td>
                  <Td w="auto">
                    <Text fontWeight="semibold" color="basic.700">{item?.status?.name}</Text>
                  </Td>
                  <Td textAlign="center">
                    <HStack justifyContent="center" spacing={5}>
                      {item.status_id === 13
                        ? (
                          <Link
                            as={ReachLink}
                            to={`/manajemen-driver/edit-driver/${item.id}`}
                            color="basic.700"
                            _hover={{ color: 'basic.500', cursor: 'pointer' }}
                          >
                            <IconChakra icon={faPenToSquare} />
                          </Link>
                        )
                        : (
                          <IconChakra color="basic.400" icon={faPenToSquare} />
                        )}
                      <HapusDataDriverIcon accessToken={auth.accessToken} id={item.id} data={state.dataDriver} dispatch={dispatch} status={item.status_id} />
                    </HStack>
                  </Td>
                </Tr>
              ))}
              <Tr borderTop="2px solid" borderColor="basic.300">
                <Td colSpan="99">
                  <HStack>
                    <Text>Show rows per page</Text>
                    <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                      {showRows.map((item) => (
                        <option key={item.id} value={item.row}>{item.row}</option>
                      ))}
                    </Select>
                    <Spacer />
                    <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                      <IconChakra
                        icon={faChevronLeft}
                        fontSize="sm"
                      />
                    </Button>
                    <Text>
                      {state?.dataPaginasi.current_page}
                      {' '}
                      of
                      {' '}
                      {state?.dataPaginasi.last_page}
                    </Text>
                    <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                      <IconChakra
                        icon={faChevronRight}
                        fontSize="sm"
                      />
                    </Button>
                  </HStack>
                </Td>
              </Tr>
            </Tbody>
          </Table>
        </TableContainer>
      </Box>

    </DashboardLayout>

  );
}

export default ManajemenDriver;
