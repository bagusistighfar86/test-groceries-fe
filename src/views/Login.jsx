import React, { useEffect, useReducer } from 'react';
import axios from 'axios';
import {
  Box, Heading, Spacer, Text, VStack, chakra, useToast, FormControl, FormLabel, Input, InputGroup, InputRightElement, Button, Link, Checkbox, HStack, useDisclosure,
} from '@chakra-ui/react';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import { deleteAllCookies, getCookie, setCookie } from 'utils/SetCookies';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash, faCircleXmark } from '@fortawesome/free-regular-svg-icons';
import useLoginFormValidator from 'utils/useLoginFormValidator';
import LoginButton from 'components/button/LoginButton';
import LoginBike from 'assets/login-bike.png';
import LoginForgotPasswordLayout from 'components/layout/LoginForgotPasswordLayout';
import { INITIAL_STATE, LoginReducer } from 'reducer/LoginReducer';

function Login() {
  const IconChakra = chakra(FontAwesomeIcon);
  const navigate = useNavigate();
  const toast = useToast();
  const { isOpen } = useDisclosure();

  const [state, dispatch] = useReducer(LoginReducer, INITIAL_STATE);

  const {
    errors, validateForm, onBlurField,
  } = useLoginFormValidator(state);

  useEffect(() => {
    const auth = { accessToken: getCookie('accessToken') };

    if (auth.accessToken) navigate('/');
  }, []);

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
    if (errors[field].dirty) {
      validateForm({
        form: nextFormState,
        errors,
        field,
      });
    }
  };

  const handleChangeCheckbox = (e) => {
    dispatch({
      type: 'SET_INGAT_SAYA',
      payload: { name: e.target.name, value: e.target.value },
    });
  };

  const handleShowPassword = () => dispatch({ type: 'SET_SHOW_PASSWORD' });

  const fetchLogin = () => {
    dispatch({ type: 'BEFORE_FETCH' });
    axios({
      method: 'post',
      url: 'login',
      data: {
        email: state.email,
        password: state.password,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        setCookie('accessToken', res.data.data.access_token);
        setTimeout(isOpen, 3000);
        navigate('/');
        dispatch({ type: 'RESET_FORM' });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      dispatch({ type: 'RESET_FORM' });
      toast({
        title: 'Silahkan cek email dan password.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    const { isValid } = validateForm({ form: { email: state.email, password: state.password }, errors, forceTouchErrors: true });
    if (!isValid) return;
    fetchLogin();
  };

  return (
    <LoginForgotPasswordLayout vector={LoginBike}>
      <VStack className="login-wrapper" w="100%" alignItems="center">
        <Box
          className="heading"
          color="basic.900"
          w={{
            base: '90%',
            lg: '80%',
          }}
          mb={10}
        >
          <Heading
            size={{
              base: 'md',
              lg: 'lg',
            }}
            fontWeight="semibold"
            mb={3}
          >
            Masuk
          </Heading>
          <Heading
            size={{
              base: 'xl',
              lg: '2xl',
            }}
            fontWeight="bold"
          >
            Selamat Datang!
          </Heading>
        </Box>
        <chakra.form
          w={{
            base: '90%',
            lg: '80%',
          }}
          onSubmit={onSubmitForm}
        >
          <FormControl mb={5}>
            <FormLabel color="basic.700" fontWeight="semibold">Email</FormLabel>
            <Input
              type="email"
              name="email"
              onBlur={onBlurField}
              onChange={onUpdateField}
              value={state.email ? state.email : ''}
              color="basic.500"
              _focus={{
                color: 'basic.800',
              }}
              border="2px solid"
              borderColor="primary.300"
              borderRadius="full"
            />
            {errors.email.dirty && errors.email.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>{errors.email.message}</Text>
              </HStack>
            ) : ''}
          </FormControl>
          <FormControl mb={5}>
            <FormLabel color="basic.700" fontWeight="semibold">Kata sandi</FormLabel>
            <InputGroup>
              <Input
                type={state.showPassword ? 'text' : 'password'}
                name="password"
                value={state.password ? state.password : ''}
                onChange={onUpdateField}
                onBlur={onBlurField}
                color="basic.500"
                _focus={{
                  color: 'basic.800',
                }}
                border="2px solid"
                borderColor="primary.300"
                borderRadius="full"
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" bg="none" color="basic.800" _hover={{ bg: 'none', color: '#AFAFAF' }} _active={{ bg: 'none', color: '#AFAFAF' }} onClick={handleShowPassword}>
                  {state.showPassword ? <IconChakra icon={faEyeSlash} /> : <IconChakra icon={faEye} />}
                </Button>
              </InputRightElement>
            </InputGroup>
            {errors.password.dirty && errors.password.error ? (
              <HStack color="red" my={2}>
                <IconChakra icon={faCircleXmark} size="lg" />
                <Text>
                  {errors.password.message}
                </Text>
              </HStack>
            ) : null}
          </FormControl>
          <HStack mb={10}>
            <Checkbox name="ingatSaya" colorScheme="black" isChecked={state.ingatSaya} onChange={handleChangeCheckbox}>
              Ingat saya
            </Checkbox>
            <Spacer />
            <Link
              as={ReachLink}
              to="/lupa-password"
              ms={2}
              color="secondary.500"
              fontWeight="bold"
              _hover={{
                cursor: 'pointer',
              }}
            >
              Lupa kata sandi?
            </Link>
          </HStack>
          <LoginButton isLoading={state.isLoading} isOpen={isOpen} fetchLogin={fetchLogin} />
        </chakra.form>
      </VStack>
    </LoginForgotPasswordLayout>
  );
}

export default Login;
