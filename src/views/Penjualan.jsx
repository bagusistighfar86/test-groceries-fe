import React from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/penjualanReducer';
import PenjualanButuhDiproses from 'components/Penjualan/PenjualanButuhDiproses/PenjualanButuhDiproses';
import PenjualanSelesai from 'components/Penjualan/PenjualanSelesai/PenjualanSelesai';
import PenjualanRetur from 'components/Penjualan/PenjualanRetur/PenjualanRetur';
import PenjualanDibatalkan from 'components/Penjualan/PenjualanDibatalkan/PenjualanDibatalkan';

function Penjualan() {
  const indexTab = useSelector((state) => state.penjualan.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Penjualan Butuh Diproses' },
    { id: '2', tab: 'Penjualan Selesai' },
    { id: '3', tab: 'Retur' },
    { id: '4', tab: 'Penjualan Dibatalkan' },
  ];

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <PenjualanButuhDiproses />
            </TabPanel>
            <TabPanel>
              <PenjualanSelesai />
            </TabPanel>
            <TabPanel>
              <PenjualanRetur />
            </TabPanel>
            <TabPanel>
              <PenjualanDibatalkan />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default Penjualan;
