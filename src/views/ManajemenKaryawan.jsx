import React, { useEffect, useReducer } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Select, Spacer, Table, TableContainer, Tag, Tbody, Td, Text, Th, Thead, Tr, VStack, Flex, Center, useToast, Button,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import { INITIAL_STATE, KaryawanReducer } from 'reducer/KaryawanReducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faGear, faLock, faPenToSquare, faTag, faUser,
} from '@fortawesome/free-solid-svg-icons';
import Avvvatars from 'avvvatars-react';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import Switch from 'react-switch';
import HapusSelectedKaryawanButton from 'components/ManajemenKaryawan/button/HapusSelectedBarangButton';
import axios from 'axios';
import { deleteAllCookies, getCookie, setCookie } from 'utils/SetCookies';
import HapusDataKaryawanIcon from 'components/ManajemenKaryawan/button/HapusDataKaryawanIcon';
import LoadingPage from './LoadingPage';

function ManajemenKaryawan() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const [state, dispatch] = useReducer(KaryawanReducer, INITIAL_STATE);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataKaryawan.every(checkSelect);

  const fetchGetAllKaryawan = () => {
    axios({
      method: 'get',
      url: `employees?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `employees?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Manajemen Karyawan');
      fetchGetAllKaryawan();
    }
  }, [state.showRows]);

  const roles = [
    { id: '1', role: 'Admin' },
    { id: '2', role: 'Gudang' },
    { id: '3', role: 'Sales' },
  ];

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataKaryawan.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  const handleChangeStatus = (id, status) => {
    axios({
      method: 'PATCH',
      url: `employees/active/${id}`,
      headers: {
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const itemPosition = state.dataKaryawan.map((item) => item.id).indexOf(id);
        dispatch({
          type: 'SET_STATUS',
          payload: {
            itemPos: itemPosition,
            data: !status,
          },
        });
        toast({
          title: 'Berhasil mengganti status',
          position: 'top',
          status: 'success',
          isClosable: true,
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengganti status. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
    });
  };

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  const RolesTag = {
    admin: <Tag bg="purple.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>SUPER ADMIN</Tag>,
    gudang: <Tag bg="blue.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>ADMIN GUDANG</Tag>,
    sales: <Tag bg="pink.500" fontSize="xs" fontWeight="bold" color="basic.100" px={4} py={2}>ADMIN SALES</Tag>,
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Flex
          flexDir={{
            base: 'column',
            lg: 'row',
          }}
          w="100%"
          mb={7}
          alignItems={{
            base: 'start',
            lg: 'center',
          }}
        >
          <HStack
            spacing={7}
            w={{
              base: '100%',
              lg: '60%',
            }}
            mb={{
              base: 6,
              lg: 0,
            }}
          >
            <FormControl>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  onChange={onUpdateField}
                  value={state.search}
                  placeholder="Cari berdasarkan nama"
                  color="basic.500"
                  _focus={{
                    color: 'basic.800',
                  }}
                  border="1px solid"
                  borderColor="primary.300"
                />
                <InputRightAddon
                  border="1px solid"
                  borderColor="primary.300"
                >
                  Cari
                </InputRightAddon>
              </InputGroup>
            </FormControl>
            <FormControl>
              <Select name="roleFilter" placeholder="Semua hak akses" variant="outline" value={state.roleFilter} onChange={onUpdateField}>
                {roles.map((item) => (
                  <option key={item.id} value={item.role}>{item.role}</option>
                ))}
              </Select>
            </FormControl>
          </HStack>
          <Spacer />
          <HStack
            alignSelf={{
              base: 'end',
              lg: 'center',
            }}
          >
            <HapusSelectedKaryawanButton
              noSelected={noSelected}
              accessToken={auth.accessToken}
              data={state.dataKaryawan}
              dispatch={dispatch}
              selectedData={state.selectedDataKaryawan}
              fetchGetAllKaryawan={fetchGetAllKaryawan}
            />
            <Link
              as={ReachLink}
              to="/manajemen-karyawan/tambah-karyawan"
              bg="primary.500"
              variant="solid"
              fontSize="sm"
              px={5}
              py={2}
              borderRadius={5}
              color="basic.100"
              fontWeight="bold"
              _hover={{
                textDecoration: 'none',
                cursor: 'pointer',
                bg: 'primary.600',
              }}
            >
              Tambah Karyawan
            </Link>
          </HStack>
        </Flex>
        <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
          <Table
            size={{
              base: 'sm',
              lg: 'md',
            }}
            fontSize={{
              base: 'sm',
              lg: 'md',
            }}
            variant="unstyled"
          >
            <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
              <Tr>
                <Th>
                  <Center py={3}>
                    <Box w="10px" h="10px" bg="white" />
                  </Center>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faLock} />
                    <Text ms={2}>ID KARYAWAN </Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faUser} />
                    <Text ms={2}>NAMA</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faGear} />
                    <Text ms={2}>STATUS</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faTag} />
                    <Text ms={2}>ROLE</Text>
                  </HStack>
                </Th>
                <Th textAlign="center">
                  <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faCircleInfo} />
                    <Text ms={2}>AKSI</Text>
                  </Center>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {state.dataKaryawan.map((item, index) => (
                <Tr key={item.id}>
                  <Td>
                    <Center>
                      <Checkbox
                        colorScheme="primary"
                        isChecked={state.selectedDataKaryawan[index].isSelect}
                        onChange={(e) => handleSelected(e, item.id)}
                      />
                    </Center>
                  </Td>
                  <Td>
                    <Link
                      as={ReachLink}
                      to={`/manajemen-karyawan/detail-karyawan/${item.id}`}
                      color="secondary.500"
                      fontWeight="bold"
                      textDecoration="underline"
                      _hover={{ cursor: 'pointer' }}
                    >
                      {item.id}
                    </Link>
                  </Td>
                  <Td>
                    <HStack>
                      <Box
                        display={{
                          base: 'none',
                          lg: 'flex',
                        }}
                      >
                        <Avvvatars value="Username" size={45} />
                      </Box>
                      <VStack spacing={0} alignItems="start">
                        <Text fontWeight="semibold" color="basic.700">{item.name}</Text>
                        <Text color="basic.500">{item.email}</Text>
                      </VStack>
                    </HStack>
                  </Td>
                  <Td>
                    <Switch
                      onChange={() => handleChangeStatus(item.id, item.is_active)}
                      checked={item.is_active}
                      uncheckedIcon={(
                        <Center h="100%" w={120} position="relative" right={16}>
                          <Text color="basic.100">Tidak Aktif</Text>
                        </Center>
                      )}
                      checkedIcon={(
                        <Center h="100%" w={120}>
                          <Text color="basic.100">Aktif</Text>
                        </Center>
                      )}
                      handleDiameter={20}
                      offColor="#CDCDCD"
                      onColor="#29B912"
                      height={30}
                      width={120}
                      className="react-switch"
                    />
                  </Td>
                  <Td>
                    {item.user.role_id === 1 && RolesTag.admin}
                    {item.user.role_id === 2 && RolesTag.gudang}
                    {item.user.role_id === 3 && RolesTag.sales}
                  </Td>
                  <Td textAlign="center">
                    <HStack justifyContent="center" spacing={5}>
                      <Link
                        as={ReachLink}
                        to={`/manajemen-karyawan/edit-karyawan/${item.id}`}
                        color="basic.700"
                        _hover={{ color: 'basic.500', cursor: 'pointer' }}
                      >
                        <IconChakra icon={faPenToSquare} />
                      </Link>
                      <HapusDataKaryawanIcon accessToken={auth.accessToken} id={item.id} data={state.dataKaryawan} dispatch={dispatch} />
                    </HStack>
                  </Td>
                </Tr>
              ))}
              <Tr borderTop="2px solid" borderColor="basic.300">
                <Td colSpan="6">
                  <HStack>
                    <Text>Show rows per page</Text>
                    <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                      {showRows.map((item) => (
                        <option key={item.id} value={item.row}>{item.row}</option>
                      ))}
                    </Select>
                    <Spacer />
                    <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                      <IconChakra
                        icon={faChevronLeft}
                        fontSize="sm"
                      />
                    </Button>
                    <Text>
                      {state?.dataPaginasi.current_page}
                      {' '}
                      of
                      {' '}
                      {state?.dataPaginasi.last_page}
                    </Text>
                    <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                      <IconChakra
                        icon={faChevronRight}
                        fontSize="sm"
                      />
                    </Button>
                  </HStack>
                </Td>
              </Tr>
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
    </DashboardLayout>
  );
}

export default ManajemenKaryawan;
