import React, { useEffect, useReducer } from 'react';
import {
  Box, Link, chakra, Checkbox, FormControl, HStack, Input, InputGroup,
  InputRightAddon, Select, Spacer, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr, Flex, Center, useToast, Button,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useLocation, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faCircleInfo, faClock, faLock, faMapPin, faPenToSquare, faSort, faStore, faUser,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, PelangganReducer } from 'reducer/PelangganReducer';
import HapusSelectedPelangganButton from 'components/Pelanggan/button/HapusSelectedPelangganButton';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';
import HapusDataPelangganIcon from 'components/Pelanggan/button/HapusDataPelangganIcon';
import { setPrevPath } from 'redux/reducer/navigationReducer';
import { useDispatch } from 'react-redux';
import LoadingPage from './LoadingPage';

function Pelanggan() {
  const location = useLocation();
  const dispatchRedux = useDispatch();
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);

  const [state, dispatch] = useReducer(PelangganReducer, INITIAL_STATE);

  const auth = { accessToken: getCookie('accessToken') };

  function checkSelect(item) {
    if (item.isSelect === false) return true;
    return false;
  }
  const noSelected = state.selectedDataPelanggan.every(checkSelect);

  const fetchGetAllPelanggan = () => {
    axios({
      method: 'get',
      url: `customers?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  const fetchGetPaginationControl = (page) => {
    axios({
      method: 'get',
      url: `customers?limit=${state.showRows}&page=${page}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            selectedData: dataBaru.map((item) => ({ id: item.id, isSelect: false })),
            isLoading: false,
          },
        });
        dispatch({
          type: 'SET_DATA',
          payload: {
            name: 'dataPaginasi',
            data: {
              current_page: res.data.data.current_page,
              prev_page_url: res.data.data.prev_page_url,
              next_page_url: res.data.data.next_page_url,
              last_page: res.data.data.last_page,
            },
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data.Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(0);
    });
  };

  useEffect(() => {
    fetchGetAllPelanggan();
  }, []);

  const handleSelected = (e, itemId) => {
    const newState = state.selectedDataPelanggan.map((item) => {
      if (item.id === itemId) {
        return { ...item, isSelect: e.target.checked };
      }
      return item;
    });

    dispatch({ type: 'SET_SELECTED', payload: newState });
  };

  useEffect(() => {
    if (auth.accessToken) {
      fetchGetAllPelanggan();
    }
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Flex
          flexDir={{
            base: 'column',
            lg: 'row',
          }}
          w="100%"
          mb={7}
          alignItems={{
            base: 'start',
            lg: 'center',
          }}
        >
          <HStack spacing={7} w="100%">
            <FormControl w={{ base: '40%', xl: '30%' }}>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  onChange={onUpdateField}
                  value={state.search}
                  placeholder="Cari berdasarkan nama"
                  color="basic.500"
                  _focus={{
                    color: 'basic.800',
                  }}
                  border="1px solid"
                  borderColor="primary.300"
                />
                <InputRightAddon
                  border="1px solid"
                  borderColor="primary.300"
                >
                  Cari
                </InputRightAddon>
              </InputGroup>
            </FormControl>
            <IconChakra icon={faSort} className="fa-xl" />

            <Spacer />

            <HapusSelectedPelangganButton
              noSelected={noSelected}
              accessToken={auth.accessToken}
              data={state.dataPelanggan}
              dispatch={dispatch}
              selectedData={state.selectedDataPelanggan}
              fetchGetAllPelanggan={fetchGetAllPelanggan}
            />
            <Link
              as={ReachLink}
              to="/pelanggan/tambah-pelanggan"
              onClick={() => dispatchRedux(setPrevPath(location.pathname))}
              bg="primary.500"
              variant="solid"
              fontSize="sm"
              px={5}
              py={2}
              borderRadius={5}
              color="basic.100"
              fontWeight="bold"
              _hover={{
                textDecoration: 'none',
                cursor: 'pointer',
                bg: 'primary.600',
              }}
            >
              Tambah Pelanggan
            </Link>
          </HStack>
        </Flex>
        <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
          <Table
            size={{
              base: 'sm',
              lg: 'md',
            }}
            fontSize={{
              base: 'sm',
              lg: 'md',
            }}
            variant="unstyled"
          >
            <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
              <Tr>
                <Th>
                  <Center py={3}>
                    <Box w="10px" h="10px" bg="white" />
                  </Center>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faLock} />
                    <Text ms={2}>ID SUPPLIER </Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faUser} />
                    <Text ms={2}>NAMA</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faStore} />
                    <Text ms={2}>TOKO</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faMapPin} />
                    <Text display={{ base: 'none', lg: 'block' }}>BATAS KREDIT</Text>
                    <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                      <Text>BATAS</Text>
                      <Text>KREDIT</Text>
                    </Flex>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faClock} />
                    <Text display={{ base: 'none', lg: 'block' }}>JATUH TEMPO</Text>
                    <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                      <Text>JATUH</Text>
                      <Text>TEMPO</Text>
                    </Flex>
                  </HStack>
                </Th>
                <Th textAlign="center">
                  <Center py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faCircleInfo} />
                    <Text ms={2}>AKSI</Text>
                  </Center>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {state.dataPelanggan.map((item, index) => (
                <Tr key={item.id}>
                  <Td>
                    <Center>
                      <Checkbox
                        colorScheme="primary"
                        isChecked={state.selectedDataPelanggan[index].isSelect}
                        onChange={(e) => handleSelected(e, item.id)}
                      />
                    </Center>
                  </Td>
                  <Td>
                    <Link
                      as={ReachLink}
                      to={`/pelanggan/detail-pelanggan/${item.id}`}
                      color="secondary.500"
                      fontWeight="bold"
                      textDecoration="underline"
                      _hover={{ cursor: 'pointer' }}
                    >
                      {item.id}
                    </Link>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.fullname}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">
                      {item.store_name}
                    </Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.max_credit)}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">
                      {item.set_due_date}
                      {' '}
                      hari
                    </Text>
                  </Td>
                  <Td textAlign="center">
                    <HStack justifyContent="center" spacing={5}>
                      <Link
                        as={ReachLink}
                        to={`/pelanggan/edit-pelanggan/${item.id}`}
                        color="basic.700"
                        _hover={{ color: 'basic.500', cursor: 'pointer' }}
                      >
                        <IconChakra icon={faPenToSquare} />
                      </Link>
                      <HapusDataPelangganIcon accessToken={auth.accessToken} id={item.id} data={state.dataPelanggan} dispatch={dispatch} />
                    </HStack>
                  </Td>
                </Tr>
              ))}
              <Tr borderTop="2px solid" borderColor="basic.300">
                <Td colSpan="99">
                  <HStack>
                    <Text>Show rows per page</Text>
                    <Select name="showRows" placeholder="0" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                      {showRows.map((item) => (
                        <option key={item.id} value={item.row}>{item.row}</option>
                      ))}
                    </Select>
                    <Spacer />
                    <Button disabled={!state.dataPaginasi.prev_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page - 1)}>
                      <IconChakra
                        icon={faChevronLeft}
                        fontSize="sm"
                      />
                    </Button>
                    <Text>
                      {state?.dataPaginasi.current_page}
                      {' '}
                      of
                      {' '}
                      {state?.dataPaginasi.last_page}
                    </Text>
                    <Button disabled={!state.dataPaginasi.next_page_url} bg="none" onClick={() => fetchGetPaginationControl(state.dataPaginasi.current_page + 1)}>
                      <IconChakra
                        icon={faChevronRight}
                        fontSize="sm"
                      />
                    </Button>
                  </HStack>
                </Td>
              </Tr>
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
    </DashboardLayout>
  );
}

export default Pelanggan;
