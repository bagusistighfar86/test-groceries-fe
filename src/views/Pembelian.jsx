import React from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/pembelianReducer';
import PembelianButuhDiproses from 'components/Pembelian/PembelianButuhDiproses/PembelianButuhDiproses';
import PembelianSelesai from 'components/Pembelian/PembelianSelesai/PembelianSelesai';
import PembelianDibatalkan from 'components/Pembelian/PembelianDibatalkan/PembelianDibatalkan';
import PembelianRetur from 'components/Pembelian/PembelianRetur/PembelianRetur';

function Pembelian() {
  const indexTab = useSelector((state) => state.pembelian.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Pembelian Butuh Diproses' },
    { id: '2', tab: 'Pembelian Selesai' },
    { id: '3', tab: 'Retur' },
    { id: '4', tab: 'Pembelian Dibatalkan' },
  ];

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <PembelianButuhDiproses />
            </TabPanel>
            <TabPanel>
              <PembelianSelesai />
            </TabPanel>
            <TabPanel>
              <PembelianRetur />
            </TabPanel>
            <TabPanel>
              <PembelianDibatalkan />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default Pembelian;
