import React, { useEffect } from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/supplierReducer';
import { getCookie, setCookie } from 'utils/SetCookies';
import Supplier from 'components/ManajemenSupplier/Supplier/Supplier';
import Persetujuan from 'components/ManajemenSupplier/Persetujuan/Persetujuan';

function ManajemenSupplier() {
  const auth = { accessToken: getCookie('accessToken') };
  const indexTab = useSelector((state) => state.supplier.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Supplier' },
    { id: '2', tab: 'Persetujuan' },
  ];

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Laporan');
    }
  }, []);

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <Supplier />
            </TabPanel>
            <TabPanel>
              <Persetujuan />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default ManajemenSupplier;
