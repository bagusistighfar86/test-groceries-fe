import React, { useEffect } from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/jurnalReducer';
import { getCookie, setCookie } from 'utils/SetCookies';
import JurnalPending from 'components/Jurnal/JurnalPending';
import JurnalPosting from 'components/Jurnal/JurnalPosting';

function Jurnal() {
  const auth = { accessToken: getCookie('accessToken') };
  const indexTab = useSelector((state) => state.jurnal.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Pending' },
    { id: '2', tab: 'Posted' },
  ];

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Keuangan');
    }
  }, []);

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <JurnalPending />
            </TabPanel>
            <TabPanel>
              <JurnalPosting />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default Jurnal;
