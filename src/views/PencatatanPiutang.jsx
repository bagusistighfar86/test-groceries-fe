import React, { useEffect, useReducer, useState } from 'react';
import {
  Link, chakra, FormControl, HStack, Input, InputGroup, Tbody, Td, Text,
  InputRightAddon, Select, Spacer, Table, TableContainer, Th, Thead, Tr, Flex, useToast, Box,
} from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ReachLink, useNavigate } from 'react-router-dom';
import {
  faChevronLeft, faChevronRight, faLock, faGear, faList, faCalendar, faSort, faUser, faCoins, faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';
import { INITIAL_STATE, PencatatanPiutangReducer } from 'reducer/PencatatanPiutangReducer';
import axios from 'axios';
import { deleteAllCookies, getCookie } from 'utils/SetCookies';
import LoadingPage from 'views/LoadingPage';
import { RangeDatepicker } from 'chakra-dayzed-datepicker';
import DashboardLayout from 'components/layout/DashboardLayout';
import ConvertMoneyIDR from 'utils/ConvertMoneyIDR';

function PencatatanPiutang() {
  const navigate = useNavigate();
  const toast = useToast();
  const IconChakra = chakra(FontAwesomeIcon);
  const RangeDate = chakra(RangeDatepicker);

  const [state, dispatch] = useReducer(PencatatanPiutangReducer, INITIAL_STATE);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const auth = { accessToken: getCookie('accessToken') };

  const fetchGetAllPencatatanPiutang = () => {
    axios({
      method: 'get',
      url: `credits?limit=${state.showRows}`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        authorization: `Bearer ${auth.accessToken}`,
      },
    }).then((res) => {
      if (res.status >= 200 && res.status < 300) {
        const dataBaru = res.data.data.data;
        dispatch({
          type: 'ON_FETCH',
          payload: {
            data: dataBaru,
            isLoading: false,
          },
        });
      }
    }).catch(async (err) => {
      if (err.response.data.message.toLowerCase() === 'Unauthenticated.' || err.response.status === 401) {
        toast({
          title: 'Token expired. Silahkan login kembali',
          position: 'top',
          status: 'error',
          isClosable: true,
        });
        await deleteAllCookies();
        navigate(0);
        return;
      }
      toast({
        title: 'Gagal mengambil data. Silahkan coba lagi',
        position: 'top',
        status: 'error',
        isClosable: true,
      });
      navigate(-1);
    });
  };

  useEffect(() => {
    fetchGetAllPencatatanPiutang();
  }, []);

  useEffect(() => {
    fetchGetAllPencatatanPiutang();
  }, [state.showRows]);

  const showRows = [
    { id: '1', row: 10 },
    { id: '2', row: 30 },
    { id: '3', row: 50 },
    { id: '4', row: 100 },
  ];

  const onUpdateField = (e) => {
    const field = e.target.name;
    const nextFormState = {
      ...state,
      [field]: e.target.value,
    };
    dispatch({
      type: 'CHANGE_INPUT',
      payload: nextFormState,
    });
  };

  if (state.isLoading) {
    return <LoadingPage />;
  }
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Flex
          flexDir={{
            base: 'column',
            lg: 'row',
          }}
          w="100%"
          mb={7}
          alignItems={{
            base: 'start',
            lg: 'center',
          }}
        >
          <Flex w="100%" flexWrap="wrap" justifyContent="start">
            <FormControl w={{ base: '40%', xl: '30%' }} me={7}>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  onChange={onUpdateField}
                  value={state.search}
                  placeholder="Cari"
                  color="basic.500"
                  _focus={{
                    color: 'basic.800',
                  }}
                  border="1px solid"
                  borderColor="primary.300"
                />
                <InputRightAddon
                  border="1px solid"
                  borderColor="primary.300"
                >
                  Cari
                </InputRightAddon>
              </InputGroup>
            </FormControl>

            <RangeDate
              propsConfigs={{
                dateNavBtnProps: {
                  colorScheme: 'primary',
                  variant: 'solid',
                },
                dayOfMonthBtnProps: {
                  defaultBtnProps: {
                    borderColor: 'primary.500',
                    _hover: {
                      background: 'primary.500',
                    },
                  },
                  isInRangeBtnProps: {
                    background: 'primary.400',
                    color: 'basic.100',
                  },
                  selectedBtnProps: {
                    background: 'primary.500',
                    color: 'basic.100',
                  },
                  todayBtnProps: {
                    background: 'blue.500',
                    color: 'basic.100',
                  },
                },
                inputProps: {
                  border: '1px solid',
                  borderColor: 'primary.300',
                  borderRadius: 5,
                  color: 'basic.500',
                  w: { base: '40%', xl: '30%' },
                  me: 7,
                },
              }}
              selectedDates={selectedDates}
              onDateChange={setSelectedDates}
              w={{ base: '40%', xl: '30%' }}
              border="1px solid"
              borderColor="primary.300"
              borderRadius={5}
              color="basic.500"
              _focus={{
                color: 'basic.800',
              }}
            />

            <IconChakra icon={faSort} className="fa-xl" />
          </Flex>
        </Flex>
        <TableContainer border="2px solid" borderColor="basic.300" borderRadius={10}>
          <Table
            size={{
              base: 'sm',
              lg: 'md',
            }}
            fontSize={{
              base: 'sm',
              lg: 'md',
            }}
            variant="unstyled"
          >
            <Thead bg="basic.200" color="basic.700" borderBottom="2px solid" borderColor="basic.300">
              <Tr>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faLock} />
                    <Text>ID HUTANG</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faUser} />
                    <Text ms={2}>SUPPLIER</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faCoins} />
                    <Text ms={2}>NOMINAL</Text>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faList} />
                    <Text display={{ base: 'none', lg: 'block' }}>NOMOR REFERENSI</Text>
                    <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                      <Text>NOMOR </Text>
                      <Text>REFERENSI</Text>
                    </Flex>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faCalendar} />
                    <Text display={{ base: 'none', lg: 'block' }}>JATUH TEMPO</Text>
                    <Flex ms={2} flexDir="column" display={{ base: 'flex', lg: 'none' }}>
                      <Text>JATUH</Text>
                      <Text>TEMPO</Text>
                    </Flex>
                  </HStack>
                </Th>
                <Th>
                  <HStack justifyContent="start" py={3} fontSize={{ base: 'xs', lg: 'sm' }}>
                    <IconChakra icon={faGear} />
                    <Text ms={2}>STATUS</Text>
                  </HStack>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {state.dataPiutang.map((item) => (
                <Tr key={item.id}>
                  <Td>
                    <Link
                      as={ReachLink}
                      to={`/pencatatan-piutang/detail-piutang/${item.id}`}
                      color="secondary.500"
                      fontWeight="bold"
                      textDecoration="underline"
                      _hover={{ cursor: 'pointer' }}
                    >
                      {item.id}
                    </Link>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item?.customer?.fullname}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.sale_id}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{ConvertMoneyIDR(item.total)}</Text>
                  </Td>
                  <Td>
                    <Text fontWeight="semibold" color="basic.700">{item.due_date}</Text>
                  </Td>
                  <Td display="flex" justifyContent="center">
                    <IconChakra icon={faCheckCircle} color={item.is_paid ? 'success.500' : 'basic.200'} size="2xl" />
                  </Td>
                </Tr>
              ))}
              <Tr borderTop="2px solid" borderColor="basic.300">
                <Td colSpan="9">
                  <HStack>
                    <Text>Show rows per page</Text>
                    <Select name="showRows" variant="outline" value={state.showRows} onChange={onUpdateField} w="80px">
                      {showRows.map((item) => (
                        <option key={item.id} value={item.row}>{item.row}</option>
                      ))}
                    </Select>
                    <Spacer />
                    <IconChakra icon={faChevronLeft} fontSize="sm" _hover={{ color: 'basic.500', cursor: 'pointer' }} />
                    <Text>1 of 1</Text>
                    <IconChakra icon={faChevronRight} fontSize="sm" _hover={{ color: 'basic.500', cursor: 'pointer' }} />
                  </HStack>
                </Td>
              </Tr>
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
    </DashboardLayout>
  );
}

export default PencatatanPiutang;
