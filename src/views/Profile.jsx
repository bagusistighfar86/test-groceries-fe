import React from 'react';
import {
  Box, Flex, HStack, Link, Text, VStack,
} from '@chakra-ui/react';
import DashboardLayout from 'components/layout/DashboardLayout';
import Avvvatars from 'avvvatars-react';
import { Link as ReachLink } from 'react-router-dom';

function Profile() {
  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <VStack alignItems="start" px={8}>
          <HStack w="100%" borderBottom="1px solid" borderColor="basic.200" py={10}>
            <Box flexBasis="20%">
              <Text fontWeight="bold" fontSize="xl">Foto Profil</Text>
            </Box>
            <Box w="80%">
              <Avvvatars value="Username" size={100} />
            </Box>
          </HStack>
          <Flex
            flexDir={{
              base: 'column',
              lg: 'row',
            }}
            w="100%"
            borderBottom="1px solid"
            borderColor="basic.200"
            py={10}
          >
            <Box flexBasis="20%" mb={5}>
              <Text fontWeight="bold" fontSize="xl">Data Diri</Text>
            </Box>
            <Box flexBasis="80%">
              <VStack alignItems="start" w="100%" wordBreak="break-word" spacing={12}>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Nama Lengkap</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>Username</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Nomor Telepon</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>081234567890</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Tanggal Lahir</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>08/08/1999</Text>
                    </Box>
                  </HStack>
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold" w="150px">Role</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>Admin</Text>
                    </Box>
                  </HStack>
                </HStack>
                <HStack w="100%" alignItems="start">
                  <HStack flexBasis="50%" alignItems="start">
                    <Box w={{
                      base: '40%',
                      lg: '35%',
                    }}
                    >
                      <Text fontWeight="bold">Email</Text>
                    </Box>
                    <Box w={{
                      base: '50%',
                      lg: '60%',
                    }}
                    >
                      <Text>username@gmail.com</Text>
                    </Box>
                  </HStack>
                </HStack>
              </VStack>
              <HStack spacing={5} mt={12}>
                <Link
                  as={ReachLink}
                  to="/profile/change-password"
                  border="1px solid"
                  borderColor="primary.500"
                  variant="outline"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.700"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                  }}
                >
                  Ubah Kata Sandi
                </Link>
                <Link
                  as={ReachLink}
                  to="/profile/edit-profile"
                  bg="primary.500"
                  variant="solid"
                  fontSize="sm"
                  px={5}
                  py={2}
                  borderRadius={5}
                  color="basic.100"
                  fontWeight="bold"
                  _hover={{
                    textDecoration: 'none',
                    cursor: 'pointer',
                    bg: 'primary.600',
                  }}
                >
                  Edit Profil
                </Link>
              </HStack>
            </Box>
          </Flex>
        </VStack>
      </Box>
    </DashboardLayout>
  );
}

export default Profile;
