import React, { useEffect } from 'react';
import {
  Box, Tabs, TabList, Tab, TabPanels, TabPanel,
} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import DashboardLayout from 'components/layout/DashboardLayout';
import { setIndexTab } from 'redux/reducer/keuanganReducer';
import KasMasuk from 'components/Keuangan/KasMasuk/KasMasuk';
import KasKeluar from 'components/Keuangan/KasKeluar/KasKeluar';
import { getCookie, setCookie } from 'utils/SetCookies';

function Keuangan() {
  const auth = { accessToken: getCookie('accessToken') };
  const indexTab = useSelector((state) => state.keuangan.indexTab);
  const dispatchRedux = useDispatch();
  const ListTab = [
    { id: '1', tab: 'Kas Masuk' },
    { id: '2', tab: 'Kas Keluar' },
  ];

  useEffect(() => {
    if (auth.accessToken) {
      setCookie('navName', 'Keuangan');
    }
  }, []);

  return (
    <DashboardLayout>
      <Box w="100%" h="100vh" overflowY="scroll" pt={5} px={4}>
        <Tabs isLazy isFitted defaultIndex={indexTab} onChange={(index) => dispatchRedux(setIndexTab(index))}>
          <TabList>
            {ListTab.map((item) => (
              <Tab
                key={item.id}
                _selected={{
                  color: 'primary.500',
                  borderColor: 'primary.500',
                }}
              >
                {item.tab}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            <TabPanel>
              <KasMasuk />
            </TabPanel>
            <TabPanel>
              <KasKeluar />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </DashboardLayout>
  );
}

export default Keuangan;
