import getNowDate from 'utils/getNowDate';

export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  form: {
    pelangganID: '',
    tujuanPengiriman: '',
    tanggalJual: getNowDate(),
    jenisPembayaran: '',
    tipeKendaraan: '',
    tanggalPengiriman: '',
    catatan: '',
    total: 0,
  },
  formAddBarang: {
    barangID: '',
    kuantitas: '',
    subTotal: 0,
  },
  formEditBarang: {
    barangID: '',
    kuantitas: '',
    subTotal: 0,
  },
  selectedStatus: [{
    value: '', label: '',
  }],
  selectedPelanggan: {
    value: 'Pilih pelanggan', label: 'Pilih pelanggan', alamatTujuan: '',
  },
  selectedTipeKendaraan: {
    value: 'Pilih tipe kendaraan', label: 'Pilih tipe kendaraan',
  },
  selectedJenisPembayaran: {
    value: 'Pilih jenis pembayaran', label: 'Pilih jenis pembayaran',
  },
  selectedBarang: {
    value: 'Pilih barang',
    label: 'Pilih barang',
    id: '',
    kodeBarang: '',
    gudang: '',
    hargaJual: '',
    stok: '',
    limitStok: 0,
  },
  dataPenjualan: [],
  selectedDataPenjualan: [],
  detailPenjualan: {},
  sale_details: [],
  dataPelanggan: [],
  dataTipeKendaraan: [],
  dataBarang: [],
  selectedDataBarang: [],
  dataStatus: [
    { value: 'butuh-diproses', label: 'Butuh Diproses' },
    { value: 'siap-dikirim', label: 'Siap Dikirim' },
    { value: 'selesai', label: 'Selesai' },
    { value: 'retur', label: 'Retur' },
    { value: 'batal', label: 'Batal' },
  ],
};

export const PenjualanDibatalkanReducer = (state, action) => {
  function setStatus() {
    const newArray = [...state.selectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  function addQuantityDataPenjualan() {
    const newArray = [...state.dataPenjualan];
    newArray[action.payload.itemPos].kuantitas = parseInt(newArray[action.payload.itemPos].kuantitas, 10) + parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(newArray[action.payload.itemPos].subTotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }

  function addQuantitySaleDetails() {
    const newArray = [...state.sale_details];
    newArray[action.payload.itemPos].qty = parseInt(newArray[action.payload.itemPos].qty, 10) + parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(newArray[action.payload.itemPos].subtotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }
  function setQuantityDataPenjualan() {
    const newArray = [...state.dataPenjualan];
    newArray[action.payload.itemPos].kuantitas = parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(action.payload.data.kuantitas, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  function setQuantitySaleDetails() {
    const newArray = [...state.sale_details];
    newArray[action.payload.itemPos].qty = parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(action.payload.data.kuantitas, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataPenjualan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPenjualan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataPenjualan: action.payload.data,
        sale_details: action.payload.sale_details,
        selectedDataPenjualan: action.payload.selectedData,
      };
    case 'ADD_QUANTITY': {
      return {
        ...state,
        dataPenjualan: addQuantityDataPenjualan(),
        sale_details: addQuantitySaleDetails(),
      };
    }
    case 'SET_QUANTITY': {
      return {
        ...state,
        dataPenjualan: setQuantityDataPenjualan(),
        sale_details: setQuantitySaleDetails(),
      };
    }
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataPenjualan: action.payload,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        [action.payload.dataName]: setStatus(),
      };
    }
    case 'RESET_STATE_ADD_BARANG_FORM':
      return {
        ...state,
        formAddBarang: INITIAL_STATE.formAddBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE_EDIT_BARANG_FORM':
      return {
        ...state,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE':
      return {
        ...state,
        form: INITIAL_STATE.form,
        formAddBarang: INITIAL_STATE.formAddBarang,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
        selectedPelanggan: INITIAL_STATE.selectedPelanggan,
        selectedTipeKendaraan: INITIAL_STATE.selectedTipeKendaraan,
        selectedJenisPembayaran: INITIAL_STATE.selectedJenisPembayaran,
        dataPenjualan: INITIAL_STATE.dataPenjualan,
        selectedDataPenjualan: INITIAL_STATE.selectedDataPenjualan,
        sale_details: INITIAL_STATE.sale_details,
        selectedDataBarang: INITIAL_STATE.selectedDataBarang,
      };
    default:
      return state;
  }
};
