export const INITIAL_STATE = {
  isLoading: true,
  showRows: 10,
  selectedSortOptions: {
    value: 'Pilih status', label: 'Pilih status',
  },
  dataLaporan: [],
  detailLaporan: {},
};

export const LaporanLabaRugiReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'ON_FETCH':
      return {
        ...state,
        dataLaporan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailLaporan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
