export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  selectedSortOptions: {
    value: 'Pilih status', label: 'Pilih status',
  },
  selectedStatus: {
    value: '', label: '', itemID: '', index: '',
  },
  dataSelectedStatus: [{
    value: '', label: '',
  }],
  dataStokKeluar: [],
  detailStokKeluar: {},
  dataStatus: [
    { value: 0, label: 'Pending' },
    { value: 1, label: 'Posting' },
  ],
  dataPaginasi: {},
};

export const StokKeluarReducer = (state, action) => {
  function setStatus() {
    const newArray = [...state.dataSelectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'ON_FETCH':
      return {
        ...state,
        dataStokKeluar: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailStokKeluar: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        [action.payload.dataName]: setStatus(),
      };
    }
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
