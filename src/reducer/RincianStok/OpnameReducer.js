import getDateTimeNow from 'utils/getDateTimeNow';

export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  roleFilter: '',
  showRows: 10,
  selectedFilterStatus: '',
  form: {
    gudangID: '',
    tanggalJamPengecekan: getDateTimeNow(),
    catatan: '',
  },
  formAddBarang: {
    barangID: '',
    stokAkhir: '',
  },
  formEditBarang: {
    barangID: '',
    stokAkhir: '',
  },
  selectedBarang: {
    value: 'Pilih barang',
    label: 'Pilih barang',
    id: '',
    kodeBarang: '',
    stokAwal: '',
  },
  selectedGudang: {
    value: 'Pilih gudang', label: 'Pilih gudang',
  },
  dataOpname: [],
  detailOpname: {},
  selectedDataOpname: [],
  opname_stok_details: [],
  dataBarang: [],
  selectedDataBarang: [],
  dataGudang: [],
  selectedDataGudang: [],
  dataPaginasi: {},
};

export const OpnameReducer = (state, action) => {
  function setQuantityDataOpname() {
    const newArray = [...state.dataOpname];
    newArray[action.payload.itemPos].stokAkhir = parseInt(action.payload.data.stokAkhir, 10);
    return newArray;
  }

  function setQuantityOpnameDetails() {
    const newArray = [...state.opname_stok_details];
    newArray[action.payload.itemPos].stokAkhir = parseInt(action.payload.data.stokAkhir, 10);
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'SET_FILTER_STATUS':
      return {
        ...state,
        selectedFilterStatus: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataOpname: action.payload.data,
        selectedDataOpname: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailOpname: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataOpname: action.payload.data,
        opname_stok_details: action.payload.opname_stok_details,
        selectedDataOpname: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_TIME':
      return {
        ...state,
        form: {
          ...state.form,
          tanggalJamPengecekan: getDateTimeNow(),
        },
      };
    case 'SET_QUANTITY': {
      return {
        ...state,
        dataOpname: setQuantityDataOpname(),
        opname_stok_details: setQuantityOpnameDetails(),
      };
    }
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataOpname: action.payload,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'RESET_STATE_ADD_BARANG_FORM':
      return {
        ...state,
        formAddBarang: INITIAL_STATE.formAddBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE_EDIT_BARANG_FORM':
      return {
        ...state,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE':
      return {
        ...state,
        form: {
          ...INITIAL_STATE.form,
          tanggalJamPengecekan: getDateTimeNow(),
        },
        formAddBarang: INITIAL_STATE.formAddBarang,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
        selectedGudang: INITIAL_STATE.selectedGudang,
        dataOpname: INITIAL_STATE.dataOpname,
        selectedDataOpname: INITIAL_STATE.selectedDataOpname,
        opname_stok_details: INITIAL_STATE.opname_stok_details,
        selectedDataBarang: INITIAL_STATE.selectedDataBarang,
      };
    default:
      return state;
  }
};
