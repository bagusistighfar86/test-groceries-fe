export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  dataPembelian: [],
  detailPembelian: {},
  dataPaginasi: {},
};

export const PembelianDibatalkanReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'ON_FETCH':
      return {
        ...state,
        dataPembelian: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPembelian: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    default:
      return state;
  }
};
