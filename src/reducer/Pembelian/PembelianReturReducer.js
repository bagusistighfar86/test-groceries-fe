import getNowDate from 'utils/getNowDate';

export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  form: {
    supplierID: '',
    noPesanan: '',
    tanggalRetur: getNowDate(),
    jenisPengembalian: '',
    catatan: '',
    total: 0,
  },
  formAddBarang: {
    barangID: '',
    kuantitasRetur: '',
    subTotal: 0,
  },
  formEditBarang: {
    barangID: '',
    kuantitasRetur: '',
    subTotal: 0,
  },
  selectedStatus: [{
    value: '', label: '',
  }],
  selectedSupplier: {
    value: 'Pilih supplier', label: 'Pilih supplier',
  },
  selectedJenisPengembalian: {
    value: 'Pilih jenis pengembalian', label: 'Pilih jenis pengembalian',
  },
  selectedBarang: {
    value: 'Pilih barang',
    label: 'Pilih barang',
    id: '',
    kodeBarang: '',
    gudang: '',
    hargaBeli: '',
    kuantitasMax: 0,
    subTotal: 0,
  },
  dataPembelian: [],
  selectedDataPembelian: [],
  detailPembelian: {},
  purchase_details: [],
  dataSupplier: [],
  dataBarang: [],
  selectedDataBarang: [],
};

export const PembelianReturReducer = (state, action) => {
  function setStatus() {
    const newArray = [...state.selectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  function addQuantityDataPembelian() {
    const newArray = [...state.dataPembelian];
    newArray[action.payload.itemPos].kuantitasRetur = parseInt(newArray[action.payload.itemPos].kuantitasRetur, 10) + parseInt(action.payload.data.kuantitasRetur, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(newArray[action.payload.itemPos].subTotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }

  function addQuantitySaleDetails() {
    const newArray = [...state.purchase_details];
    newArray[action.payload.itemPos].qty = parseInt(newArray[action.payload.itemPos].qty, 10) + parseInt(action.payload.data.kuantitasRetur, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(newArray[action.payload.itemPos].subtotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }
  function setQuantityDataPembelian() {
    const newArray = [...state.dataPembelian];
    newArray[action.payload.itemPos].kuantitasRetur = parseInt(action.payload.data.kuantitasRetur, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(action.payload.data.kuantitasRetur, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  function setQuantitySaleDetails() {
    const newArray = [...state.purchase_details];
    newArray[action.payload.itemPos].qty = parseInt(action.payload.data.kuantitasRetur, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(action.payload.data.kuantitasRetur, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataPembelian: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPembelian: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataPembelian: action.payload.data,
        purchase_details: action.payload.data_purchase_details,
        selectedDataPembelian: action.payload.selectedData,
      };
    case 'ADD_QUANTITY': {
      return {
        ...state,
        dataPembelian: addQuantityDataPembelian(),
        purchase_details: addQuantitySaleDetails(),
      };
    }
    case 'SET_QUANTITY': {
      return {
        ...state,
        dataPembelian: setQuantityDataPembelian(),
        purchase_details: setQuantitySaleDetails(),
      };
    }
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataPembelian: action.payload,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        [action.payload.dataName]: setStatus(),
      };
    }
    case 'RESET_STATE_ADD_BARANG_FORM':
      return {
        ...state,
        formAddBarang: INITIAL_STATE.formAddBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE_EDIT_BARANG_FORM':
      return {
        ...state,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE':
      return {
        ...state,
        form: INITIAL_STATE.form,
        formAddBarang: INITIAL_STATE.formAddBarang,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
        selectedSupplier: INITIAL_STATE.selectedSupplier,
        selectedJenisPengembalian: INITIAL_STATE.selectedJenisPengembalian,
        dataPembelian: INITIAL_STATE.dataPembelian,
        selectedDataPembelian: INITIAL_STATE.selectedDataPembelian,
        purchase_details: INITIAL_STATE.purchase_details,
        selectedDataBarang: INITIAL_STATE.selectedDataBarang,
      };
    default:
      return state;
  }
};
