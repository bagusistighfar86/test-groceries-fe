import getNowDate from 'utils/getNowDate';

export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  form: {
    supplierID: '',
    noPesanan: '',
    tanggalBeli: getNowDate(),
    jenisPembayaran: '',
    catatan: '',
    total: 0,
  },
  formAddBarang: {
    barangID: '',
    kuantitas: '',
    subTotal: 0,
  },
  formEditBarang: {
    barangID: '',
    kuantitas: '',
    subTotal: 0,
  },
  selectedStatus: [{
    value: '', label: '',
  }],
  selectedSupplier: {
    value: 'Pilih supplier', label: 'Pilih supplier',
  },
  selectedJenisPembayaran: {
    value: 'Pilih jenis pembayaran', label: 'Pilih jenis pembayaran',
  },
  selectedBarang: {
    value: 'Pilih barang',
    label: 'Pilih barang',
    id: '',
    kodeBarang: '',
    gudang: '',
    hargaBeli: '',
  },
  dataPembelian: [],
  selectedDataPembelian: [],
  detailPembelian: {},
  purchase_details: [],
  dataSupplier: [],
  dataBarang: [],
  selectedDataBarang: [],
  dataStatus: [],
  dataPaginasi: {},
};

export const PembelianDiprosesReducer = (state, action) => {
  function changeStatusDisabled() {
    const arr = [...state.dataStatus];
    let disabledIndex = [];
    if (action.payload.value === 8) disabledIndex = [8, 9, 10];
    else if (action.payload.value === 9) disabledIndex = [8, 9, 12];

    const newArr = arr[action.payload.itemPos].map((item) => {
      if (disabledIndex.includes(item.value)) {
        return { ...item, disabled: true };
      }
      return { ...item, disabled: false };
    });
    arr[action.payload.itemPos] = newArr;
    return arr;
  }

  function setStatus() {
    const newArray = [...state.selectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  function addQuantityDataPembelian() {
    const newArray = [...state.dataPembelian];
    newArray[action.payload.itemPos].kuantitas = parseInt(newArray[action.payload.itemPos].kuantitas, 10) + parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(newArray[action.payload.itemPos].subTotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }

  function addQuantityPurchaseDetails() {
    const newArray = [...state.purchase_details];
    newArray[action.payload.itemPos].qty = parseInt(newArray[action.payload.itemPos].qty, 10) + parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(newArray[action.payload.itemPos].subtotal, 10) + parseInt(action.payload.data.subTotal, 10);
    return newArray;
  }
  function setQuantityDataPembelian() {
    const newArray = [...state.dataPembelian];
    newArray[action.payload.itemPos].kuantitas = parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subTotal = parseInt(action.payload.data.kuantitas, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  function setQuantityPurchaseDetails() {
    const newArray = [...state.purchase_details];
    newArray[action.payload.itemPos].qty = parseInt(action.payload.data.kuantitas, 10);
    newArray[action.payload.itemPos].subtotal = parseInt(action.payload.data.kuantitas, 10) * parseInt(action.payload.data.hargaBeli, 10);
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataPembelian: action.payload.data,
        selectedDataPembelian: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPembelian: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataPembelian: action.payload.data,
        purchase_details: action.payload.data_purchase_details,
        selectedDataPembelian: action.payload.selectedData,
      };
    case 'ADD_QUANTITY': {
      return {
        ...state,
        dataPembelian: addQuantityDataPembelian(),
        purchase_details: addQuantityPurchaseDetails(),
      };
    }
    case 'SET_QUANTITY': {
      return {
        ...state,
        dataPembelian: setQuantityDataPembelian(),
        purchase_details: setQuantityPurchaseDetails(),
      };
    }
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataPembelian: action.payload,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        selectedStatus: action.payload.selected,
        dataStatus: action.payload.data,
      };
    }
    case 'UPDATE_STATUS': {
      return {
        ...state,
        selectedStatus: setStatus(),
        dataStatus: changeStatusDisabled(),
      };
    }
    case 'RESET_STATE_ADD_BARANG_FORM':
      return {
        ...state,
        formAddBarang: INITIAL_STATE.formAddBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE_EDIT_BARANG_FORM':
      return {
        ...state,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
      };
    case 'RESET_STATE':
      return {
        ...state,
        form: INITIAL_STATE.form,
        formAddBarang: INITIAL_STATE.formAddBarang,
        formEditBarang: INITIAL_STATE.formEditBarang,
        selectedBarang: INITIAL_STATE.selectedBarang,
        selectedSupplier: INITIAL_STATE.selectedSupplier,
        selectedJenisPembayaran: INITIAL_STATE.selectedJenisPembayaran,
        dataPembelian: INITIAL_STATE.dataPembelian,
        selectedDataPembelian: INITIAL_STATE.selectedDataPembelian,
        purchase_details: INITIAL_STATE.purchase_details,
        selectedDataBarang: INITIAL_STATE.selectedDataBarang,
      };
    default:
      return state;
  }
};
