export const INITIAL_STATE = {
  isLoading: true,
  showPassword: false,
  search: '',
  roleFilter: '',
  showRows: 10,
  form: {
    nama: '',
    telepon: '',
    jenisKelamin: '',
    tanggalLahir: '',
    nik: '',
    alamat: '',
    email: '',
    password: '',
    jabatan: '',
    role: '',
    isActive: false,
  },
  formEditKaryawan: {
    nama: '',
    telepon: '',
    jenisKelamin: '',
    tanggalLahir: '',
    nik: '',
    alamat: '',
    email: '',
    password: '--------',
    jabatan: '',
    role: '',
    isActive: false,
  },
  dataPaginasi: {},
  dataKaryawan: [],
  detailKaryawan: {},
  selectedDataKaryawan: [],
};

export const KaryawanReducer = (state, action) => {
  function setStatus() {
    const newArray = [...state.dataKaryawan];
    newArray[action.payload.itemPos].is_active = action.payload.data;
    return newArray;
  }
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataKaryawan: action.payload.data,
        selectedDataKaryawan: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailKaryawan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataKaryawan: action.payload.data,
        selectedDataKaryawan: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataKaryawan: action.payload,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        dataKaryawan: setStatus(),
      };
    }
    case 'SET_SHOW_PASSWORD':
      return {
        ...state,
        showPassword: !state.showPassword,
      };
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
