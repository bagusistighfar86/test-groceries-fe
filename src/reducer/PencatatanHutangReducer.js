export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  formBayarAngsuran: {
    kodePembelian: '',
    sisaTagihan: '',
    nominalBayar: '',
    buktiPembayaran: undefined,
  },
  dataHutang: [],
  detailHutang: [],
  dataPembayaran: [],
};

export const PencatatanHutangReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'ON_FETCH':
      return {
        ...state,
        dataHutang: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailHutang: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_FORM':
      return {
        ...state,
        formBayarAngsuran: action.payload,
      };
    default:
      return state;
  }
};
