export const INITIAL_STATE = {
  nama: '',
  telfon: '',
  password: '',
  isEdit: true,
  showPassword: false,
};

export const EditProfileReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_SHOW_PASSWORD':
      return {
        ...state,
        showPassword: !state.showPassword,
      };
    case 'SET_EDIT':
      return {
        ...state,
        isEdit: !state.isEdit,
      };
    default:
      return state;
  }
};
