export const INITIAL_STATE = {
  email: '',
  form: {
    oldPassword: '',
    newPassword: '',
    confirmNewPassword: '',
  },
  isLoading: false,
  showOldPassword: false,
  showNewPassword: false,
  showConfirmNewPassword: false,
};

export const ChangePasswordProfileReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_INPUT':
      return action.payload;
    case 'RESET_FORM':
      return INITIAL_STATE;
    case 'BEFORE_FETCH':
      return {
        ...state,
        isLoading: true,
      };
    case 'SET_SHOW_OLD_PASSWORD':
      return {
        ...state,
        showOldPassword: !state.showOldPassword,
      };
    case 'SET_SHOW_PASSWORD':
      return {
        ...state,
        showNewPassword: !state.showNewPassword,
      };
    case 'SET_SHOW_CONFIRM_PASSWORD':
      return {
        ...state,
        showConfirmNewPassword: !state.showConfirmNewPassword,
      };
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    default:
      return state;
  }
};
