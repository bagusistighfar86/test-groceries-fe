export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  formBayarAngsuran: {
    kodePembelian: '',
    sisaTagihan: '',
    nominalBayar: '',
    buktiPembayaran: undefined,
  },
  dataPiutang: [],
  detailPiutang: [],
  dataPembayaran: [],
};

export const PencatatanPiutangReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'ON_FETCH':
      return {
        ...state,
        dataPiutang: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPiutang: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_FORM':
      return {
        ...state,
        formBayarAngsuran: action.payload,
      };
    default:
      return state;
  }
};
