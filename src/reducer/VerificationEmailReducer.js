export const INITIAL_STATE = {
  email: '',
};

export const VerificationEmailReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_INPUT':
      return action.payload;
    case 'RESET_FORM':
      return INITIAL_STATE;
    default:
      return state;
  }
};
