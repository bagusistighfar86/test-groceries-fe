export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  roleFilter: '',
  showRows: 10,
  form: {
    namaLengkap: '',
    telepon: '',
    jenisKelamin: '',
    nik: '',
    alamat: '',
  },
  selectedFilterStatus: {
    value: '0', label: 'Pilih status driver',
  },
  selectedStatus: [{
    value: '', label: '',
  }],
  dataStatus: [],
  dataDriver: [],
  detailDriver: {},
  selectedDataDriver: [],
  dataPaginasi: {},
};

export const DriverReducer = (state, action) => {
  function changeStatusDisabled() {
    const arr = [...state.dataStatus];
    let disabledIndex = [];
    if (action.payload.value === 13) disabledIndex = [13, 15];
    else if (action.payload.value === 14) disabledIndex = [13, 14];
    else if (action.payload.value === 15) disabledIndex = [13, 14, 15];

    const newArr = arr[action.payload.itemPos].map((item) => {
      if (disabledIndex.includes(item.value)) {
        return { ...item, disabled: true };
      }
      return { ...item, disabled: false };
    });
    arr[action.payload.itemPos] = newArr;
    return arr;
  }

  function setStatus() {
    const newArray = [...state.selectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FILTER_STATUS':
      return {
        ...state,
        selectedFilterStatus: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataDriver: action.payload.data,
        selectedDataDriver: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailDriver: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataDriver: action.payload.data,
        selectedDataDriver: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        selectedStatus: action.payload.selected,
        dataStatus: action.payload.data,
      };
    }
    case 'UPDATE_STATUS': {
      return {
        ...state,
        selectedStatus: setStatus(),
        dataStatus: changeStatusDisabled(),
      };
    }
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataDriver: action.payload,
      };
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
