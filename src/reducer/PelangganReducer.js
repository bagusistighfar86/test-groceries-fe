export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  roleFilter: '',
  showRows: 10,
  form: {
    namaLengkap: '',
    telepon: '',
    jenisKelamin: '',
    nik: '',
    namaToko: '',
    batasKredit: '',
    jatuhTempo: '',
    catatan: '',
    negara: '',
    provinsi: '',
    kota: '',
    kodePos: '',
    alamatToko: '',
  },
  dataPelanggan: [],
  detailPelanggan: {},
  selectedDataPelanggan: [],
  dataPaginasi: {},
};

export const PelangganReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataPelanggan: action.payload.data,
        selectedDataPelanggan: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailPelanggan: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataPelanggan: action.payload.data,
        selectedDataPelanggan: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataPelanggan: action.payload,
      };
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
