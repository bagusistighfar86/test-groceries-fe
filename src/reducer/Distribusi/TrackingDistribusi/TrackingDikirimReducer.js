export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  selectedStatus: {
    value: '', label: '',
  },
  dataStatus: [],
  dataTracking: [],
  detailTracking: [],
  dataDriver: [],
  detailDriver: {},
  sale_details: [],
  dataPaginasi: {},
};

export const TrackingDikirimReducer = (state, action) => {
  function changeStatusDisabled() {
    const arr = [...state.dataStatus];
    let disabledIndex = [];
    if (action.payload.value === 15) disabledIndex = [14, 15];
    else if (action.payload.value === 16) disabledIndex = [14, 15, 16];

    const newArr = arr[action.payload.itemPos].map((item) => {
      if (disabledIndex.includes(item.value)) {
        return { ...item, disabled: true };
      }
      return { ...item, disabled: false };
    });
    arr[action.payload.itemPos] = newArr;
    return arr;
  }

  function setStatus() {
    const newArray = [...state.selectedStatus];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataTracking: action.payload.data,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailTracking: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataDriver: action.payload.data,
        selectedDriver: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_STATUS': {
      return {
        ...state,
        selectedStatus: action.payload.selected,
        dataStatus: action.payload.data,
      };
    }
    case 'UPDATE_STATUS': {
      return {
        ...state,
        selectedStatus: setStatus(),
        dataStatus: changeStatusDisabled(),
      };
    }
    default:
      return state;
  }
};
