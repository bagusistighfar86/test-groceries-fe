export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  selectedDriver: {
    value: 'Pilih Driver', label: 'Pilih Driver', itemID: '',
  },
  selectedDataDriver: [{
    value: 'Pilih Driver', label: 'Pilih Driver', itemID: '',
  }],
  dataTracking: [],
  dataDriver: [],
  detailDriver: {},
  sale_details: [],
  dataPaginasi: {},
};

export const TrackingSiapDikirimReducer = (state, action) => {
  function setDriver() {
    const newArray = [...state.selectedDriver];
    newArray[action.payload.itemPos] = action.payload.data;
    return newArray;
  }

  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'SET_FORM_ADD_BARANG':
      return {
        ...state,
        formAddBarang: action.payload,
      };
    case 'SET_FORM_EDIT_BARANG':
      return {
        ...state,
        formEditBarang: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataTracking: action.payload.data,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailDriver: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_DRIVER': {
      return {
        ...state,
        [action.payload.name]: setDriver(),
      };
    }
    default:
      return state;
  }
};
