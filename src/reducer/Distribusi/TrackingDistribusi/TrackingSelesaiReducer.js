export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  selectedStatus: {
    value: '', label: '',
  },
  dataTracking: [],
  detailTracking: {},
  dataPaginasi: {},
};

export const TrackingSelesaiReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataTracking: action.payload.data,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailTracking: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    default:
      return state;
  }
};
