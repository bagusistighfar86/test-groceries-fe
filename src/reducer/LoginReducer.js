export const INITIAL_STATE = {
  email: '',
  password: '',
  isLoading: false,
  showPassword: false,
  ingatSaya: false,
};

export const LoginReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_INPUT':
      return action.payload;
    case 'RESET_FORM':
      return INITIAL_STATE;
    case 'BEFORE_FETCH':
      return {
        ...state,
        isLoading: true,
      };
    case 'SET_SHOW_PASSWORD':
      return {
        ...state,
        showPassword: !state.showPassword,
      };
    case 'SET_INGAT_SAYA':
      return {
        ...state,
        ingatSaya: !state.ingatSaya,
      };
    default:
      return state;
  }
};
