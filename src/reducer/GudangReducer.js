export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  roleFilter: '',
  showRows: 10,
  form: {
    namaGudang: '',
    teleponGudang: '',
    alamatGudang: '',
    staffGudangID: '',
  },
  dataGudang: [],
  detailGudang: {},
  selectedDataGudang: [],
  dataStaffGudang: [],
  selectedStaffGudang: {},
};

export const GudangReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'ON_FETCH':
      return {
        ...state,
        dataGudang: action.payload.data,
        selectedDataGudang: action.payload.selectedData,
        isLoading: action.payload.isLoading,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailGudang: action.payload.data,
        isLoading: action.payload.isLoading,
      };
    case 'UPDATE_DATA':
      return {
        ...state,
        dataGudang: action.payload.data,
        selectedDataGudang: action.payload.selectedData,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };

    case 'SET_SELECTED':
      return {
        ...state,
        selectedDataGudang: action.payload,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};
