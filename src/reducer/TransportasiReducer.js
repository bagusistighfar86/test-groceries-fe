export const INITIAL_STATE = {
  isLoading: true,
  search: '',
  showRows: 10,
  form: {
    nomorKendaraan: '',
    tipeKendaraan: '',
    driver: '',
  },
  selectedSortOptions: {
    value: 'Tipe Kendaraan', label: 'Tipe Kendaraan',
  },
  selectedDriver: {
    value: 'Pilih driver', label: 'Pilih driver',
  },
  selectedTipeKendaraan: {
    value: 'Pilih tipe kendaraan', label: 'Pilih tipe kendaraan',
  },
  detailTransportasi: {},
  dataKategoriProduk: [],
  dataTipeKendaraan: [],
  dataDriver: [],
};

export const TransportasiReducer = (state, action) => {
  switch (action.type) {
    case 'SET_IS_LOADING':
      return {
        ...state,
        isLoading: action.payload,
      };
    case 'CHANGE_INPUT':
      return action.payload;
    case 'SET_FORM':
      return {
        ...state,
        form: action.payload,
      };
    case 'ON_FETCH_GET_BY_ID':
      return {
        ...state,
        detailTransportasi: action.payload.data,
      };
    case 'SET_DATA':
      return {
        ...state,
        [action.payload.name]: action.payload.data,
      };
    case 'SET_SELECTED_SELECT':
      return {
        ...state,
        [action.payload.dataName]: action.payload.data,
      };
    case 'RESET_STATE':
      return {
        ...INITIAL_STATE,
        dataTipeKendaraan: state.dataTipeKendaraan,
        dataDriver: state.dataDriver,
      };
    default:
      return state;
  }
};
